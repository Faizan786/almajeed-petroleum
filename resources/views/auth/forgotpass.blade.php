@extends('layouts.default')
@section('content')

<div class="about_us contact-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Retrieve Your Password</h1>
            </div>
        </div>
    </div>
</div>
<div class="middle-section login-section">      
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="contact_section text-center login_area">
                    <h3>Forgot Password? Retrieve Now!</strong></h3>
                    <p>
                        @if(session('message_title') != "" && session('message') != "")
                    <h4 class="pt-10 mt-0 mb-30" style="color: red;">
                        {{ session('message_title') }}: {{ session('message') }}
                    </h4>
                    @endif
                    </p>
                    <br/>
                    <div class="contact_form ">
                        <form name="forgot-form" class="clearfix" method="POST" action="{{ url('forgot-password') }}">
                            @csrf

                            <div class="row">
                                <div class="form-group col-md-12 text-left">
                                    <label for="email">Enter Your Email Address</label>
                                    <input name="email" id="email" type="email" class="form-control" value="" required autofocus />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <input type="submit" value="Login" class="submit login" name="submitbtn">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="regsiter_txt">
                        <p>Remembered Username/Password ? <a href="{{ url('/login') }}">Go to Login Page</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="call-action">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8">
                <h6>We Look Forward To Hearing From You</h6>
                <h4>Please contact us to learn how our services will benefit you.</h4>
                <h3>+1 (702) 629-5189</h3>
            </div>
            <div class="col-12 col-sm-4">
                <a href="{{ url('/contact') }}">Contact Us</a>
            </div>
        </div>
    </div>
</div>

@endsection