@extends('layouts.default')
@section('content')

<div class="about_us contact-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>User Registration</h1>
            </div>
        </div>
    </div>
</div>
<div class="middle-section login-section">      
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="contact_section text-center login_area">
                    <h3>Already have an Account? <a href="{{ url('/login') }}"><strong>Login Now</strong></a></h3>
                    <br/>
                    <div class="contact_form ">
                        <form name="reg-form" class="register-form" method="post" action="{{ url('register') }}">
                            @csrf
                            <div class="icon-box mb-0 p-0">
                                @if(session('message_title') != "" && session('message') != "")
                                <h4 class="pt-10 mt-0 mb-30" style="color: red;">
                                    {{ session('message_title') }}: {{ session('message') }}
                                </h4>
                                @endif
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 text-left">
                                    <label>First Name</label>
                                    <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 text-left">
                                    <label>Last Name</label>
                                    <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name">
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 text-left">
                                    <label>Your Company</label>
                                    <input id="company" type="text" class="form-control @error('first_name') is-invalid @enderror" name="company" value="{{ old('company') }}" required autocomplete="company" autofocus>
                                    @error('company')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 text-left">
                                    <label>Email Address</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 text-left">
                                    <label for="form_choose_username">Choose Username</label>
                                    <input id="form_choose_username" name="username" class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 text-left">
                                    <label for="form_choose_password">Choose Password</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 text-left">
                                    <label>Re-enter Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Register Now" class="submit login" name="registerBtn">
                            </div>
                        </form>
                    </div>
                    <div class="regsiter_txt">
                        <p>Already have an account? <a href="{{ url('/login') }}">Login Now</a></p>
                    </div>
                    <div class="empty-space-12">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="call-action">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8">
                <h6>We Look Forward To Hearing From You</h6>
                <h4>Please contact us to learn how our services will benefit you.</h4>
                <h3>+1 (702) 629-5189</h3>
            </div>
            <div class="col-12 col-sm-4">
                <a href="contact.html">Contact Us</a>
            </div>
        </div>
    </div>
</div>

@stop