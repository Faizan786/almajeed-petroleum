@extends('layouts.default')
@section('content')

<div class="about_us contact-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Login to client portal</h1>
            </div>
        </div>
    </div>
</div>
<div class="middle-section login-section">      
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="contact_section text-center login_area">
                    <h3>Client Login!</strong></h3>
                    <p>
                        @if(session('message_title') != "" && session('message') != "")
                    <h4 class="pt-10 mt-0 mb-30" style="color: red;">
                        {{ session('message_title') }}: {{ session('message') }}
                    </h4>
                    @endif
                    </p>
                    <br/>
                    <div class="contact_form ">
                        <form name="login-form" class="clearfix" method="POST" action="{{ url('login') }}">
                            @csrf

                            <div class="row">
                                <div class="form-group col-md-12 text-left">
                                    <label for="form_username_email">Email or Username</label>
                                    <input id="email_username" ytpe="text" class="form-control" name="email_username" value="{{ old('email') }}" required autocomplete="email_username" autofocus>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 text-left">
                                    <label for="form_password">Password</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <ul>
                                        <li><label><input type="checkbox"> Remember Me?</label></li>
                                        <li class="no-border"><a href="{{ url('forgot-password') }}">Forgot Password?</a></li>
                                    </ul>
                                    <input type="submit" value="Login" class="submit login" name="submitbtn">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="regsiter_txt">
                        <p>Don't have an Account? <a href="{{ url('/register') }}">Request Account Now</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="call-action">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8">
                <h6>We Look Forward To Hearing From You</h6>
                <h4>Please contact us to learn how our services will benefit you.</h4>
                <h3>+1 (702) 629-5189</h3>
            </div>
            <div class="col-12 col-sm-4">
                <a href="{{ url('/contact') }}">Contact Us</a>
            </div>
        </div>
    </div>
</div>

@endsection