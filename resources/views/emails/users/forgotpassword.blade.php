@component('mail::message')
# Hi <i>{{ $user->fullname }}</i>,<br /><br />
<p>Your username is <b>{{ $user->username }}</b>. Please use your temporary new password to login. Your temporary new password is below.</p>

<p style="background: #f1f1f1; padding: 5px 20px;">{{ $temp_pass }}</p>

Thanks,<br />
{{ config('app.name') }} Team <br />
<img src="{{ asset('/docrequest_images/logo.png') }}" width="150" />
@endcomponent
