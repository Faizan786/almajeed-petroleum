@component('mail::message')
# Hi <i>{{ $user->fullname }}</i>,<br /><br />
<h2>It’s time to confirm your email address.</h2>
<p>Have we got the right email address to reach you on? To confirm that you can get our emails, just click the button below.</p>

@component('mail::button', ['url' => config('app.url').'verify-email/'.$encryptId])
Confirm my email address
@endcomponent

Thanks,<br />
{{ config('app.name') }} Team <br />
<img src="{{ asset('/docrequest_images/logo.png') }}" width="150" />
@endcomponent
