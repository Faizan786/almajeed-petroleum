@component('mail::message')
# Hi <i>{{ $user->fullname }}</i>,<br /><br />
<p>Thank you for registration on our website. Once your account is approved by us, you can login to our website portal. You'll get notification when your account will be approved.</p>

@component('mail::button', ['url' => config('app.url').'login'])
Visit Website Portal
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team <br />
<img src="{{ asset('/docrequest_images/logo.png') }}" width="150" />
@endcomponent
