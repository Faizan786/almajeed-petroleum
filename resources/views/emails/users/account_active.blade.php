@component('mail::message')
# Hi <i>{{ $user->fullname }}</i>,<br /><br />
<p>Thank you for registration on our website. Your account verification has been completed. You can login to our website portal with username/email and password which you provided at registration time.</p>

@component('mail::button', ['url' => config('app.url').'login'])
Visit Website Portal
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team <br />
<img src="{{ asset('/docrequest_images/logo.png') }}" width="150" />
@endcomponent
