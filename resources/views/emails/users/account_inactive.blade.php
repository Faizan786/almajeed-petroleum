@component('mail::message')
# Hi <i>{{ $user->fullname }}</i>,<br /><br />
<p>Thank you for being our customer. Unfortunately! Your account has been deactivated by us, you can't login to our website portal. You'll get notification when your account will be approved. Please contact us as soon as possible for further information.</p>

@component('mail::button', ['url' => config('app.url').'contact'])
Contact Us
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team <br />
<img src="{{ asset('/docrequest_images/logo.png') }}" width="150" />
@endcomponent
