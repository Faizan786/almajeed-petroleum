<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ $title }}</title>
        <style type="text/css" media="all">
            * {
                font-family: Verdana, Arial, sans-serif;
            }
            table{
                font-size: x-small;
            }
            tfoot tr td{
                font-weight: bold;
                font-size: x-small;
            }
            .gray {
                background-color: lightgray
            }
        </style>
    </head>
    <body>
        <table width="100%">
            <tr>
                <td valign="top">
                    <img src="{{ $logo }}" alt="Logo" width="150"/><pre>P.O. Box 530718<br />Henderson, NV 89053<br /><b>Phone#:</b> (702) 629-5189<br /><b>Fax#:</b> (888) 341-5040<br /><b>Email:</b> customerservice@docrequest.com </pre>
                </td>
                <td align="right">
                    <h2>PRE-PAYMENT - INVOICE</h2>
                    <h2>Invoice#: {{ $invoice_ob->invoice_number }}</h2>
                    <h2>Date: {{ date('d M, Y', strtotime($invoice_ob->created_at)) }}</h2>
                </td>
            </tr>

        </table>

        <table width="100%" cellspacing='0'>
            <tr>
                <td><strong>Bill To:</strong> {{ select_field_id('users', $invoice_ob->user_id, 'fullname') }}</td>
                <td><strong>Attention to:</strong> {{ select_field_id('users', $invoice_ob->user_id, 'company') }}</td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px 0;">We have received your request for records regarding your client/patient:</td>
            </tr>
        </table>

        <table width='100%' cellpadding='5' cellspacing='0' border='1' style="margin-bottom:15px;">
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Provider</td>
                <td width='30%'>{{ select_field_id('providers', $order_ob->medical_provider_id, 'facility').' '.select_field_id('providers', $order_ob->medical_provider_id, 'physician') }}</td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Date of Service</td>
                <td width='30%'>{{ ($order_ob->service_date_from != "")?date('d M, Y', strtotime($order_ob->service_date_from)):'' }} --- {{ ($order_ob->service_date_to != "")?date('d M, Y', strtotime($order_ob->service_date_to)):'' }}</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Patient Name</td>
                <td width='30%'>{{ $order_ob->first_name.' '.$order_ob->last_name }}</td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">DOB</td>
                <td width='30%'>{{ date('d M, Y', strtotime($order_ob->DOB)) }}</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Claim/Case#</td>
                <td width='30%'>{{ $order_ob->claim_case_number }}</td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Terms</td>
                <td width='30%'>&nbsp;</td>
            </tr>
        </table>

        <h4 style="text-align:left; margin: 5px 0;">PAYMENT IS REQUIRED BEFORE RECORDS CAN BE MAILED OR FAX.</h4>
        <h5 style="text-align:left; margin: 8px 0; font-weight: normal;">FIND OUT MORE ABOUT MONTHLY BILLING SERVICES. Records requested can be delivered right away. To set up an account, please contact Isis Johnson at 702-629-5189 or isis@docrequest.com</h5>
        
        @if(isset($items_list) && !is_null($items_list) && count($items_list) > 0)
        <table width='100%' cellpadding='5' cellspacing='0' border='0' style="margin-bottom:15px;">
            <thead style="background-color: lightgray; border: 1px solid #ccc;">
                <tr>
                    <th>#</th>
                    <th>Description</th>
                    <th align='center'>Quantity</th>
                    <th align='center'>Rate</th>
                    <th align='right'>Amount</th>
                </tr>
            </thead>
            <tbody style="border: 1px solid #ccc;">
                @foreach($items_list as $key => $value)
                <tr>
                    <th scope="row" style="border-right: 1px solid #ccc;">{{ $key+1 }}</th>
                    <td style="border-right: 1px solid #ccc;">{{ select_field_id('items_records', $value, 'name') }}</td>
                    <td style="border-right: 1px solid #ccc;" align="center">{{ $quantities_list[$key] }}</td>
                    <td style="border-right: 1px solid #ccc;" align="center">{{ $rates_list[$key] }}</td>
                    <td align="right">{{ (isset($quantities_list[$key]) && isset($rates_list[$key]))? $quantities_list[$key]*$rates_list[$key] :'' }}</td>
                </tr>
                @endforeach
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td style="border-right: 1px solid #ccc;" align="right">Subtotal $</td>
                    <td style="border-right: 1px solid #ccc;" align="right">{{ $invoice_ob->amount }}</td>
                </tr>
                <tr>
                    <td colspan="3">PAYMENT OPTIONS: <span style="font-weight: normal;">Check / Credit Card</span></td>
                    <td style="border-right: 1px solid #ccc;" align="right">Sales Tax (0.0%) $</td>
                    <td style="border-right: 1px solid #ccc;" align="right">0.00</td>
                </tr>
                <tr>
                    <td colspan="3" style="font-weight: normal;">We do not accept Credit Card Payment less than $15.00.</td>
                    <td style="border-right: 1px solid #ccc;" align="right">Payments/Credits $</td>
                    <td style="border-right: 1px solid #ccc;" align="right">0.00</td>
                </tr>
                <tr>
                    <td colspan="3">PLEASE MAKE CHECK PAYABLE TO:</td>
                    <td style="border-right: 1px solid #ccc;" align="right">Balance Due $</td>
                    <td style="border-right: 1px solid #ccc;" align="right">0.00</td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:left; font-weight: normal;">DOC REQUEST, LLC, PO Box 530718, Henderson, NV 89053.<br />TAX ID# 26-1335414</td>
                    <td style="border-right: 1px solid #ccc;" align="right">Total $</td>
                    <td style="border-right: 1px solid #ccc;" align="right" class="gray">$ {{ $invoice_ob->amount }}</td>
                </tr>
            </tfoot>
        </table>
        @endif
        
        <br />
        <div style="font-size: 9px; text-align: justify;">
            <b>Doc Request PAYMENT, REFUND AND CANCELLATION POLICY</b><br />
            Please read it carefully as this is the official policy. The policy listed below supersedes any other written document you may have received prior to today's date.
            ALL SALES FOR SERVICES, RECORDS AND GOODS ARE FINAL Doc Request will not issue a refund for services purchased. Once you have requested records in writing or posted your transaction on our website, you have agreed to pay for the services, records, or goods. You
            cannot cancel or modify an invoice which is a bill for services rendered. The sale of the services, records and goods applies to the Doc Request service charge associated with all such requests as well as the
            copying cost for records and goods. All fees are in accordance with applicable state and federal law. Upon certain conditions, Doc Request may offer a refund, or credit for the copy cost of records requested, if
            the order is modified or cancelled in writing before an invoice is issued, but a $35.00 processing and/or early cancellation fee will apply.
            <br /><b>CURRENT PAYMENT POLICY:</b><br />
            The following payment policy MUST be adhered to. THERE ARE NO EXCEPTIONS, ALL Doc Request clients MUST READ, UNDERSTAND AND ADHERE TO THIS POLICY except by other written
            arrangements approved by Doc Request. Any payments made without regard to this payment policy will be charged a service charge of $50.00. Late charges may also apply (see below). Our policy is to collect
            payment on the date an invoice is issued. We accept payment by credit card including VISA, Mastercard and Discover. Doc Request also accepts payment by check. Returned checks are subject to a $35.00
            returned check fee as well as any bank charges for returned checks.
            All invoices for any services, records or goods are due, paid in full, within 30 business days of the invoice issue date, unless other Doc Request approved arrangements have been made ahead of time. Late
            charges apply (see below).
            <br /><b>LATE CHARGES:</b><br />
            ALL payments such as invoices, service charges and returned check charges described in the above policy MUST BE RECEIVED BY Doc Request within 30 business days of the date the invoice was issued or
            late fees in the amount of 10% of the invoice total will be charged for every business day your payment is late. This applies to ALL clients, without exception. Non-payment may result in your account being sent
            to collections, your credit standing with us being downgraded and possible litigation to recover payment. If you have questions or concerns, or you feel that you have been charged incorrectly, please contact us
            &nbsp; <b>IMMEDIATELY</b> so that we can work together to find a solution. We prefer to work payment issues out in a positive way and we try not to resort to sending payments to collections or small-claims unless we
            have no recourse. We value you as a client and strive for your satisfaction
        </div>
    </body>
</html>