<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ $title }}</title>
        <style type="text/css" media="all">
            * {
                font-family: Verdana, Arial, sans-serif;
            }
            table{
                font-size: x-small;
            }
            tfoot tr td{
                font-weight: bold;
                font-size: x-small;
            }
            .gray {
                background-color: lightgray
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0">
            <tr>
                <td width="40%" align="left">
                    <img src="{{ $logo }}" alt="Logo" width="150"/><pre>P.O. Box 530718<br />Henderson, NV 89053<br /><b>Phone#:</b> (702) 629-5189<br /><b>Fax#:</b> (888) 341-5040<br /><b>Email:</b> customerservice@docrequest.com </pre>
                </td>
                <td width="60%" align="right" valign="top">
                    <h2 style="font-size:16px;">CERTIFICATE of MEDICAL RECORDS CUSTODIAN</h2>
                    <h2 style="font-size:18px;">STATE OF NEVADA</h2>
                    <h2 style="font-size:16px;">COUNTY OF CLARK</h2>
                </td>
            </tr>
        </table>
        
        <hr style="width: 100%; border:2px dotted #ccc; margin-bottom: 20px;" />
        
        

        <table width='100%' cellpadding='5' cellspacing='0' border='1' style="margin-bottom:15px;">
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">NOW COMES</td>
                <td width='30%'>
                    @if($order_ob->other_provider != "")
                        {{ $order_ob->other_provider }}
                    @else
                        {{ select_field_id('providers', $order_ob->medical_provider_id, 'facility').' '.select_field_id('providers', $order_ob->medical_provider_id, 'physician') }}
                    @endif
                </td>
                <td width='50%' colspan="2">who after first duly sworn, deposes and says the following: </td>
            </tr>
            <tr>
                <td width='100%' colspan="4">That the deponent is the Copy Technician in the Health information Management Department and such capacity is the custodian of the Medical Records at:</td>
            </tr>
            <tr>
                <td width='100%' colspan="4">{{ select_field_id('providers', $order_ob->medical_provider_id, 'facility').' '.select_field_id('providers', $order_ob->medical_provider_id, 'physician') }}</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">That on:</td>
                <td width='30%'>{{ ($order_ob->request_date != "")?date('d M, Y', strtotime($order_ob->request_date)):'' }}</td>
                <td width='50%' colspan="2">The deponent received a release of information requesting medical records</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Pertaining to:</td>
                <td width='30%'>
                    @if($order_ob->other_user != "")
                        {{ $order_ob->other_user }}
                    @else 
                        {{ $order_ob->pertaining_to }}
                    @endif
                </td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Date of Birth:</td>
                <td width='30%'>{{ ($order_ob->DOB != "")?date('d M, Y', strtotime($order_ob->DOB)):'' }}</td>
            </tr>
            <tr>
                <td width='100%' colspan="4">That the deponent has examined the original or microfilmed original or scanned original of those medical records and has made a true and exact copy of them and that the reproduction of them attached hereto contains:</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Pages of medical records</td>
                <td width='30%'>{{ $order_ob->medical_records_pages }}</td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">and of billing records and is true and complete</td>
                <td width='30%'>{{ $order_ob->billing_records_pages }}</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Date of Service From:</td>
                <td width='30%'>{{ ($order_ob->service_date_from != "")?date('d M, Y', strtotime($order_ob->service_date_from)):'' }}</td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Date of Service To:</td>
                <td width='30%'>{{ ($order_ob->service_date_to != "")?date('d M, Y', strtotime($order_ob->service_date_to)):'' }}</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Films on CD</td>
                <td width='30%' style="margin:0px; padding: 0px 5px; text-align: left;">{!! ($order_ob->cd == "Yes")?'#':'N/A' !!}</td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">No Films</td>
                <td width='30%' style="margin:0px; padding: 0px 5px; text-align: left;">{!! ($order_ob->no_files == "Yes")?'#':'N/A' !!}</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">No#. of CD's</td>
                <td width='30%' style="margin:0px; padding: 0px 5px; text-align: left;">{!! ($order_ob->no_of_cds != "")?$order_ob->no_of_cds:'N/A' !!}</td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">No films requested</td>
                <td width='30%' style="margin:0px; padding: 0px 5px; text-align: left;">{!! ($order_ob->no_files_requested == "Yes")?'#':'N/A' !!}</td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Films located at:</td>
                <td width='30%'>
                    @if($order_ob->other_films_located != "")
                        {{ $order_ob->other_films_located }}
                    @else
                        {{ select_field_id('films_located', $order_ob->films_located_id, 'name') }}
                    @endif
                </td>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Billing records located at:</td>
                <td width='30%'>
                    @if($order_ob->other_billing_records_located != "")
                        {{ $order_ob->other_billing_records_located }}
                    @else
                        {{ select_field_id('billing_records_located', $order_ob->billing_records_located_id, 'name') }}
                    @endif
                </td>
            </tr>
            <tr>
                <td width='100%' colspan="4">That the original of those medical records was made at or near the time of the acts, event, conditions, opinions, or diagnoses recited therein by or from information transmitted by a person with knowledge in the course of a regularly conducted activity of the deponent or the office or institution in which the deponent is engaged.</td>
            </tr>
            <tr>
                <td width='100%' colspan="4">To the extent that the medical records being provided herewith contain medical records received from a different provider of health care, I am unable to make any representation as to the authenticity of such a records. </td>
            </tr>
            <tr>
                <td width='100%' colspan="4"><b>CERTIFICATION OF NO RECORDS:</b></td>
            </tr>
            <tr>
                <td width='100%' colspan="4">
                    <table>
                        <tr>
                            <td align="right" width="3%" valign="top">{!! ($order_ob->no_records_certificate == "Yes")?'<img src="./docrequest_images/tick.png" width="18" />':'<img src="./docrequest_images/cross.png" width="18" />' !!}</td>
                            <td align="justify" width="97%">A through search of our files, carried out under my direction using the specific information provided in your request revealed no documents, records, or other materials or images. It is to be understood that this does not mean that records do not exist under another spelling, name, or other classification.</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width='20%' style="text-align:left; background: #f1f1f1; font-weight: bold;">Date of Service:</td>
                <td width='30%'>{{ ($order_ob->no_records_service_date != "")?date('d M, Y', strtotime($order_ob->no_records_service_date)):'' }}</td>
                <td width='50%' colspan="2">
                    @if($order_ob->other_no_records != "")
                        {{ $order_ob->other_no_records }}
                    @else
                        {{ ($order_ob->no_records_id > 0)?select_field_id('no_records_certification', $order_ob->no_records_id, 'name'):'' }}
                    @endif
                </td>
            </tr>
            <tr>
                <td width='100%' colspan="4">I declare under penalty of perjury that the foregoing is true and correct:</td>
            </tr>
            <tr>
                <td width='50%' colspan="2" style="text-align:right; background: #f1f1f1; font-weight: bold;">By: </td>
                <td width='50%' colspan="2">{{ $order_ob->declared_by }}</td>
            </tr>
        </table>
        
        <h4>Subscribed and Sworn to before me</h4>
        <h4 style="font-weight: normal;">This ______________ day of ______________, 2019</h4>
        <h4 style="margin-top: 40px; font-weight: normal;">_________________________________________<br />
            Notary Signature</h4>
    </body>
</html>