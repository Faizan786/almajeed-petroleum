<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-tab ">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item  @if(Request::is('cpadmin/dashboard') == true) kt-menu__item--active @endif " aria-haspopup="true"><a href="{{ url('/cpadmin/dashboard') }}" class="kt-menu__link"><span class="kt-menu__link-text">Dashboard</span></a></li>
            @if(check_user_role(Session::get('admin_user_id'), 1, ''))
            <li class="kt-menu__item @if(Request::is('cpadmin/users-list/active') == true || Request::is('cpadmin/users-list/inactive') == true || Request::is('cpadmin/users-list/trashed') == true) kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ url('/cpadmin/users-list/active') }}" class="kt-menu__link"><span class="kt-menu__link-text">Users</span></a></li>
            @endif
            @if(check_user_role(Session::get('admin_user_id'), 5, ''))
            <li class="kt-menu__item @if(Request::is('cpadmin/messages-list') == true) kt-menu__item--active @endif " aria-haspopup="true"><a href="{{ url('/cpadmin/messages-list') }}" class="kt-menu__link"><span class="kt-menu__link-text">Messages List</span></a></li>
            @endif
            @if(check_user_role(Session::get('admin_user_id'), 6, ''))
            <li class="kt-menu__item @if(Request::is('cpadmin/beneficiary-list') == true) kt-menu__item--active @endif " aria-haspopup="true"><a href="{{ url('/cpadmin/beneficiary-list') }}" class="kt-menu__link"><span class="kt-menu__link-text">Beneficiary</span></a></li>
            @endif
            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ url('/cpadmin/logout') }}" class="kt-menu__link"><span class="kt-menu__link-text">Logout</span></a></li>
        </ul>
    </div>
</div>