<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

    <!-- begin:: Aside -->
    <div class="kt-aside__brand kt-grid__item  " id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            <a href="{{ url('/cpadmin/dashboard') }}">
                <h3>EN</h3>
                <p>DATABASE</p>
            </a>
        </div>
    </div>
    <!-- end:: Aside -->

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu  kt-aside-menu--dropdown " data-ktmenu-vertical="1" data-ktmenu-dropdown="1" data-ktmenu-scroll="0">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__item @if(Request::is('cpadmin/dashboard') == true) kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ url('/cpadmin/dashboard') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-chart-bar"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
                @if(check_user_role(Session::get('admin_user_id'), 1, ''))
                <li class="kt-menu__item @if(Request::is('cpadmin/users-list/active') == true || Request::is('cpadmin/users-list/inactive') == true || Request::is('cpadmin/users-list/trashed') == true) kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ url('/cpadmin/users-list/active') }}" class="kt-menu__link"><i class="kt-menu__link-icon fa fa-list"></i><span class="kt-menu__link-text">Users List</span></a></li>
                @endif
                @if(check_user_role(Session::get('admin_user_id'), 5, ''))
                <li class="kt-menu__item @if(Request::is('cpadmin/messages-list') == true) kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ url('/cpadmin/messages-list') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-list"></i><span class="kt-menu__link-text">Messages</span></a></li>
                @endif
                @if(check_user_role(Session::get('admin_user_id'), 6, ''))
                <li class="kt-menu__item @if(Request::is('cpadmin/beneficiary-list') == true) kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ url('/cpadmin/beneficiary-list') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-list"></i><span class="kt-menu__link-text">Beneficiary</span></a></li>
                @endif
                <li class="kt-menu__item @if(Request::is('cpadmin/logout') == true) kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ url('/cpadmin/logout') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-settings"></i><span class="kt-menu__link-text">Logout</span></a></li>
            </ul>
        </div>
    </div>
    <!-- end:: Aside Menu -->
</div>