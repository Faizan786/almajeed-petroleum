<script type="text/javascript">
    $(document).ready(function(){
        windowload();
        
        $('body').on('click', '.kt-chat__reply', function(e){
            message = $('#message_input').val();
            beneficiary_id = $('#hidden_input_field #beneficiary_id').val();
            user_id = $('#hidden_input_field #user_id').val();
            chatsubmit(message, beneficiary_id, user_id);
            
            e.preventDefault();
            return false;
        });
        $('body').on('keydown', '#message_input', function(e){
            if (e.keyCode == 13) {
                message = $(this).val();
                beneficiary_id = $('#hidden_input_field #beneficiary_id').val();
                user_id = $('#hidden_input_field #user_id').val();
                chatsubmit(message, beneficiary_id, user_id);
                
                e.preventDefault();
                return false; 
            }
        });
        $('body').on('keyup', '#search_messages', function(){
            searchQuery = $(this).val();
            windowload(searchQuery);
        });
        $('body').on('click', '.actionClearMessages', function(){
            beneficiary_id = $(this).attr('data-id');
            if(confirm("Are you sure, you want to clear messages history for beneficiary#"+beneficiary_id+" ?")){
                $('.screenloader').show();
                $.post("{{ url('/cpadmin/messages-list/clearchat') }}", { _token: "{{csrf_token()}}", beneficiary:beneficiary_id }, function(result){
                    if(!result.error){
                        windowload();
                    } else {
                        alert(result.message);
                    }
                });
                $('.screenloader').hide();
            }
        });
        $('body').on('click', '.beneficiary_message_detail', function(){
            beneficiary_id = $(this).attr('data-beneficiary-id');
            user_id = $(this).attr('data-user-id');
            $(".beneficiary_message_detail").removeClass("beneficiary_message_active");
            $(this).addClass("beneficiary_message_active");
            
            windowloadchat(beneficiary_id, user_id);
            $('#message_input').val('');
        });
        
    });
    
    function chatsubmit(message="", beneficiary_id=0, user_id=0){
        $('.screenloader').show();
        $.post("{{ url('/cpadmin/messages-list/chatsubmit') }}", { _token: "{{csrf_token()}}", beneficiary:beneficiary_id, user:user_id, msg:message }, function(result){
            //console.log(result);
            if(!result.error){
                chatcode = '';
                chatcode += '<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user">';
                chatcode += '<span class="kt-chat__datetime">'+result.admin_created_at+'</span><a href="#" class="kt-chat__username">'+result.admin_username+'</a>';
                if(result.admin_photo == null || result.admin_photo.trim() == ""){
                    chatcode += '<span class="kt-userpic kt-userpic--circle kt-userpic--sm"><img src="{{ asset("cpadmin_assets/media/users/default.jpg") }}" alt="image"></span>';
                } else {
                    chatcode += '<span class="kt-userpic kt-userpic--circle kt-userpic--sm"><img src="{{ asset("uploaded_files/profile_avatars") }}/'+result.admin_photo+'" alt="image"></span>';
                }
                chatcode += '</div>';
                chatcode += '<div class="kt-chat__text kt-bg-light-brand">'+result.admin_message+'</div></div>';
                $('#kt-chat__messages').append(chatcode);
                $('.kt-scroll-messages-bottom').animate({ scrollTop: $('.kt-scroll-messages-bottom')[0].scrollHeight}, "slow");
            } else {
                alert(result.message);
            }
            $('#message_input').val('');
            $(".screenloader").hide();
        });
    };
    
    function windowloadchat(beneficiary_id="", user_id=""){
        $('#hidden_input_field').html('<input type="hidden" name="beneficiary_id" id="beneficiary_id" value="'+beneficiary_id+'" /> <input type="hidden" name="user_id" id="user_id" value="'+user_id+'" />');
        leftcode = '<div class="dropdown dropdown-inline"><button type="button" data-id="'+beneficiary_id+'" class="btn btn-clean btn-sm btn-icon btn-icon-md actionClearMessages" title="Click here to clear messages history for this beneficiary."><i class="flaticon-delete"></i></button></div>';
        $('.kt-chat__left').html(leftcode);
        rightcode = '<div class="dropdown dropdown-inline"><button type="button" data-id="'+beneficiary_id+'" class="btn btn-clean btn-sm btn-icon btn-icon-md actionview" data-toggle="modal" data-target="#kt_modal_view" aria-haspopup="true" aria-expanded="false" title="Click here to view beneficiary detail"><i class="flaticon-squares-1"></i></button></div>';
        $('.kt-chat__right').html(rightcode);
        $(".kt-scroll-messages-bottom").scrollTop(0);
        
        $('.screenloader').show();
        $.post("{{ url('/cpadmin/messages-list/chatload') }}", { _token: "{{csrf_token()}}", beneficiary:beneficiary_id, user:user_id }, function(result){
            //console.log(result);
            if(!result.error && result.beneficiary != null){
                titlecode = '<div class="kt-chat__label"><a class="kt-chat__title">'+ result.user.first_name +" "+ result.user.last_name +'</a><span class="kt-chat__status">Beneficiary# '+ beneficiary_id +' - '+ result.user.company +'</span></div>';
                $('.kt-chat__center').html(titlecode);
                chatcode = '';
                //console.log(result);
                $.each(result.message, function(index, item){
                    //console.log(index+" => "+item);
                    if(item.admin_id != null && item.admin_id > 0){
                        //console.log(item.admin_id);
                        chatcode += '<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user">';
                        chatcode += '<span class="kt-chat__datetime">'+item.created_at+'</span><a href="#" class="kt-chat__username">'+item.admin_username+'</a>';
                        if(item.admin_photo == null || item.admin_photo.trim() == ""){
                            chatcode += '<span class="kt-userpic kt-userpic--circle kt-userpic--sm"><img src="{{ asset("cpadmin_assets/media/users/default.jpg") }}" alt="image"></span>';
                        } else {
                            chatcode += '<span class="kt-userpic kt-userpic--circle kt-userpic--sm"><img src="{{ asset("uploaded_files/profile_avatars") }}/'+item.admin_photo+'" alt="image"></span>';
                        }
                        chatcode += '</div>';
                        chatcode += '<div class="kt-chat__text kt-bg-light-brand">'+item.message+'</div></div>';
                    }
                    if(item.user_id != null && item.user_id > 0){
                    //console.log(item.user_id);
                        chatcode += '<div class="kt-chat__message"><div class="kt-chat__user">';
                        if(item.client_photo == null || item.client_photo.trim() == ""){
                            chatcode += '<span class="kt-userpic kt-userpic--circle kt-userpic--sm"><img src="{{ asset("cpadmin_assets/media/users/default.jpg") }}" alt="image"></span>';
                        } else {
                            chatcode += '<span class="kt-userpic kt-userpic--circle kt-userpic--sm"><img src="{{ asset("uploaded_files/profile_avatars") }}/'+item.client_photo+'" alt="image"></span>';
                        }
                        chatcode += '<a href="#" class="kt-chat__username">'+item.client_username+'</a><span class="kt-chat__datetime">'+item.created_at+'</span>';
                        chatcode += '</div>';
                        chatcode += '<div class="kt-chat__text kt-bg-light-success">'+item.message+'</div></div>';
                    }
                });
                $('#kt-chat__messages').html(chatcode);
                $('.kt-scroll-messages-bottom').animate({ scrollTop: $('.kt-scroll-messages-bottom')[0].scrollHeight}, "slow");
            } else {
                if(!result.error && result.beneficiary != null){
                    alert(result.message);
                } else {
                
                }
            }
            $('.screenloader').hide();
        });
    }
    
    function windowload(searchQuery=""){
        $.post("{{ url('/cpadmin/messages-list/onload') }}", { _token: "{{csrf_token()}}", search:searchQuery }, function(result){
            var htmlcode = '';
            var beneid = '';
            var usrid = '';
            
            $.each(result, function(index, item){
                if(index == 0){
                    beneid = item.beneficiary_id;
                    if(item.user_id){
                        usrid = item.user_id;
                    } else {
                        usrid = item.admin_id;    
                    }
                }
                htmlcode += '<div class="kt-widget__item bene_message_detail" data-bene-id="'+item.beneficiary_id+'" data-user-id="'+item.user_id+'">';
                if(item.photo == null || item.photo.trim() == ""){
                    htmlcode += '<span class="kt-userpic kt-userpic--circle"><img src="{{ asset("cpadmin_assets/media/users/default.jpg") }}" alt="image"></span>';
                } else {
                    htmlcode += '<span class="kt-userpic kt-userpic--circle"><img src="{{ asset("uploaded_files/profile_avatars") }}/'+item.photo+'" alt="image"></span>';
                }
                htmlcode += '<div class="kt-widget__info"><div class="kt-widget__section">';
                htmlcode += '<a class="kt-widget__username">'+item.first_name+" "+item.last_name+'</a>';
                htmlcode += '<span class="kt-badge kt-badge--success kt-badge--dot"></span>';
                htmlcode += '</div>';
                htmlcode += '<span class="kt-widget__desc">'+item.company+" - Beneficiary#"+item.beneficiary_id+'</span></div>';
                htmlcode += '<div class="kt-widget__action">';
                htmlcode += '<span class="kt-widget__date">'+item.created_at+'</span>';
                if(item.unread_msg_count > 0){
                    htmlcode += '<span class="kt-badge kt-badge--success kt-font-bold">'+item.unread_msg_count+'</span>';
                }
                htmlcode += '</div></div>';
            });
            $('#messages_users_list').html(htmlcode);
            windowloadchat(beneid, usrid);
            $(".beneficiary_message_detail").filter( "[data-bene-id~='"+beneid+"']" ).addClass("beneficiary_message_active");
        });
    }
</script>