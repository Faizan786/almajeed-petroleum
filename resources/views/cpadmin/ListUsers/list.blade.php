@extends('cpadmin.layouts.default')
@section('content')

@include('cpadmin.ListUsers.include.includedjs')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    List User
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total"> {{ session::get('admin_username') }} </span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    @if(check_user_role(Session::get('admin_user_id'), 2, ''))
                    <a href="{{ url('/cpadmin/users-list/active') }}" class="btn @if(Request::is('cpadmin/users-list/active') == true) kt-subheader__btn-primary @else kt-subheader__btn-secondary @endif">Active Users List</a>
                    @endif
                    @if(check_user_role(Session::get('admin_user_id'), 3, ''))
                    <a href="{{ url('/cpadmin/users-list/inactive') }}" class="btn @if(Request::is('cpadmin/users-list/inactive') == true) kt-subheader__btn-primary @else kt-subheader__btn-secondary @endif">Inactive Users List</a>
                    @endif
                    @if(check_user_role(Session::get('admin_user_id'), 4, ''))
                    <a href="{{ url('/cpadmin/users-list/trashed') }}" class="btn @if(Request::is('cpadmin/users-list/trashed') == true) kt-subheader__btn-primary @else kt-subheader__btn-secondary @endif">Trashed Users List</a>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid margin-top-60">
        <div class="kt-portlet kt-portlet--mobile">
            {{ message_display() }}
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Users List Management
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            @if(check_user_role(Session::get('admin_user_id'), 2, 'has_delete') || check_user_role(Session::get('admin_user_id'), 3, 'has_delete') || check_user_role(Session::get('admin_user_id'), 4, 'has_delete'))
                                @if($listType == "D")
                                <a href="#"  data-toggle="modal" data-target="#kt_modal_delete_everthing" data-backdrop="static" data-keyboard="false" class="btn btn-danger btn-elevate btn-icon-sm">
                                    <i class="la la-trash-o"></i> Empty Trash
                                </a>
                                @endif
                            @endif
                            
                            @if(check_user_role(Session::get('admin_user_id'), 2, 'has_create') || check_user_role(Session::get('admin_user_id'), 3, 'has_create') || check_user_role(Session::get('admin_user_id'), 4, 'has_create'))
                                <a href="#"  data-toggle="modal" data-target="#kt_modal_add_new" data-backdrop="static" data-keyboard="false" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus"></i> Add New
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <div class="table-responsive-xl">
                    <table class="table table-hover table-bordered table-striped" id="usersdatatableId">
                        <thead>
                            <tr>
                                <td colspan="6" align="left" style="border-right: none;">
                                    <label>
                                        Show 
                                        <select name="table_result_size" id="table_result_size">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                        </select> 
                                        entries
                                    </label>
                                </td>
                                <td colspan="2" align="right" style="border-left: none;">
                                    <input type="search" name="table_search" id="table_search" class="form-control form-control-sm" placeholder="Search Column">
                                </td>
                            </tr>
                            <tr>
                                <th scope="col" class="sort_column" data-col="id">ID</th>
                                <th scope="col" class="sort_column" data-col="fullname">Full Name</th>
                                <th scope="col" class="sort_column" data-col="username">Username</th>
                                <th scope="col" class="sort_column" data-col="email">Email Address</th>
                                <th scope="col" class="sort_column" data-col="created_at">Date</th>
                                <th scope="col" class="sort_column" data-col="user_type">User Type</th>
                                <th scope="col" class="sort_column" data-col="status">Status</th>
                                <th scope="col" >Action</th>
                            </tr>
                        </thead>
                        <tbody id="loadtablebody">
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" align="left" style="border-right: none; padding-top: 20px;">
                                    <div id="display_number_of_records"></div>
                                </td>
                                <td colspan="4" align="right" style="border-left: none;">
                                    <div id="pagination_show"></div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!--end: Datatable -->

            </div>
        </div>
    </div>

    <!-- end:: Content -->
</div>


<!--begin::Modal-->
@include('cpadmin.ListUsers.include.modal_add')
<!--end::Modal-->

<!--begin::Modal-->
@include('cpadmin.ListUsers.include.modal_edit')
<!--end::Modal-->

<!--begin::Modal-->
@include('cpadmin.ListUsers.include.modal_view')
<!--end::Modal-->

<!--begin::Modal-->
@include('cpadmin.ListUsers.include.modal_delete')
<!--end::Modal-->

<!--begin::Modal-->
@include('cpadmin.includes.spinner_modal')
<!--end::Modal-->

@endsection