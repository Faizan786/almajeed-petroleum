<script type="text/javascript">
    function loadView(type='', pagination_no='', page_size='', search='', sort_col=''){
        $('.screenloader').show();
        if(search == ""){
            search = $('#table_search').val();
        }
        if(page_size == ""){
            page_size = $('#table_result_size').val();
        }
        if(pagination_no == ""){
            //pagination_no = $('.page-link.active').attr('data-page');
        }
        
        $.post("{{ url('/cpadmin/users-list-data') }}", {_token:"{{csrf_token()}}", page:type, pageno:pagination_no, size:page_size, query_search:search, sorting:sort_col}, function(result){
            //console.log(result);
            if(!result.error && result.counter != ""){
                $('#pagination_show').html(result.pagination);
                $('#display_number_of_records').html(result.counter);
                var status = '';
                var usr_type = '';
                var htmlcode = '';
                var month = new Array();
                month[0] = "Jan"; month[1] = "Feb"; month[2] = "Mar"; month[3] = "Apr"; month[4] = "May"; month[5] = "Jun"; month[6] = "Jul"; month[7] = "Aug"; month[8] = "Sep"; month[9] = "Oct"; month[10] = "Nov"; month[11] = "Dec";
                
                $.each(result.object, function(index, val) {
                    if(val.status == "Y"){
                        status = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill"><a class="statuschange statuschange_'+val.id+'" data-id="'+val.id+'">Active</a></span>';
                    } else if(val.status == "N"){
                        status = '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill"><a class="statuschange statuschange_'+val.id+'" data-id="'+val.id+'">Inactive</a></span>';                        
                    } else if(val.status == "D"){
                        status = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill"><a class="statuschange statuschange_'+val.id+'" data-id="'+val.id+'">Deleted</a></span>';
                    }
                    if(val.email_verified == "No"){
                        status += '<br /><span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill"><a class="resendmail resendmail_'+val.id+'" data-id="'+val.id+'" style="cursor:pointer;">Resent Mail Verification</a></span>';
                    }
                    if(val.user_type == "superadmin"){
                        usr_type = '<span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-primary">Super Admin</span>';
                    } else if(val.user_type == "admin"){
                        usr_type = '<span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">Admin</span>';
                    } else if(val.user_type == "employee"){
                        usr_type = '<span class="kt-badge kt-badge--warning kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-warning">Employee</span>';
                    } else if(val.user_type == "user"){
                        usr_type = '<span class="kt-badge kt-badge--danger kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-danger">User</span>';
                    }
                    
                    @if(check_user_role(Session::get('admin_user_id'), 2, 'has_detail') || check_user_role(Session::get('admin_user_id'), 3, 'has_detail') || check_user_role(Session::get('admin_user_id'), 4, 'has_detail'))
                        buttons = '<a class="dropdown-item actionview" data-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_view"><i class="la la-search-plus"></i> View</a>';
                    @endif
                    @if(check_user_role(Session::get('admin_user_id'), 2, 'has_edit') || check_user_role(Session::get('admin_user_id'), 3, 'has_edit') || check_user_role(Session::get('admin_user_id'), 4, 'has_edit'))
                        buttons += '<a class="dropdown-item actionedit" data-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_edit" data-backdrop="static" data-keyboard="false"><i class="la la-edit"></i> Edit</a>';
                    @endif
                    @if(check_user_role(Session::get('admin_user_id'), 2, 'has_delete') || check_user_role(Session::get('admin_user_id'), 3, 'has_delete') || check_user_role(Session::get('admin_user_id'), 4, 'has_delete'))
                        if(val.status == "D"){
                            buttons += '<a class="dropdown-item actionPermanentlyDel" data-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_permanently_delete"><i class="la la-trash-o"></i> Delete Permanent</a>';
                        } else {
                            buttons += '<a class="dropdown-item actiondelete" data-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_delete"><i class="la la-trash-o"></i> Trash</a>';
                        }
                    @endif
                    var actionBtn = '<span class="dropdown">';
                    actionBtn += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false"><i class="la la-bars"></i></a>';
                    actionBtn += '<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">';
                    actionBtn += buttons;
                    actionBtn += '</div></span>';
                    
                    dbdate = "---";
                    if(val.created_at != "0000-00-00 00:00:00"){
                        var created_at = new Date(val.created_at);
                        dbdate = (month[created_at.getMonth()]) +' '+ created_at.getDate() +', '+ created_at.getFullYear();
                    }
                    htmlcode += '<tr id="row_' + val.id + '">';
                    htmlcode += '<th scope="row">' + val.id + '</th>';
                    htmlcode += '<td>' + val.fullname + '</td>';
                    htmlcode += '<td>' + val.username + '</td>';
                    htmlcode += '<td>' + val.email + '</td>';
                    htmlcode += '<td>' + dbdate +'</td>';
                    htmlcode += '<td>' + usr_type + '</td>';
                    htmlcode += '<td>' + status + '</td>';
                    htmlcode += '<td>' + actionBtn + '</td>';
                    htmlcode += '</tr>';
                });
                $('#loadtablebody').html(htmlcode);
            } else {
                if(!result.message){
                    $('#loadtablebody').html('<tr><td align="center" colspan="8">No record founds!</td></tr>');
                } else {
                    $('#loadtablebody').html('<tr><td align="center" colspan="8">'+result.message+'</td></tr>');
                }
            }
            $('.screenloader').hide();
        });
    }
    
    jQuery(document).ready(function() {
        /*******Datatable Codes Start********/
        loadView('{{ $listType }}', '', '', '', '');
        $('body').on('keyup', '#table_search', function(){
            var search = $(this).val();
            loadView('{{ $listType }}', '', '', search, '');
        });
        $('body').on('change', '#table_result_size', function(){
            var size = $(this).val();
            loadView('{{ $listType }}', '', size, '', '');
        });
        $('body').on('click', '.page-link', function(){
            var id = $(this).attr('data-page');
            loadView('{{ $listType }}', id, '', '', '');
        });
        $('body').on('click', '.sort_column', function(){
            var col = $(this).attr('data-col');
            if($(this).hasClass('sort_desc')){
                $(".sort_column").removeClass("sort_desc");
                $(this).addClass('sort_asc');
                col += "~ASC";
            } else {
                $(".sort_column").removeClass("sort_asc");
                $(this).addClass('sort_desc');
                col += "~DESC";
            }
            loadView('{{ $listType }}', '', '', '', col);
        });
        /*******Datatable Codes End********/
        
        $('body').on('click', '.actionview', function(){
            var id = $(this).attr('data-id');
            $('#kt_modal_view .modal-body').hide();
            $('#kt_modal_view .spinnerbox').show();
            $.post("{{ url('/cpadmin/users-list/view') }}", {id:id, _token:"{{csrf_token()}}"}, function(result){
                if(!result.error){
                    var month = new Array();
                    month[0] = "Jan";
                    month[1] = "Feb";
                    month[2] = "Mar";
                    month[3] = "Apr";
                    month[4] = "May";
                    month[5] = "Jun";
                    month[6] = "Jul";
                    month[7] = "Aug";
                    month[8] = "Sep";
                    month[9] = "Oct";
                    month[10] = "Nov";
                    month[11] = "Dec";
                    
                    var usr_type = "";
                    var usr_status = "";
                    if(result.status == "Y"){
                        usr_status = "Active";                        
                    } else if(result.status == "N"){
                        usr_status = "Inactive";                        
                    } else if(result.status == "D"){
                        usr_status = "Delete";                        
                    }
                    
                    if(result.user_type == "superadmin"){
                        usr_type = "Super Admin";
                    } else if(result.user_type == "admin"){
                        usr_type = "Admin";
                    } else if(result.user_type == "employee"){
                        usr_type = "Employee";
                    } else if(result.user_type == "user"){
                        usr_type = "User";
                    }
                    if(result.photo != null && result.photo != undefined){
                        $('#kt_modal_view #photo').attr("src", "{{ url('uploaded_files/profile_avatars') }}/"+result.photo);
                    } else {
                        $('#kt_modal_view #photo').attr("src", "{{ url('cpadmin_assets/media/users/default.jpg') }}");
                    }
                    $('#kt_modal_view #first_name').html(result.first_name);
                    $('#kt_modal_view #last_name').html(result.last_name);
                    $('#kt_modal_view #email_address').html(result.email);
                    $('#kt_modal_view #username').html(result.username);
                    $('#kt_modal_view #company').html(result.company);
                    $('#kt_modal_view #phone_number').html(result.phone_number);
                    $('#kt_modal_view #user_type').html(usr_type);
                    $('#kt_modal_view #status').html(usr_status);
                    $('#kt_modal_view #email_verified_at').html(result.email_verified_at);
                    var created_at = new Date(result.created_at);
                    $('#kt_modal_view #created_at').html((month[created_at.getMonth()])+' '+created_at.getDate()+', '+created_at.getFullYear()+' '+created_at.getHours()+':'+created_at.getMinutes()+':'+created_at.getSeconds());
                    
                    $('#kt_modal_view .modal-body').show();
                    $('#kt_modal_view .spinnerbox').hide();
                }
            });
        });
        
        $('body').on('click', '.actionedit', function(){
            var id = $(this).attr('data-id');
            $('#kt_modal_edit .modal-body').hide();
            $('#kt_modal_edit .spinnerbox').show();
            $.post("{{ url('/cpadmin/users-list/view') }}", {id:id, _token:"{{csrf_token()}}"}, function(result){
                if(!result.error){
                    if(result.status == "Y"){
                        $("#kt_modal_edit #status_y").prop("checked", true);
                    } else {
                        $("#kt_modal_edit #status_n").prop("checked", true);
                    }
                    $("#kt_modal_edit #user_type").val(result.user_type);
                    if(result.photo != null && result.photo != undefined){
                        $('#kt_modal_edit #profile_avatar').attr("src", "{{ url('uploaded_files/profile_avatars') }}/"+result.photo);
                    } else {
                        $('#kt_modal_edit #profile_avatar').attr("src", "{{ url('cpadmin_assets/media/users/default.jpg') }}");
                    }
                    $('#kt_modal_edit #first_name').val(result.first_name);
                    $('#kt_modal_edit #last_name').val(result.last_name);
                    $('#kt_modal_edit #email').val(result.email);
                    $('#kt_modal_edit #username').val(result.username);
                    $('#kt_modal_edit #company').val(result.company);
                    $('#kt_modal_edit #phone_number').val(result.phone_number);
                    $('#kt_modal_edit #id').val(id);
                    $('#kt_modal_edit #edit_user_rights').html(result.user_rights);
                    
                    $('#kt_modal_edit .modal-body').show();
                    $('#kt_modal_edit .spinnerbox').hide();
                }
            });
            
        });
        $('body').on('click', '.actiondelete', function(){
            var val = $(this).attr('data-id');
            $('#kt_modal_delete #primary_id').val(val);
        });
        $('body').on('click', '.actionPermanentlyDel', function(){
            var val = $(this).attr('data-id');
            $('#kt_modal_permanently_delete #primary_id').val(val);
        });
        $('body').on('click', '.statuschange', function(){
            var id = $(this).attr('data-id');
            var value = $(this).text();
            
            $('.screenloader').show();
            $.post("{{ url('/cpadmin/users-list/changestatus') }}", {id:id, value:value, _token:"{{csrf_token()}}"}, function(result){
                if(result == "N"){
                    $('.statuschange_'+id).text('Inactive');
                    //$('.statuschange_'+id).parent().parent().parent().remove();
                } else if(result == "Y"){
                    $('.statuschange_'+id).text('Active');
                    //$('.statuschange_'+id).parent().parent().parent().remove();
                }
                $('.screenloader').hide();
                loadView('{{ $listType }}', '', '', '');
            });
        });
        $('body').on('click', '.resendmail', function(){
            var id = $(this).attr('data-id');
            $('.screenloader').show();
            $.post("{{ url('resend-mail-verification') }}", {id:id, _token:"{{csrf_token()}}"}, function(result){
                $('.screenloader').hide();
                alert(result.message);
            });
        });
        
        $('body').on('change', '.checkUncheck', function(){
            var id = $(this).attr('data-id');
            var pid = $(this).attr('data-parent');
            var gpid = $(this).attr('data-grand-parent');
            var type = $(this).attr('data-page-type');

            if($(this).is(':checked')){
                $('.id_'+id+':checkbox').prop('checked', true);
                $('.id_'+pid+':checkbox').prop('checked', true);
                $('.id_'+gpid+':checkbox').prop('checked', true);
                $('.pid_'+id+':checkbox').prop('checked', true);
                $('.gpid_'+id+':checkbox').prop('checked', true);
              } else {
                $('.id_'+id+':checkbox').prop('checked', false);
                $('.pid_'+id+':checkbox').prop('checked', false);
                $('.gpid_'+id+':checkbox').prop('checked', false);

                if($('.pid_'+pid+':checked').length == '0'){
                    $('.id_'+pid+':checkbox').prop('checked', false);
                }
                if($('.pid_'+gpid+':checked').length == '0'){
                    $('.id_'+gpid+':checkbox').prop('checked', false);
                }
            }
        });
        $('body').on('change', '.getValue', function(){
            var user_type = $(this).val();    
            if(user_type == 'Admin'){            
                $('.user_type:checkbox').prop('checked',true);
                //$('.user_type:checkbox').attr('disabled',true);
            } else if(user_type == 'Operator') {
                //$('.user_type:checkbox').attr('disabled',false);
                $('.user_type:checkbox').prop('checked',false);
            } else {
                //$('.user_type:checkbox').attr('disabled',false);
                $('.user_type:checkbox').prop('checked',true);
            }
        });
        
    });
    
    function CheckAll(){
        var ml = document.frmDirectory;
        var len = ml.elements.length;
        if (document.frmDirectory.AC.checked==true) {
            for (var i = 0; i < len; i++) {
               document.frmDirectory.elements[i].checked=true;
            }
        } else {
            for (var i = 0; i < len; i++)  {
              document.frmDirectory.elements[i].checked=false;
            }
        }
    }
    function CheckAll2(){
        var ml = document.frmDirectory2;
        var len = ml.elements.length;
        if (document.frmDirectory2.AC.checked==true) {
            for (var i = 0; i < len; i++) {
               document.frmDirectory2.elements[i].checked=true;
            }
        } else {
            for (var i = 0; i < len; i++)  {
              document.frmDirectory2.elements[i].checked=false;
            }
        }
    }
    function UnCheckAll() {
        var ml = document.frmDirectory;
        var len = ml.elements.length;
        var count=0; var checked=0;
        for (var i = 0; i < len; i++) {
            if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
                count = count + 1;
                if (document.frmDirectory.elements[i].checked == true){
                    checked = checked + 1;
                }
            }
         }
        if (checked == count){
            document.frmDirectory.AC.checked = true;
        } else {
            document.frmDirectory.AC.checked = false;
        }
    }
        
    function readURL(input, imageid) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(imageid)
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>