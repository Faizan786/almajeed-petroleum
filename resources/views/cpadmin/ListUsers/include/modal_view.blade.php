<div class="modal fade" id="kt_modal_view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="form-group col-md-12 spinnerbox">
                <div class="kt-section__content kt-section__content--solid">
                    <div class="kt-divider">
                        <span></span>
                        <span><button class="btn btn-success btn-icon btn-circle kt-spinner kt-spinner--v2 kt-spinner--center kt-spinner--sm kt-spinner--danger"></button></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label><b>Photo:</b></label>
                        <span><img id="photo" src="{{ url('/cpadmin_assets/media/users/default.jpg') }}" width="100" /></span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>First Name:</b></label>
                        <span id="first_name">&nbsp;</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Last Name:</b></label>
                        <span id="last_name">&nbsp;</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>Email Address:</b></label>
                        <span id="email_address">&nbsp;</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Username:</b></label>
                        <span id="username">&nbsp;</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>Company:</b></label>
                        <span id="company">&nbsp;</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Phone #:</b></label>
                        <span id="phone_number">&nbsp;</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>User Type:</b></label>
                        <span id="user_type">&nbsp;</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Status:</b></label>
                        <span id="status">&nbsp;</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>Email Verified at:</b></label>
                        <span id="email_verified_at">&nbsp;</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Registered at:</b></label>
                        <span id="created_at">&nbsp;</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>