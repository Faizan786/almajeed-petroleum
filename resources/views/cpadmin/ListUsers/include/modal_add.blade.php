<div class="modal fade" id="kt_modal_add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form name="frmDirectory" method="post" action="{{ url('/cpadmin/users-list/create') .'/'.$listType }}" enctype="multipart/form-data" class="kt-form kt-form--label-right">
                @csrf
                <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>First Name:</label>
                                    <input name="first_name" type="text" class="form-control" placeholder="Enter first name">
                                </div>
                                <div class="col-lg-6">
                                    <label>Last Name:</label>
                                    <input name="last_name" type="text" class="form-control" placeholder="Enter last name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Email Address:</label>
                                    <input name="email" type="email" class="form-control" placeholder="Enter your email address">
                                </div>
                                <div class="col-lg-6">
                                    <label>Username:</label>
                                    <input name="username" type="text" class="form-control" placeholder="Choose username">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Password:</label>
                                    <input name="password" type="password" class="form-control" placeholder="Enter password">
                                </div>
                                <div class="col-lg-6">
                                    <label>Confirm Password:</label>
                                    <input name="confirm_password" type="password" class="form-control" placeholder="Enter password again">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Company:</label>
                                    <input name="company" type="text" class="form-control" placeholder="Enter company name">
                                </div>
                                <div class="col-lg-6">
                                    <label>Phone#:</label>
                                    <input name="phone_number" type="tel" class="form-control" placeholder="Enter contact number">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>User Type:</label>
                                    <div class="kt-radio-inline">
                                        <select name="user_type" id="user_type" class="form-control">
                                            <option value="superadmin">Super Admin</option>
                                            <option value="admin">Admin</option>
                                            <option value="employee">Employee</option>
                                            <option value="user">User</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
                                        <div class="kt-avatar__holder">
                                            <img id="profile_avatar" src="{{ url('/cpadmin_assets/media/users/default.jpg') }}" width="115" />
                                        </div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-edit"></i>
                                            <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg, .gif" onchange="readURL(this, '#kt_modal_add_new #profile_avatar');">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label>User Rights:</label> <br />
                                    <input name="AC" type="checkbox" id="checkUncheckBoxall" onClick="CheckAll();"> <label for="checkUncheckBoxall">[Select All]</label>
                                    {!! user_roles_form_list(0,0,0,'') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancel </button>
                        <button type="submit" class="btn btn-primary"> Create </button>
                    </div>
                </form>
        </div>
    </div>
</div>