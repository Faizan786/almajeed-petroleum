@extends('cpadmin.layouts.default')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

        <!--Begin::Dashboard 3-->

        <!--Begin::Row-->
        <div class="row">
            <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">

                <!--begin:: Widgets/Trends-->
                <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Orders Stats
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fluid kt-portlet__body--fit">
                        <div class="kt-widget4 kt-widget4--sticky" style='display: block;'>
                            <div class="kt-widget4__items kt-widget4__items--bottom kt-portlet__space-x kt-margin-b-20">
                                <div class="kt-widget4__item">
                                    <div class="kt-widget4__img kt-widget4__img--logo">
                                        <img src="{{ asset('cpadmin_assets/media/client-logos/logo1.png') }}" alt="">
                                    </div>
                                    <div class="kt-widget4__info">
                                        <a href="#" class="kt-widget4__title">
                                            New Orders
                                        </a>
                                        <span class="kt-widget4__sub">
                                            New orders received and waiting for processing.
                                        </span>
                                    </div>
                                    <span class="kt-widget4__ext">
                                        <span class="kt-widget4__number kt-font-danger">1</span>
                                    </span>
                                </div>
                                <div class="kt-widget4__item">
                                    <div class="kt-widget4__img kt-widget4__img--logo">
                                        <img src="{{ asset('cpadmin_assets/media/client-logos/logo1.png') }}" alt="">
                                    </div>
                                    <div class="kt-widget4__info">
                                        <a href="#" class="kt-widget4__title">
                                            Processing Orders
                                        </a>
                                        <span class="kt-widget4__sub">
                                            Orders under processing and waiting for accounting.
                                        </span>
                                    </div>
                                    <span class="kt-widget4__ext">
                                        <span class="kt-widget4__number kt-font-danger">1</span>
                                    </span>
                                </div>
                                <div class="kt-widget4__item">
                                    <div class="kt-widget4__img kt-widget4__img--logo">
                                        <img src="{{ asset('cpadmin_assets/media/client-logos/logo1.png') }}" alt="">
                                    </div>
                                    <div class="kt-widget4__info">
                                        <a href="#" class="kt-widget4__title">
                                            Orders Accounting
                                        </a>
                                        <span class="kt-widget4__sub">
                                            Orders accounting where invoice generate.
                                        </span>
                                    </div>
                                    <span class="kt-widget4__ext">
                                        <span class="kt-widget4__number kt-font-danger">1</span>
                                    </span>
                                </div>
                                <div class="kt-widget4__item">
                                    <div class="kt-widget4__img kt-widget4__img--logo">
                                        <img src="{{ asset('cpadmin_assets/media/client-logos/logo1.png') }}" alt="">
                                    </div>
                                    <div class="kt-widget4__info">
                                        <a href="#" class="kt-widget4__title">
                                            Orders Unpaid
                                        </a>
                                        <span class="kt-widget4__sub">
                                            Orders unpaid and waiting for payment to be made by client.
                                        </span>
                                    </div>
                                    <span class="kt-widget4__ext">
                                        <span class="kt-widget4__number kt-font-danger">1</span>
                                    </span>
                                </div>
                                <div class="kt-widget4__item">
                                    <div class="kt-widget4__img kt-widget4__img--logo">
                                        <img src="{{ asset('cpadmin_assets/media/client-logos/logo1.png') }}" alt="">
                                    </div>
                                    <div class="kt-widget4__info">
                                        <a href="#" class="kt-widget4__title">
                                            Orders Paid
                                        </a>
                                        <span class="kt-widget4__sub">
                                            Orders paid by client.
                                        </span>
                                    </div>
                                    <span class="kt-widget4__ext">
                                        <span class="kt-widget4__number kt-font-danger">1</span>
                                    </span>
                                </div>
                                <div class="kt-widget4__item">
                                    <div class="kt-widget4__img kt-widget4__img--logo">
                                        <img src="{{ asset('cpadmin_assets/media/client-logos/logo1.png') }}" alt="">
                                    </div>
                                    <div class="kt-widget4__info">
                                        <a href="#" class="kt-widget4__title">
                                            Orders Completed
                                        </a>
                                        <span class="kt-widget4__sub">
                                            Orders completed and delivered.
                                        </span>
                                    </div>
                                    <span class="kt-widget4__ext">
                                        <span class="kt-widget4__number kt-font-danger">1</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end:: Widgets/Trends-->
            </div>

            <div class="col-xl-4 col-lg-4 order-lg-2 order-xl-1">

                <!--begin:: Widgets/New Users-->
                <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Users Verification Pending
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget4_tab1_content">
                                <div class="kt-widget4">
                                    @foreach($data['users'] as $key => $value)
                                    <div class="kt-widget4__item">
                                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                                            @if($value->photo != "")
                                            <img src="{{ asset('uploaded_files/profile_avatars/'.$value->photo) }}" alt="{{ $value->fullname }}">
                                            @else
                                            <img src="{{ asset('cpadmin_assets/media/users/default.jpg') }}" alt="{{ $value->fullname }}">
                                            @endif
                                        </div>
                                        <div class="kt-widget4__info">
                                            <a href="#" class="kt-widget4__username">
                                                {{ $value->fullname }}
                                            </a>
                                            <p class="kt-widget4__text">
                                                {{ $value->company }}
                                            </p>
                                        </div>
                                        <a href="{{ url('cpadmin/users-list/inactive') }}" target="_blank" title="Go to pending users list" class="btn btn-sm btn-label-brand btn-bold"><i class="flaticon2-link"></i> {{ date('M d, Y', strtotime($value->created_at)) }}</a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end:: Widgets/New Users-->
            </div>

            <div class="col-xl-4 col-lg-4 order-lg-2 order-xl-1">
                <!--Begin::Portlet-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Recent Orders Messages
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget3_tab2_content">

                                <!--Begin::Timeline 3 -->
                                <div class="kt-timeline-v3">
                                    <div class="kt-timeline-v3__items">
                                        @foreach($data['messages'] as $key => $value)
                                        <div class="kt-timeline-v3__item kt-timeline-v3__item--brand">
                                            <span class="kt-timeline-v3__item-time kt-font-primary">
                                                @if($value->photo != "")
                                                    <img src="{{ asset('uploaded_files/profile_avatars/'.$value->photo) }}" alt="{{ $value->fullname }}" width="40">
                                                @else
                                                    <img src="{{ asset('cpadmin_assets/media/users/default.jpg') }}" alt="{{ $value->fullname }}" width="40">
                                                @endif
                                            </span>
                                            <div class="kt-timeline-v3__item-desc">
                                                    <span class="kt-timeline-v3__item-text">
                                                        <a href="{{ url('cpadmin/messages-list') }}" target="_blank" title="Go to pending users list" class="btn btn-sm btn-label-brand btn-bold">
                                                            {{ $value->message }}
                                                        </a>
                                                    </span>
                                                <br />
                                                <span class="kt-timeline-v3__item-user-name">
                                                    <a href="{{ url('cpadmin/messages-list') }}" target="_blank" class="kt-link kt-link--dark kt-timeline-v3__itek-link">
                                                        By {{ select_field_id('users', $value->user_id, 'fullname') }} - Order#: {{ $value->order_id }} - {{ get_time_ago(strtotime($value->created_at)) }}
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>

                                <!--End::Timeline 3 -->
                            </div>
                        </div>
                    </div>
                </div>

                <!--End::Portlet-->
            </div>

        </div>

        <!--End::Row-->

        <!--End::Dashboard 3-->
    </div>

    <!-- end:: Content -->
</div>

@endsection