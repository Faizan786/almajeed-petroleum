<div class="modal fade" id="kt_modal_pay_stub" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form name="frmDirectory" method="post" action="{{ url('cpadmin/paystub-list/paystub/')}}" enctype="multipart/form-data" class="kt-form kt-form--label-right">
                @csrf
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="container">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Pay start date:</label>
                                    <input name="PayPeriodStart" type="text" class="form-control kt_datepicker_2" placeholder="mm/dd/yyyy" autocomplete="off" />
                                </div>
                                <div class="col-lg-6">
                                    <label>Pay end date:</label>
                                    <input name="PayPeriodEnd" type="text" class="form-control kt_datepicker_2" placeholder="mm/dd/yyyy" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Paid Date :</label>
                                    <input name="PaidDate" type="text" class="form-control kt_datepicker_2" placeholder="mm/dd/yyyy" autocomplete="off" />
                                </div>
                                <div class="col-lg-6">
                                    <label>Gross Earnings:</label>
                                    <input name="GrossEarnings" type="text" class="form-control" placeholder="Gross Earnings">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label>Net Earnings:</label>
                                    <input name="NetEarnings" type="text" class="form-control" placeholder="Net Earnings">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancel </button>
                        <button type="submit" class="btn btn-primary"> Create </button>
                        <input type="hidden" name="hire_id" id="hire_id" value="" />
                        <input type="hidden" name="beneficiary_id" id="beneficiary_id" value="" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
