<div class="modal fade" id="kt_modal_view_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="form-group col-md-12 spinnerbox">
                <div class="kt-section__content kt-section__content--solid">
                    <div class="kt-divider">
                        <span></span>
                        <span><button class="btn btn-success btn-icon btn-circle kt-spinner kt-spinner--v2 kt-spinner--center kt-spinner--sm kt-spinner--danger"></button></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label><b>Beneficiary:</b></label>
                            <div class="value" id="BeneficiaryLabel" onclick="return UpdateField('BeneficiaryLabel', 'BeneficiaryField')">&nbsp;</div>
                            <div id="BeneficiaryField" class="hide editableInput">
                                <?php $variable = "'kt_modal_view_detail', 'beneficiary_id', 'BeneficiaryLabel', 'BeneficiaryField'"; ?>
                                {!! select_dropdown('name="beneficiary_id" id="beneficiary_id" class="form-control" onchange="return UpdateValue('.$variable.')"', "beneficiary", array('is_trashed' => 'No'), 'FirstName', 'LastName') !!}
                            </div>
                        </div>
                        <div class="form-group col-md-6" >
                            <label><b>Job Type:</b></label>
                            <div class="value" id="JobTypeLabel" onclick="return UpdateField('JobTypeLabel', 'JobTypeField')">&nbsp;</div>
                            <div id="JobTypeField" class="hide editableInput">
                                <label class="kt-radio extendwidth kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="JobType" id="JobType_Full" value="Full Time" onclick="return UpdateValue('kt_modal_view_detail', 'JobType_Full', 'JobTypeLabel', 'JobTypeField')" /> Full Time
                                    <span></span>
                                </label>
                                <label class="kt-radio extendwidth kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="JobType" id="JobType_Part" value="Part Time" onclick="return UpdateValue('kt_modal_view_detail', 'JobType_Part', 'JobTypeLabel', 'JobTypeField')" /> Part Time
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label><b>Hourly Wages:</b></label>
                            <div class="value" id="HourlyWageLabel" onclick="return UpdateField('HourlyWageLabel', 'HourlyWageField')">&nbsp;</div>
                            <div id="HourlyWageField" class="hide editableInput">
                                <input type="text" name="HourlyWage" id="HourlyWage" class="form-control" placeholder="Hourly Wages...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'HourlyWage', 'HourlyWageLabel', 'HourlyWageField')">Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Employer:</b></label>
                            <div class="value" id="EmployerLabel" onclick="return UpdateField('EmployerLabel', 'EmployerField')">&nbsp;</div>
                            <div id="EmployerField" class="hide editableInput">
                                <input type="text" name="Employer" id="Employer" class="form-control" placeholder="Employer...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Employer', 'EmployerLabel', 'EmployerField')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label><b>Benefits:</b></label>
                            <div class="value" id="BenefitsLabel" onclick="return UpdateField('BenefitsLabel', 'BenefitsField')">&nbsp;</div>
                            <div id="BenefitsField" class="hide editableInput">
                                <input type="text" name="Benefits" id="Benefits" class="form-control" placeholder="Benefits...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Benefits', 'BenefitsLabel', 'BenefitsField')">Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b> Gross Earnings:</b></label>
                            <div class="value" id="GrossEarningsLabel" onclick="return UpdateField('GrossEarningsLabel', 'GrossEarningsField')">&nbsp;</div>
                            <div id="GrossEarningsField" class="hide editableInput">
                                <input type="text" name="GrossEarnings" id="GrossEarnings" class="form-control" placeholder="Gross Earnings...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'GrossEarnings', 'GrossEarningsLabel', 'GrossEarningsField')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label><b>Earning Structure :</b></label>
                            <div class="value" id="EarningStructureLabel" onclick="return UpdateField('EarningStructureLabel', 'EarningStructureField')">&nbsp;</div>
                            <div id="EarningStructureField" class="hide editableInput">
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="EarningStructure" id="EarningStructure_SSDI" value="SSDI" onclick="return UpdateValue('kt_modal_view_detail', 'EarningStructure_SSDI', 'EarningStructureLabel', 'EarningStructureField')" /> SSDI
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="EarningStructure" id="EarningStructure_SSI" value="SSI" onclick="return UpdateValue('kt_modal_view_detail', 'EarningStructure_SSI', 'EarningStructureLabel', 'EarningStructureField')" /> SSI
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="EarningStructure" id="EarningStructure_Both" value="Both" onclick="return UpdateValue('kt_modal_view_detail', 'EarningStructure_Both', 'EarningStructureLabel', 'EarningStructureField')" /> Both
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Address:</b></label>
                            <div class="value" id="AddressLabel" onclick="return UpdateField('AddressLabel', 'AddressField')">&nbsp;</div>
                            <div id="AddressField" class="hide editableInput">
                                <input type="text" name="Address" id="Address" class="form-control" placeholder="Address...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Address', 'AddressLabel', 'AddressField')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="PayStart"><b>Pay Start Date:</b></label>
                            <div class="value" id="PayStartLabel" onclick="return UpdateField('PayStartLabel', 'PayStartField')">&nbsp;</div>
                            <div id="PayStartField" class="hide editableInput">
                                <input type="text" name="PayStart" id="PayStart" class="form-control kt_datepicker_2" placeholder="Search for...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PayStart', 'PayStartLabel', 'PayStartField')">Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="PayEnd"><b>Pay End Date:</b></label>
                            <div class="value" id="PayEndLabel" onclick="return UpdateField('PayEndLabel', 'PayEndField')">&nbsp;</div>
                            <div id="PayEndField" class="hide editableInput">
                                <input type="text" name="PayEnd" id="PayEnd" class="form-control kt_datepicker_2" placeholder="Search for...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PayEnd', 'PayEndLabel', 'PayEndField')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="PaidDate"><b>Paid Date:</b></label>
                            <div class="value" id="PaidDateLabel" onclick="return UpdateField('PaidDateLabel', 'PaidDateField')">&nbsp;</div>
                            <div id="PaidDateField" class="hide editableInput">
                                <input type="text" name="PaidDate" id="PaidDate" class="form-control kt_datepicker_2" placeholder="Search for...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PaidDate', 'PaidDateLabel', 'PaidDateField')">Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="DateOfHire"><b>Date Of Hire:</b></label>
                            <div class="value" id="DateOfHireLabel" onclick="return UpdateField('DateOfHireLabel', 'DateOfHireField')">&nbsp;</div>
                            <div id="DateOfHireField" class="hide editableInput">
                                <input type="text"  name="DateOfHire" id="DateOfHire" class="form-control kt_datepicker_2" placeholder="Search for...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'DateOfHire', 'DateOfHireLabel', 'DateOfHireField')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label><b>TWL:</b></label>
                            <div class="value" id="TWLLabel" onclick="return UpdateField('TWLLabel', 'TWLField')">&nbsp;</div>
                            <div id="TWLField" class="hide editableInput">
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="TWL" id="TWL_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'TWL_Yes', 'TWLLabel', 'TWLField')" /> Yes
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="TWL" id="TWL_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'TWL_No', 'TWLLabel', 'TWLField')" /> No
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>SGA :</b></label>
                            <div class="value" id="SGALabel" onclick="return UpdateField('SGALabel', 'SGAField')">&nbsp;</div>
                            <div id="SGAField" class="hide editableInput">
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="SGA" id="SGA_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'SGA_Yes', 'SGALabel', 'SGAField')" /> Yes
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="SGA" id="SGA_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'SGA_No', 'SGALabel', 'SGAField')" /> No
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="hidden" id="primary_id" value="" />
                </div>
            </div>
        </div>
    </div>
</div>
