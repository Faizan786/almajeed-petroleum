<script type="text/javascript">
    function loadView(type='', pagination_no='', page_size='', search='', sort_col=''){
        $('.screenloader').show();
        if(search == ""){
            search = $('#table_search').val();
        }
        if(page_size == ""){
            page_size = $('#table_result_size').val();
        }
        if(pagination_no == ""){
            //pagination_no = $('.page-link.active').attr('data-page');
        }

        $.post("{{ url('/cpadmin/hire-list-data') }}", {_token:"{{csrf_token()}}", page:type, pageno:pagination_no, size:page_size, query_search:search, sorting:sort_col}, function(result){
            //console.log(result);
            if(!result.error && result.counter != ""){
                $('#pagination_show').html(result.pagination);
                $('#display_number_of_records').html(result.counter);
                 var htmlcode = '';
                var month = new Array();
                month[0] = "Jan"; month[1] = "Feb"; month[2] = "Mar"; month[3] = "Apr"; month[4] = "May"; month[5] = "Jun"; month[6] = "Jul"; month[7] = "Aug"; month[8] = "Sep"; month[9] = "Oct"; month[10] = "Nov"; month[11] = "Dec";

                $.each(result.object, function(index, val) {
                    var buttons = "";
                    buttons += '<a class="dropdown-item actionview" data-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_view_detail" data-backdrop="static" data-keyboard="false"><i class="la la-search-plus"></i> View</a>';
                    buttons += '<a class="dropdown-item actionPayStub" data-hire-id="' + val.id + '" data-beneficiary-id="'+ val.beneficiary_id +'" data-toggle="modal" data-target="#kt_modal_pay_stub" data-backdrop="static" data-keyboard="false"><i class="la la-search-plus"></i> Pay Stub</a>';
                    buttons += '<a class="dropdown-item actionReport" href="{{ url('cpadmin/paystub-list/export') }}/'+ val.id +'" target="_blank"><i class="la la-search-plus"></i> Report</a>';
                    buttons += '<a class="dropdown-item actionTrash" data-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_trash"><i class="la la-trash-o"></i> Trash</a>';

                    var actionBtn = '<span class="dropdown">';
                    actionBtn += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false"><i class="la la-bars"></i></a>';
                    actionBtn += '<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">';
                    actionBtn += buttons;
                    actionBtn += '</div></span>';

                    dbdate = "---";
                    if(val.DateOfHire != "0000-00-00 00:00:00"){
                        var created_at = new Date(val.DateOfHire);
                        dbdate = (month[created_at.getMonth()]) +' '+ created_at.getDate() +', '+ created_at.getFullYear();
                    }
                    htmlcode += '<tr id="row_' + val.id + '">';
                    htmlcode += '<th scope="row">' + val.id + '</th>';
                    htmlcode += '<td>' + val.FirstName +" "+ val.LastName + '</td>';
                    htmlcode += '<td>' + val.Employer +'</td>';
                    htmlcode += '<td>' + val.JobType + '</td>';
                    htmlcode += '<td>$' + val.HourlyWage + '</td>';
                    htmlcode += '<td>' + val.Benefits +'</td>';
                    htmlcode += '<td>' + dbdate + '</td>';
                    htmlcode += '<td>' + actionBtn + '</td>';
                    htmlcode += '</tr>';
                });
                $('#loadtablebody').html(htmlcode);
            } else {
                if(!result.message){
                    $('#loadtablebody').html('<tr><td align="center" colspan="8">No record founds!</td></tr>');
                } else {
                    $('#loadtablebody').html('<tr><td align="center" colspan="8">'+result.message+'</td></tr>');
                }
            }
            $('.screenloader').hide();
        });
    }

    jQuery(document).ready(function() {
        /*******Datatable Codes Start********/
        loadView('{{ $listType }}', '', '', '', '');
        $('body').on('keyup', '#table_search', function(){
            var search = $(this).val();
            loadView('{{ $listType }}', '', '', search, '');
        });
        $('body').on('change', '#table_result_size', function(){
            var size = $(this).val();
            loadView('{{ $listType }}', '', size, '', '');
        });
        $('body').on('click', '.page-link', function(){
            var id = $(this).attr('data-page');
            loadView('{{ $listType }}', id, '', '', '');
        });
        $('body').on('click', '.sort_column', function(){
            var col = $(this).attr('data-col');
            if($(this).hasClass('sort_desc')){
                $(".sort_column").removeClass("sort_desc");
                $(this).addClass('sort_asc');
                col += "~ASC";
            } else {
                $(".sort_column").removeClass("sort_asc");
                $(this).addClass('sort_desc');
                col += "~DESC";
            }
            loadView('{{ $listType }}', '', '', '', col);
        });
        /*******Datatable Codes End********/

        $('body').on('click', '.actionview', function(){
            var id = $(this).attr('data-id');
            $('#kt_modal_view .modal-body').hide();
            $('#kt_modal_view .spinnerbox').show();
            $.post("{{ url('/cpadmin/hire-list/view') }}", {id:id, _token:"{{csrf_token()}}"}, function(result){
                if(!result.error){
                    var month = new Array();
                    month[0] = "Jan";
                    month[1] = "Feb";
                    month[2] = "Mar";
                    month[3] = "Apr";
                    month[4] = "May";
                    month[5] = "Jun";
                    month[6] = "Jul";
                    month[7] = "Aug";
                    month[8] = "Sep";
                    month[9] = "Oct";
                    month[10] = "Nov";
                    month[11] = "Dec";
                    $('#kt_modal_view_detail #primary_id').val(result.id);
                    $('#kt_modal_view_detail #BeneficiaryLabel').html(result.beneficiary);
                    $('#kt_modal_view_detail #beneficiary_id').val(result.beneficiary_id);
                    $('#kt_modal_view_detail #HourlyWageLabel').html(result.HourlyWage);
                    $('#kt_modal_view_detail #HourlyWage').val(result.HourlyWage);
                    $('#kt_modal_view_detail #EmployerLabel').html(result.Employer);
                    $('#kt_modal_view_detail #Employer').val(result.Employer);
                    $('#kt_modal_view_detail #BenefitsLabel').html(result.Benefits);
                    $('#kt_modal_view_detail #Benefits').val(result.Benefits);
                    $('#kt_modal_view_detail #AddressLabel').html(result.Address);
                    $('#kt_modal_view_detail #Address').val(result.Address);
                    $('#kt_modal_view_detail #PayStartLabel').html(result.PayStart);
                    $('#kt_modal_view_detail #PayStart').val(result.PayStart);
                    $('#kt_modal_view_detail #PayEndLabel').html(result.PayEnd);
                    $('#kt_modal_view_detail #PayEnd').val(result.PayEnd);
                    $('#kt_modal_view_detail #PaidDateLabel').html(result.PaidDate);
                    $('#kt_modal_view_detail #PaidDate').val(result.PaidDate);
                    $('#kt_modal_view_detail #GrossEarningsLabel').html(result.GrossEarnings);
                    $('#kt_modal_view_detail #GrossEarnings').val(result.GrossEarnings);
                    $('#kt_modal_view_detail #JobTypeLabel').html(result.JobType);
                    $('#kt_modal_view_detail #JobType').val(result.JobType);
                    if(result.JobType == "Full Time"){
                        $('#kt_modal_view_detail #JobType_Full').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #JobType_Part').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #EarningStructureLabel').html(result.EarningStructure);
                    if(result.EarningStructure == "SSDI"){
                        $('#kt_modal_view_detail #EarningStructure_SSDI').prop("checked", true);
                    } else if(result.EarningStructure == "SSI"){
                        $('#kt_modal_view_detail #EarningStructure_SSI').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #EarningStructure_Both').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #TWLLabel').html(result.TWL);
                    if(result.TWL == "Yes"){
                        $('#kt_modal_view_detail #TWL_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #TWL_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #SGALabel').html(result.SGA);
                    if(result.SGA == "Yes"){
                        $('#kt_modal_view_detail #SGA_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #SGA_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #DateOfHireLabel').html(result.DateOfHire);
                    $('#kt_modal_view_detail #DateOfHire').val(result.DateOfHire);
                    var created_at = new Date(result.created_at);
                    $('#kt_modal_view_detail #created_at').html((month[created_at.getMonth()])+' '+created_at.getDate()+', '+created_at.getFullYear()+' '+created_at.getHours()+':'+created_at.getMinutes()+':'+created_at.getSeconds());

                    $('#kt_modal_view .modal-body').show();
                    $('#kt_modal_view .spinnerbox').hide();
                }
            });
        });
        
        $('body').on('click', '.actionTrash', function(){
            var val = $(this).attr('data-id');
            $('#kt_modal_trash #primary_id').val(val);
        });
        $('body').on('click', '.actionPayStub', function(){
            var val1 = $(this).attr('data-hire-id');
            var val2 = $(this).attr('data-beneficiary-id');
            $('#kt_modal_pay_stub #hire_id').val(val1);
            $('#kt_modal_pay_stub #beneficiary_id').val(val2);
        });

    });

    function readURL(input, imageid) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(imageid)
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function UpdateField(selecter, field) {
        $('#'+selecter).hide();
        $('#'+field).show();
    }
    function UpdateValue(page, idField, selecter, field) {
        var primary_id = $('#'+page+' #primary_id').val();
        var fieldvalue = $('#'+page+' #'+idField).val();
        var fieldname = $('#'+page+' #'+idField).attr('name');
        
        $.post("{{ url('cpadmin/hire-list/update') }}", {id:primary_id, name:fieldname, value:fieldvalue, _token:"{{csrf_token()}}"}, function(result){
            if(!result.error){
                if($('#'+page+' #'+idField+" option:selected").text()){
                    $('#'+page+' #'+selecter).text($('#'+page+' #'+idField+" option:selected").text());
                } else {
                    $('#'+page+' #'+selecter).text(fieldvalue);
                }
                $('#'+page+' #'+selecter).show();
                $('#'+page+' #'+field).hide();
            } else {
                alert(result.message);
            }
        });
    }
</script>
