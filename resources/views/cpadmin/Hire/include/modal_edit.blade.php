<div class="modal fade" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form name="frmDirectory" method="post" action="{{ url('cpadmin/hire/create/')}}" enctype="multipart/form-data" class="kt-form kt-form--label-right">
                @csrf
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="container">
                            <div class="form-group row">
                                <label>Date Of Hire:</label>
                                <input name="FirstName" type="date" id="FirstName" class="form-control" placeholder="Enter first name">
                            </div>
                            <div class="form-group row">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-info" type="button">Full Time</button>
                                        <button class="btn btn-outline-secondary" type="button">Part Time</button>
                                    </div>
                                    <div class="col-3  float-right">
                                        <input name="Address" id="Address" type="text" class="form-control" placeholder="Enter Address">
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label>Address:</label>
                                <input name="Address" id="Address" type="text" class="form-control" placeholder="Enter Address">
                            </div>

                            <div class="form-group row">
                                <label>Phone:</label>
                                <input name="PhoneNumber" id="PhoneNumber" type="text" class="form-control" placeholder="Enter Phone">
                            </div>

                            <div class="form-group row">
                                <label>Email:</label>
                                <input name="Email" id="Email" type="text" class="form-control" placeholder="Enter Email">
                            </div>

                            <div class="form-group row">
                                <label>SSA:</label>
                                <input name="SSA" id="SSA" type="text" class="form-control" placeholder="Enter Email">
                            </div>

                            <div class="form-group row">
                                <label>Date Ticket Assigned:</label>
                                <input name="DateTicketAssigned" id="DateTicketAssigned" type="text" class="form-control" placeholder="Enter Email">
                            </div>

                            <div class="form-group row">
                                <label>Disabilities:</label>
                                <input name="Disabilities" id="Disabilities" type="text" class="form-control" placeholder="Enter Disabilities">
                            </div>

                            <div class="form-group row">
                                <label>Physical Restructions:</label>
                                <input name="PhysicalRestrictions" id="PhysicalRestrictions" type="text" class="form-control" placeholder="Physical Restructions">
                            </div>

                            <div class="form-group row">
                                <label>Job Goals:</label>
                                <input name="JobGoals"  id="JobGoals" type="text" class="form-control" placeholder="Job Goals">
                            </div>
                            {{--options --}}

                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <h5> ROI on File:</h5><br>
                                    <label><input type="radio" name="ROIOnFile"  id="ROIOnFile" value="Yes"  checked>Yes</label>
                                    <label><input type="radio" name="ROIOnFile" id="ROIOnFile"  >No</label>
                                </div>
                                <div class="col-lg-6">
                                    <h5>Release to verify wages:</h5><br>
                                    <label><input type="radio" name="ReleaseToVerifyWages"  id="ReleaseToVerifyWages" checked>Yes</label>
                                    <label><input type="radio" name="ReleaseToVerifyWages" id="ReleaseToVerifyWages"  >No</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <h5>Benifit Counselling</h5><br>
                                    <label><input type="radio" name="BenefitsCounseling" id="BenefitsCounseling"  value="Yes" checked>Yes</label>
                                    <label><input type="radio" name="BenefitsCounseling" id="BenefitsCounseling"  >No</label>
                                </div>
                                <div class="col-lg-6">
                                    <h5>Soft Skills:</h5><br>
                                    <label><input type="radio" name="SoftSkills" value="Yes" id="SoftSkills" checked>Yes</label>
                                    <label><input type="radio" name="SoftSkills"  id="SoftSkills">No</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <h5>JSS:</h5><br>
                                    <label><input type="radio" name="JSS"  value="Yes" id="JSS" checked>Yes</label>
                                    <label><input type="radio" name="JSS" id="JSS" >No</label>
                                </div>
                                <div class="col-lg-6">
                                    <h5>Job Search:</h5><br>
                                    <label><input type="radio" name="JobSearch" value="Yes" id="JobSearch">Yes</label>
                                    <label><input type="radio" name="JobSearch" id="JobSearch" >No</label>
                                </div>
                            </div>
                            {{--end options--}}
                            <div class="form-group row">
                                <label>Retention:</label>
                                <input name="Retention" id="Retention" type="text" class="form-control" placeholder="Physical Restructions">
                            </div>

                            <div class="alert alert-success">
                                <h5>Does EN Report Earnings</h5><br>
                                <label><input type="radio" name="Retention"  id="Retention" checked>Yes</label>
                                <label><input type="radio" name="optradio" id="Retention" >No</label>
                            </div>
                            <br>
                            <h4>Reportings: &nbsp; <button class="btn btn-info"><i class="fa fa-plus"></i> Add Notes </button> </h4>
                            <br>
                            <div class="col-sm-3">
                                <small>2017-8-10</small>
                                <div class="you-message">
                                    asfdasff
                                </div>
                            </div>
                        </div>




                        {{--<div class="form-group row">--}}
                        {{--<div class="col-lg-12">--}}
                        {{--<label>User Rights:</label> <br />--}}
                        {{--<input name="AC" type="checkbox" id="checkUncheckBoxall" onClick="CheckAll();"> <label for="checkUncheckBoxall">[Select All]</label>--}}
                        {{--{!! user_roles_form_list(0,0,0,'') !!}--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancel </button>
                    <button type="submit" class="btn btn-primary"> Create </button>
                </div>
            </form>
        </div>
    </div>
</div>
