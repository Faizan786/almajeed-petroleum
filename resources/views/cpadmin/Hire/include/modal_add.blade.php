<div class="modal fade" id="kt_modal_add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form name="frmDirectory" method="post" action="{{ url('cpadmin/hire-list/create/')}}" enctype="multipart/form-data" class="kt-form kt-form--label-right">
                @csrf
                <div class="modal-body">
                <div class="kt-portlet__body">
                    <div class="container">
                        <div class="form-group row ">
                            <div class="col-lg-6">
                                <label>Job Type:</label><br />
                                <input name="JobType" data-switch="true" type="checkbox" checked="checked" data-on-text="Full Time" data-handle-width="170" data-off-text="Part Time" data-on-color="brand" />
                            </div>
                            <div class=" col-sm-6">
                                <label>Hourly Wages:</label>
                                <input type="text" name="HourlyWage" class="form-control" placeholder="Hourly Wages" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Beneficiary:</label>
                                {!! select_dropdown_with_brackets("beneficiary_id", "beneficiary_id", "form-control", "", "beneficiary", array('is_trashed' => 'No'), "FirstName", "LastName", "SSN") !!}
                            </div>
                            <div class="col-lg-6">
                                <label>Employer:</label>
                                <input name="Employer" type="text" class="form-control" placeholder="Employeer" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label>Address:</label>
                                <input name="Address" type="text" class="form-control" placeholder="Address" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Pay Start Date:</label>
                                <input type="text" name="PayStart" class="form-control kt_datepicker_2" placeholder="mm/dd/yyyy" autocomplete="off" />
                            </div>
                            <div class=" col-lg-6">
                                <label class="">Pay End Date:</label>
                                <input type="text" name="PayEnd" class="form-control kt_datepicker_2" placeholder="mm/dd/yyyy" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Paid Date:</label>
                                <input type="text" name="PaidDate" class="form-control kt_datepicker_2" placeholder="mm/dd/yyyy" autocomplete="off" />
                            </div>
                            <div class="col-lg-6">
                                <label>Date Of Hire:</label>
                                <input type="text" name="DateOfHire" class="form-control kt_datepicker_2" placeholder="mm/dd/yyyy" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Gross Earning:</label>
                                <input name="GrossEarnings" type="text" class="form-control" placeholder="Gross Earning" />
                            </div>
                            <div class="col-lg-6">
                                <label>Benefits:</label>
                                <input name="Benefits" type="text" class="form-control" placeholder="Benefits" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label>Earning Structure:</label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="EarningStructure" value="SSD" />SSD
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="EarningStructure" value="SSI" />SSI
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="EarningStructure" value="Both" checked>Both
                                    <span></span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label>At TWL:</label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="TWL" value="Yes" checked /> Yes
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input type="radio" name="TWL" value="No" /> No
                                    <span></span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label>At SSG</label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger kt-margin-r-30">
                                    <input type="radio" name="SGA" value="Yes" checked /> Yes
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--danger">
                                    <input name="SGA" type="radio" value="No" /> No
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cancel </button>
                    <button type="submit" class="btn btn-primary"> Create </button>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>
