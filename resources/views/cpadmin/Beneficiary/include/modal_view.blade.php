<div class="modal fade" id="kt_modal_view_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Beneficiary Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="form-group col-md-12 spinnerbox">
                <div class="kt-section__content kt-section__content--solid">
                    <div class="kt-divider">
                        <span></span>
                        <span><button class="btn btn-success btn-icon btn-circle kt-spinner kt-spinner--v2 kt-spinner--center kt-spinner--sm kt-spinner--danger"></button></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form name="updateformdata" id="updateformdata" method="POST" action="{{ url('/cpadmin/beneficiary-list/update') }}">
                @csrf
                <div class="kt-portlet__body">

                    <ul class="nav nav-tabs nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link fill_view_detail first active" data-toggle="tab" data-id="" data-page="" data-panel="kt_tabs_1_1">Beneficiary Detail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fill_view_detail" data-toggle="tab" data-id="" data-page="" data-panel="kt_tabs_1_2">Hire Information</a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>First Name:</b></label>
                                    <!--<div class="value" id="FirstNameLabel" onclick="return UpdateField('FirstNameLabel', 'FirstNameField')">&nbsp;</div>-->
                                    <div id="FirstNameField" class="editableInput">
                                        <input type="text" name="FirstName" id="FirstName" class="form-control" placeholder="First Name ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'FirstName', 'FirstNameLabel', 'FirstNameField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b>Last Name:</b></label>
                                    <!--<div class="value" id="LastNameLabel" onclick="return UpdateField('LastNameLabel', 'LastNameField')">&nbsp;</div>-->
                                    <div id="LastNameField" class="editableInput">
                                        <input type="text" name="LastName" id="LastName" class="form-control" placeholder="Last Name ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'LastName', 'LastNameLabel', 'LastNameField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>SSN:</b></label>
                                    <!--<div class="value" id="SSNLabel" onclick="return UpdateField('SSNLabel', 'SSNField')">&nbsp;</div>-->
                                    <div id="SSNField" class="editableInput">
                                        <input type="text" name="SSN" id="SSN" class="form-control kt_inputmast_ssn" placeholder="Social Security Number ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'SSN', 'SSNLabel', 'SSNField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b>Address:</b></label>
                                    <!--<div class="value" id="AddressLabel" onclick="return UpdateField('AddressLabel', 'AddressField')">&nbsp;</div>-->
                                    <div id="AddressField" class="editableInput">
                                        <input type="text" name="Address" id="Address" class="form-control" placeholder="Address ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Address', 'AddressLabel', 'AddressField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>Phone Number:</b></label>
                                    <!--<div class="value" id="PhoneNumberLabel" onclick="return UpdateField('PhoneNumberLabel', 'PhoneNumberField')">&nbsp;</div>-->
                                    <div id="PhoneNumberField" class="editableInput">
                                        <input type="text" name="PhoneNumber" id="PhoneNumber" class="form-control kt_inputmast_phone" placeholder="Phone Number ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PhoneNumber', 'PhoneNumberLabel', 'PhoneNumberField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b>Email:</b></label>
                                    <!--<div class="value" id="EmailLabel" onclick="return UpdateField('EmailLabel', 'EmailField')">&nbsp;</div>-->
                                    <div id="EmailField" class="editableInput">
                                        <input type="text" name="Email" id="Email" class="form-control" placeholder="Email Address ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Email', 'EmailLabel', 'EmailField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>Disabilities :</b></label>
                                    <!--<div class="value" id="DisabilitiesLabel" onclick="return UpdateField('DisabilitiesLabel', 'DisabilitiesField')">&nbsp;</div>-->
                                    <div id="DisabilitiesField" class="editableInput">
                                        <input type="text" name="Disabilities" id="Disabilities" class="form-control" placeholder="Disabilities ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Disabilities', 'DisabilitiesLabel', 'DisabilitiesField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b>Physical Restrictions:</b></label>
                                    <!--<div class="value" id="PhysicalRestrictionsLabel" onclick="return UpdateField('PhysicalRestrictionsLabel', 'PhysicalRestrictionsField')">&nbsp;</div>-->
                                    <div id="PhysicalRestrictionsField" class="editableInput">
                                        <input type="text" name="PhysicalRestrictions" id="PhysicalRestrictions" class="form-control" placeholder="Physical Restrictions ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PhysicalRestrictions', 'PhysicalRestrictionsLabel', 'PhysicalRestrictionsField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>Job Goals:</b></label>
                                    <!--<div class="value" id="JobGoalsLabel" onclick="return UpdateField('JobGoalsLabel', 'JobGoalsField')">&nbsp;</div>-->
                                    <div id="JobGoalsField" class="editableInput">
                                        <input type="text" name="JobGoals" id="JobGoals" class="form-control" placeholder="Job Goals ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'JobGoals', 'JobGoalsLabel', 'JobGoalsField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b>Retention:</b></label>
                                    <!--<div class="value" id="RetentionLabel" onclick="return UpdateField('RetentionLabel', 'RetentionField')">&nbsp;</div>-->
                                    <div id="RetentionField" class="editableInput">
                                        <input type="text" name="Retention" id="Retention" class="form-control" placeholder="Job Goals ...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Retention', 'RetentionLabel', 'RetentionField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label><b>ROI On File:</b></label>
                                    <!--<div class="value" id="ROIOnFileLabel" onclick="return UpdateField('ROIOnFileLabel', 'ROIOnFileField')">&nbsp;</div>-->
                                    <div id="ROIOnFileField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="ROIOnFile" id="ROIOnFile_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'ROIOnFile_Yes', 'ROIOnFileLabel', 'ROIOnFileField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="ROIOnFile" id="ROIOnFile_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'ROIOnFile_No', 'ROIOnFileLabel', 'ROIOnFileField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label><b>Release To Verify Wages:</b></label>
                                    <!--<div class="value" id="ReleaseToVerifyWagesLabel" onclick="return UpdateField('ReleaseToVerifyWagesLabel', 'ReleaseToVerifyWagesField')">&nbsp;</div>-->
                                    <div id="ReleaseToVerifyWagesField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="ReleaseToVerifyWages" id="ReleaseToVerifyWages_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'ReleaseToVerifyWages_Yes', 'ReleaseToVerifyWagesLabel', 'ReleaseToVerifyWagesField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="ReleaseToVerifyWages" id="ReleaseToVerifyWages_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'ReleaseToVerifyWages_No', 'ReleaseToVerifyWagesLabel', 'ReleaseToVerifyWagesField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label><b>Benefits Counseling:</b></label>
                                    <!--<div class="value" id="BenefitsCounselingLabel" onclick="return UpdateField('BenefitsCounselingLabel', 'BenefitsCounselingField')">&nbsp;</div>-->
                                    <div id="BenefitsCounselingField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="BenefitsCounseling" id="BenefitsCounseling_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'BenefitsCounseling_Yes', 'BenefitsCounselingLabel', 'BenefitsCounselingField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="BenefitsCounseling" id="BenefitsCounseling_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'BenefitsCounseling_No', 'BenefitsCounselingLabel', 'BenefitsCounselingField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <h4>Status:</h4>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label><b>Soft Skills:</b></label>
                                    <!--<div class="value" id="SoftSkillsLabel" onclick="return UpdateField('SoftSkillsLabel', 'SoftSkillsField')">&nbsp;</div>-->
                                    <div id="SoftSkillsField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="SoftSkills" id="SoftSkills_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'SoftSkills_Yes', 'SoftSkillsLabel', 'SoftSkillsField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="SoftSkills" id="SoftSkills_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'SoftSkills_No', 'SoftSkillsLabel', 'SoftSkillsField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label><b>JSS:</b></label>
                                    <!--<div class="value" id="JSSLabel" onclick="return UpdateField('JSSLabel', 'JSSField')">&nbsp;</div>-->
                                    <div id="JSSField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="JSS" id="JSS_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'JSS_Yes', 'JSSLabel', 'JSSField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="JSS" id="JSS_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'JSS_No', 'JSSLabel', 'JSSField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label><b>Job Search:</b></label>
                                    <!--<div class="value" id="JobSearchLabel" onclick="return UpdateField('JobSearchLabel', 'JobSearchField')">&nbsp;</div>-->
                                    <div id="JobSearchField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="JobSearch" id="JobSearch_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'JobSearch_Yes', 'JobSearchLabel', 'JobSearchField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="JobSearch" id="JobSearch_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'JobSearch_No', 'JobSearchLabel', 'JobSearchField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="message_heading " id="EnReportEarningHeading">
                                    <div class="col-lg-5">
                                        <label class="full-width"><b>Does EN Reporting Earnings</b></label>
                                        <!--<div class="value" id="EnReportEarningLabel" onclick="return UpdateField('EnReportEarningLabel', 'EnReportEarningField')">&nbsp;</div>-->
                                        <div id="EnReportEarningField" class="editableInput">
                                            <label class="kt-radio kt-radio--bold kt-radio--yellow">
                                                <input type="radio" name="EnReportEarning" id="EnReportEarning_Yes" class="EnReportEarning" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'EnReportEarning_Yes', 'EnReportEarningLabel', 'EnReportEarningField')" /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--success">
                                                <input type="radio" name="EnReportEarning" id="EnReportEarning_No" class="EnReportEarning" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'EnReportEarning_No', 'EnReportEarningLabel', 'EnReportEarningField')" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                            <div class="row">
                                <div class="form-group col-md-6" >
                                    <label><b>Job Type:</b></label>
                                    <!--<div class="value" id="JobTypeLabel" onclick="return UpdateField('JobTypeLabel', 'JobTypeField')">&nbsp;</div>-->
                                    <div id="JobTypeField" class="editableInput">
                                        <label class="kt-radio extendwidth kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="JobType" id="JobType_Full" value="Full Time" onclick="return UpdateValue('kt_modal_view_detail', 'JobType_Full', 'JobTypeLabel', 'JobTypeField')" /> Full Time
                                            <span></span>
                                        </label>
                                        <label class="kt-radio extendwidth kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="JobType" id="JobType_Part" value="Part Time" onclick="return UpdateValue('kt_modal_view_detail', 'JobType_Part', 'JobTypeLabel', 'JobTypeField')" /> Part Time
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b>Hourly Wages:</b></label>
                                    <!--<div class="value" id="HourlyWageLabel" onclick="return UpdateField('HourlyWageLabel', 'HourlyWageField')">&nbsp;</div>-->
                                    <div id="HourlyWageField" class="editableInput input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" name="HourlyWage" id="HourlyWage" class="form-control" placeholder="0.00" aria-label="Text input with $ sign">
                                        <span style="padding-left: 12px;font-size: 18px;line-height: 35px;">/hour</span>

<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'HourlyWage', 'HourlyWageLabel', 'HourlyWageField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>Employer:</b></label>
                                    <!--<div class="value" id="EmployerLabel" onclick="return UpdateField('EmployerLabel', 'EmployerField')">&nbsp;</div>-->
                                    <div id="EmployerField" class="editableInput">
                                        <input type="text" name="Employer" id="Employer" class="form-control" placeholder="Employer...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Employer', 'EmployerLabel', 'EmployerField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b>Benefits:</b></label>
                                    <!--<div class="value" id="BenefitsLabel" onclick="return UpdateField('BenefitsLabel', 'BenefitsField')">&nbsp;</div>-->
                                    <div id="BenefitsField" class="editableInput">
                                        <input type="text" name="Benefits" id="Benefits" class="form-control" placeholder="Benefits...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Benefits', 'BenefitsLabel', 'BenefitsField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b> Gross Earnings:</b></label>
                                    <!--<div class="value" id="GrossEarningsLabel" onclick="return UpdateField('GrossEarningsLabel', 'GrossEarningsField')">&nbsp;</div>-->
                                    <div id="GrossEarningsField" class="editableInput">
                                        <input type="text" name="GrossEarnings" id="GrossEarnings" class="form-control" placeholder="Gross Earnings...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'GrossEarnings', 'GrossEarningsLabel', 'GrossEarningsField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label><b> Net Earnings:</b></label>
                                    <!--<div class="value" id="GrossEarningsLabel" onclick="return UpdateField('GrossEarningsLabel', 'GrossEarningsField')">&nbsp;</div>-->
                                    <div id="GrossEarningsField" class="editableInput">
                                        <input type="text" name="NetEarnings" id="NetEarnings" class="form-control" placeholder="Net Earnings...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'GrossEarnings', 'GrossEarningsLabel', 'GrossEarningsField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label><b>Address:</b></label>
                                    <!--<div class="value" id="AddressLabel" onclick="return UpdateField('AddressLabel', 'AddressField')">&nbsp;</div>-->
                                    <div id="AddressField" class="editableInput">
                                        <input type="text" name="Address" id="Address" class="form-control" placeholder="Address...">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'Address', 'AddressLabel', 'AddressField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="PayStart"><b>Pay Start Date:</b></label>
                                    <!--<div class="value" id="PayStartLabel" onclick="return UpdateField('PayStartLabel', 'PayStartField')">&nbsp;</div>-->
                                    <div id="PayStartField" class="editableInput">
                                        <input type="text" name="PayStart" id="PayStart" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PayStart', 'PayStartLabel', 'PayStartField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="PayEnd"><b>Pay End Date:</b></label>
                                    <!--<div class="value" id="PayEndLabel" onclick="return UpdateField('PayEndLabel', 'PayEndField')">&nbsp;</div>-->
                                    <div id="PayEndField" class="editableInput">
                                        <input type="text" name="PayEnd" id="PayEnd" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PayEnd', 'PayEndLabel', 'PayEndField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="PaidDate"><b>Paid Date:</b></label>
                                    <!--<div class="value" id="PaidDateLabel" onclick="return UpdateField('PaidDateLabel', 'PaidDateField')">&nbsp;</div>-->
                                    <div id="PaidDateField" class="editableInput">
                                        <input type="text" name="PaidDate" id="PaidDate" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'PaidDate', 'PaidDateLabel', 'PaidDateField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="DateOfHire"><b>Date Of Hire:</b></label>
                                    <!--<div class="value" id="DateOfHireLabel" onclick="return UpdateField('DateOfHireLabel', 'DateOfHireField')">&nbsp;</div>-->
                                    <div id="DateOfHireField" class="editableInput">
                                        <input type="text"  name="DateOfHire" id="DateOfHire" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY">
<!--                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="return UpdateValue('kt_modal_view_detail', 'DateOfHire', 'DateOfHireLabel', 'DateOfHireField')">Save</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>Earning Structure :</b></label>
                                    <!--<div class="value" id="EarningStructureLabel" onclick="return UpdateField('EarningStructureLabel', 'EarningStructureField')">&nbsp;</div>-->
                                    <div id="EarningStructureField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="EarningStructure" id="EarningStructure_SSDI" value="SSDI" onclick="return UpdateValue('kt_modal_view_detail', 'EarningStructure_SSDI', 'EarningStructureLabel', 'EarningStructureField')" /> SSDI
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="EarningStructure" id="EarningStructure_SSI" value="SSI" onclick="return UpdateValue('kt_modal_view_detail', 'EarningStructure_SSI', 'EarningStructureLabel', 'EarningStructureField')" /> SSI
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="EarningStructure" id="EarningStructure_Both" value="Both" onclick="return UpdateValue('kt_modal_view_detail', 'EarningStructure_Both', 'EarningStructureLabel', 'EarningStructureField')" /> Both
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>TWL:</b></label>
                                    <!--<div class="value" id="TWLLabel" onclick="return UpdateField('TWLLabel', 'TWLField')">&nbsp;</div>-->
                                    <div id="TWLField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="TWL" id="TWL_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'TWL_Yes', 'TWLLabel', 'TWLField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="TWL" id="TWL_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'TWL_No', 'TWLLabel', 'TWLField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label><b>SGA :</b></label>
                                    <!--<div class="value" id="SGALabel" onclick="return UpdateField('SGALabel', 'SGAField')">&nbsp;</div>-->
                                    <div id="SGAField" class="editableInput">
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="SGA" id="SGA_Yes" value="Yes" onclick="return UpdateValue('kt_modal_view_detail', 'SGA_Yes', 'SGALabel', 'SGAField')" /> Yes
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger">
                                            <input type="radio" name="SGA" id="SGA_No" value="No" onclick="return UpdateValue('kt_modal_view_detail', 'SGA_No', 'SGALabel', 'SGAField')" /> No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                            <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Close</button>
                            <input type="hidden" name="beneficiary_id" id="beneficiary_id" value="" />
                            <input type="hidden" name="hire_id" id="hire_id" value="" />
                            <input type="hidden" id="data_panel" value="" />
                        </div>
                    </div>
                </div>
            </form>
                <div class="modal-footer">
                    <div class="message_heading">
                        <label class="kt-padding-t-10"><b>Notes:</b> &nbsp; <button class="btn btn-info addNotes" style="float:right; margin-top: -8px;" type="button" data-beneficiary-id=""><i class="fa fa-plus"></i> Add Notes </button> </label>
                        <form name="message_form" id="message_form" class="hide" action="" method="post">
                            <div id="msg-display" class="alert kt-padding-5 kt-margin-b-0" role="alert" style="display: none; font-size: 13px;"></div>
                            <textarea name="message" id="message" class="form-control required  kt-margin-t-10" rows="3" placeholder="Your Message..." aria-required="true" spellcheck="false"></textarea>

                            <button type="submit" class="btn btn-primary submit-input pull-right kt-margin-t-10" id="msg-submit-btn">Send Message</button>
                            <button type="button" class="btn btn-secondary addNotes pull-right kt-margin-t-10" id="msg-close-btn">Close</button>
                            <span id="message-loader" class="pull-right kt-margin-t-10 hide"><img src="{{ asset('images/preloaders/11.gif') }}" /></span>
                            <input type="hidden" name="beneficiary_id" id="beneficiary_id" value="" />
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                        </form>
                    </div>
                    <div class="full-width" >
                        <div class="chat">
                            <div class="chat-history" id="notesDetail">
                                <span id="chat-loader" style="display:none;"><img src="{{ asset('images/preloaders/11.gif') }}" /></span>
                            </div> <!-- end chat-history -->
                        </div> <!-- end chat -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
