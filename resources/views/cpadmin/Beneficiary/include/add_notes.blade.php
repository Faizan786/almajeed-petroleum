
<!-- Add Notes Modal -->
<div id="addNotes" class="modal fade" role="dialog">
    <div class="modal-dialog">
        
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Message</h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <form name="message_form" id="message_form" action="" method="post">
                <div class="modal-body">
                    <div id="msg-display" class="alert" role="alert" style="display: none;"></div>
                    <p><textarea name="message" id="message" class="form-control required" rows="5" placeholder="Your Message..." aria-required="true" spellcheck="false"></textarea></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submit-input" id="msg-submit-btn">Send Message</button>
                    <span id="message-loader" style="display:none;"><img src="{{ asset('images/preloaders/11.gif') }}" /></span>
                    <input type="hidden" name="beneficiary_id" id="beneficiary_id" value="" />
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                </div>
            </form>
        </div>
        
    </div>
</div>