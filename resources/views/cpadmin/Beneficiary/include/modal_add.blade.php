<div class="modal fade" id="kt_modal_add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Fill Beneficiary Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="kt-portlet__body">
                    <div class="container">
                        <div class="stepwizard">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step col-xs-4">
                                    <a data-next-tab="step-1" id="nav_tab_1" class="btn btn-success btn-circle navigation-tab">1</a>
                                    <p><small>Beneficiary</small></p>
                                </div>
                                <div class="stepwizard-step col-xs-4"> 
                                    <a data-next-tab="step-2" id="nav_tab_2" class="btn btn-disabled btn-circle">2</a>
                                    <p><small>Hire Information</small></p>
                                </div>
                                <div class="stepwizard-step col-xs-4"> 
                                    <a data-next-tab="step-3" id="nav_tab_3" class="btn btn-disabled btn-circle">3</a>
                                    <p><small>Final</small></p>
                                </div>
                            </div>
                        </div>

                        <form name="frmDirectory" method="post" action="{{ url('cpadmin/beneficiary-list/create')}}" enctype="multipart/form-data" class="kt-form kt-form--label-right">
                            @csrf
                            <div class="panel panel-primary setup-content" id="step-1">
                                <div class="panel-heading">
                                    <h3 class="panel-title bouneInDown animated">Beneficiary</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>First Name:</label>
                                            <input name="FirstName" type="text" class="form-control" placeholder="Enter first name">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Last Name:</label>
                                            <input name="LastName" type="text" class="form-control" placeholder="Enter Last Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Address:</label>
                                            <input name="Address" type="text" class="form-control" placeholder="Enter Address">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Phone:</label>
                                            <input name="PhoneNumber" type="text" class="form-control" placeholder="Enter Phone" id="kt_inputmask_3">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Email:</label>
                                            <input name="Email" type="text" class="form-control" placeholder="Enter Email">
                                        </div>
                                        <div class="col-lg-6 ssn">
                                            <label>SSN:</label>
                                            <input name="SSN" id="kt_inputmask_3_1" type="text" class="form-control" placeholder="Enter SSN">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Disabilities:</label>
                                            <input name="Disabilities" type="text" class="form-control" placeholder="Enter Disabilities">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Physical Restructions:</label>
                                            <input name="PhysicalRestrictions" type="text" class="form-control" placeholder="Physical Restructions">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Job Goals:</label>
                                            <input name="JobGoals" type="text" class="form-control" placeholder="Job Goals">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Retention:</label>
                                            <input name="Retention" type="text" class="form-control" placeholder="Retention">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <label>ROI on File:</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="ROIOnFile" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="ROIOnFile" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Release to verify wages:</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="ReleaseToVerifyWages" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="ReleaseToVerifyWages" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Benefits Counseling</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="BenefitsCounseling" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="BenefitsCounseling" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>

                                    <h4>Status:</h4>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <label>Soft Skills:</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="SoftSkills" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="SoftSkills" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>JSS:</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="JSS" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="JSS" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Job Search:</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="JobSearch" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="JobSearch" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="message_heading ">
                                        <div class="col-lg-5">
                                            <label class="full-width"><b>Does EN Reporting Earnings</b></label>
                                            <label class="kt-radio kt-radio--bold kt-radio--success"id="no">
                                                <input type="radio" name="EnReportEarning" id="EnReportEarning_no" class="EnReportEarning" value="No" checked /> No
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--yellow"id="yes">
                                                <input type="radio" name="EnReportEarning" id="EnReportEarning_yes" class="EnReportEarning" value="Yes" /> Yes
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    
                                    <button class="btn btn-primary nextTabBtnStep pull-right" data-next-tab="step-2" data-nav="nav_tab_2" type="button">Next</button>
                                    <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"> Cancel </button>
                                </div>
                            </div>
                            <div class="panel panel-primary setup-content hide" id="step-2">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Hire Information</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group row ">
                                        <div class="col-lg-6">
                                            <label>Job Type:</label><br />
                                            <input name="JobType" data-switch="true" type="checkbox" value="Full Time" checked="checked" data-on-text="Full Time" data-handle-width="170" data-off-text="Part Time" data-on-color="brand" />
                                        </div>
                                        <div class=" col-sm-6">
                                            <label>Hourly Wages:</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="HourlyWage" class="form-control" placeholder="0.00" aria-label="Text input with $ sign">
                                                <span style="padding-left: 12px;font-size: 18px;line-height: 35px;">/hour</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Beneficiary:</label>
                                            {!! select_dropdown_with_brackets("beneficiary_id", "beneficiary_id", "form-control", "", "beneficiary", array('is_trashed' => 'No'), "FirstName", "LastName", "SSN") !!}
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Employer:</label>
                                            <input name="Employer" type="text" class="form-control" placeholder="Employeer" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <label>Address:</label>
                                            <input name="Address" type="text" class="form-control" placeholder="Address" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Pay Start Date:</label>
                                            <input type="text" name="PayStart" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY" autocomplete="off" />
                                        </div>
                                        <div class=" col-lg-6">
                                            <label class="">Pay End Date:</label>
                                            <input type="text" name="PayEnd" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Paid Date:</label>
                                            <input type="text" name="PaidDate" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY" autocomplete="off" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Date Of Hire:</label>
                                            <input type="text" name="DateOfHire" class="form-control kt_datepicker_2" placeholder="MM/DD/YYYY" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Gross Earning:</label>
                                            <input name="GrossEarnings" type="text" class="form-control" placeholder="Gross Earning" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Benefits:</label>
                                            <input name="Benefits" type="text" class="form-control" placeholder="Benefits" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-5">
                                            <label>Earning Structure:</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="EarningStructure" value="SSD" />SSD
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="EarningStructure" value="SSI" />SSI
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="EarningStructure" value="Both" checked>Both
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>At TWL:</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="TWL" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input type="radio" name="TWL" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label>At SSG</label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger kt-margin-r-30">
                                                <input type="radio" name="SGA" value="Yes" checked /> Yes
                                                <span></span>
                                            </label>
                                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                                <input name="SGA" type="radio" value="No" /> No
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    
                                    <button class="btn btn-primary nextTabBtnStep pull-right" data-next-tab="step-3" data-nav="nav_tab_3" type="button">Next</button>
                                    <button class="btn btn-primary prevTabBtnStep pull-right" data-next-tab="step-1" type="button">Previous</button>
                                    <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"> Cancel </button>
                                </div>
                            </div>
                            <div class="panel panel-primary setup-content hide" id="step-3">
                                <div class="panel-heading">
                                     <h3 class="panel-title">Final Step</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="control-label">Please click on save button to proceed.</label>
                                    </div>
                                    <button class="btn btn-primary nextBtn pull-right" type="Submit">Save</button>
                                    <button class="btn btn-primary prevTabBtnStep pull-right" data-next-tab="step-2" type="button">Previous</button>
                                    <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"> Cancel </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
