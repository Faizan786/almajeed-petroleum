<script type="text/javascript">
    function loadView(type='', pagination_no='', page_size='', search='', sort_col=''){
        $('.screenloader').show();
        if(search == ""){
            search = $('#table_search').val();
        }
        if(page_size == ""){
            page_size = $('#table_result_size').val();
        }
        if(pagination_no == ""){
            //pagination_no = $('.page-link.active').attr('data-page');
        }

        $.post("{{ url('/cpadmin/beneficiary-list-data') }}", {_token:"{{csrf_token()}}", page:type, pageno:pagination_no, size:page_size, query_search:search, sorting:sort_col}, function(result){
            //console.log(result);
            if(!result.error && result.counter != ""){
                $('#pagination_show').html(result.pagination);
                $('#display_number_of_records').html(result.counter);
                var status = '';
                var usr_type = '';
                var htmlcode = '';
                var month = new Array();
                month[0] = "Jan"; month[1] = "Feb"; month[2] = "Mar"; month[3] = "Apr"; month[4] = "May"; month[5] = "Jun"; month[6] = "Jul"; month[7] = "Aug"; month[8] = "Sep"; month[9] = "Oct"; month[10] = "Nov"; month[11] = "Dec";

                $.each(result.object, function(index, val) {
                    buttons = '<a class="dropdown-item actionview" data-beneficiary-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_view_detail"><i class="la la-search-plus"></i> View</a>';
                    buttons += '<a class="dropdown-item actionPayStub" data-hire-id="' + val.hire_id + '" data-beneficiary-id="'+ val.id +'" data-toggle="modal" data-target="#kt_modal_pay_stub" data-backdrop="static" data-keyboard="false"><i class="la la-search-plus"></i> Pay Stub</a>';
                    buttons += '<a class="dropdown-item actionReport" href="{{ url('cpadmin/paystub-list/export') }}/'+ val.id +'" target="_blank"><i class="la la-search-plus"></i> Report</a>';
                    buttons += '<a class="dropdown-item actionTrash" data-beneficiary-id="' + val.id + '" data-toggle="modal" data-target="#kt_modal_trash"><i class="la la-trash-o"></i> Trash</a>';

                    var actionBtn = '<span class="dropdown">';
                    actionBtn += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false"><i class="la la-bars"></i></a>';
                    actionBtn += '<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">';
                    actionBtn += buttons;
                    actionBtn += '</div></span>';

                    dbdate = "---";
                    if(val.created_at != "0000-00-00 00:00:00"){
                        var created_at = new Date(val.created_at);
                        dbdate = (month[created_at.getMonth()]) +' '+ created_at.getDate() +', '+ created_at.getFullYear();
                    }
                    htmlcode += '<tr id="row_' + val.id + '">';
                    htmlcode += '<th scope="row">' + val.id + '</th>';
                    htmlcode += '<td>' + val.FirstName +" "+ val.LastName + '</td>';
                    htmlcode += '<td>' + val.Email +'</td>';
                    htmlcode += '<td>' + val.PhoneNumber +'</td>';
                    htmlcode += '<td>' + val.JobGoals +'</td>';
                    htmlcode += '<td>' + val.Disabilities +'</td>';
                    htmlcode += '<td>' + val.PhysicalRestrictions +'</td>';
                    htmlcode += '<td>' + dbdate +'</td>';
                    htmlcode += '<td>' + actionBtn + '</td>';
                    htmlcode += '</tr>';
                });
                $('#loadtablebody').html(htmlcode);
            } else {
                if(!result.message){
                    $('#loadtablebody').html('<tr><td align="center" colspan="10">No record founds!</td></tr>');
                } else {
                    $('#loadtablebody').html('<tr><td align="center" colspan="10">'+result.message+'</td></tr>');
                }
            }
            $('.screenloader').hide();
        });
    }

    jQuery(document).ready(function() {
        /*******Datatable Codes Start********/
        loadView('{{ $listType }}', '', '', '', '');
        $('body').on('keyup', '#table_search', function(){
            var search = $(this).val();
            loadView('{{ $listType }}', '', '', search, '');
        });
        $('body').on('change', '#table_result_size', function(){
            var size = $(this).val();
            loadView('{{ $listType }}', '', size, '', '');
        });
        $('body').on('click', '.page-link', function(){
            var id = $(this).attr('data-page');
            loadView('{{ $listType }}', id, '', '', '');
        });
        $('body').on('click', '.sort_column', function(){
            var col = $(this).attr('data-col');
            if($(this).hasClass('sort_desc')){
                $(".sort_column").removeClass("sort_desc");
                $(this).addClass('sort_asc');
                col += "~ASC";
            } else {
                $(".sort_column").removeClass("sort_asc");
                $(this).addClass('sort_desc');
                col += "~DESC";
            }
            loadView('{{ $listType }}', '', '', '', col);
        });
        /*******Datatable Codes End********/

        $('body').on('click', '.actionPayStub', function(){
            var val1 = $(this).attr('data-hire-id');
            var val2 = $(this).attr('data-beneficiary-id');
            $('#kt_modal_pay_stub #hire_id').val(val1);
            $('#kt_modal_pay_stub #beneficiary_id').val(val2);
        });
        
        $('body').on('click', '#kt_modal_view_detail .fill_view_detail', function(){
            var id = $(this).attr('data-panel');
            $('#kt_modal_view_detail .tab-pane').removeClass('active');
            $('#kt_modal_view_detail #'+id).addClass('active');
            $('#kt_modal_view_detail #data_panel').val(id);
        });
        
        $('body').on('click', '.actionview', function(){
            var beneficiary_id = $(this).attr('data-beneficiary-id');
            $('.message_heading button').attr('data-beneficiary-id', beneficiary_id);
            $('#kt_modal_view_detail .fill_view_detail').removeClass('active');
            $('#kt_modal_view_detail .fill_view_detail.first').addClass('active');
            $('#kt_modal_view_detail .tab-pane').removeClass('active');
            $('#kt_modal_view_detail #kt_tabs_1_1').addClass('active');
            
            $('#kt_modal_view_detail .modal-body').hide();
            $('#kt_modal_view_detail .spinnerbox').show();
            $.post("{{ url('/cpadmin/beneficiary-list/view') }}", {id:beneficiary_id, _token:"{{csrf_token()}}"}, function(result){
                if(!result.error){
                    var month = new Array();
                    month[0] = "Jan"; month[1] = "Feb"; month[2] = "Mar"; month[3] = "Apr"; month[4] = "May"; month[5] = "Jun"; month[6] = "Jul"; month[7] = "Aug"; month[8] = "Sep"; month[9] = "Oct"; month[10] = "Nov"; month[11] = "Dec";
                    
                    $('#kt_modal_view_detail #data_panel').val('kt_tabs_1_1');
                    $('#kt_modal_view_detail #beneficiary_id').val(result.beneficiary.id);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #FirstNameLabel').html(result.beneficiary.FirstName);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #FirstName').val(result.beneficiary.FirstName);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #LastNameLabel').html(result.beneficiary.LastName);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #LastName').val(result.beneficiary.LastName);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #SSNLabel').html(result.beneficiary.SSN);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #SSN').val(result.beneficiary.SSN);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #AddressLabel').html(result.beneficiary.Address);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #Address').val(result.beneficiary.Address);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #PhoneNumberLabel').html(result.beneficiary.PhoneNumber);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #PhoneNumber').val(result.beneficiary.PhoneNumber);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #EmailLabel').html(result.beneficiary.Email);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #Email').val(result.beneficiary.Email);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #DisabilitiesLabel').html(result.beneficiary.Disabilities);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #Disabilities').val(result.beneficiary.Disabilities);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #PhysicalRestrictionsLabel').html(result.beneficiary.PhysicalRestrictions);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #PhysicalRestrictions').val(result.beneficiary.PhysicalRestrictions);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #JobGoalsLabel').html(result.beneficiary.JobGoals);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #JobGoals').val(result.beneficiary.JobGoals);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #RetentionLabel').html(result.beneficiary.Retention);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #Retention').val(result.beneficiary.Retention);
                    $('#kt_modal_view_detail #kt_tabs_1_1 #ROIOnFileLabel').html(result.beneficiary.ROIOnFile);
                    if(result.beneficiary.ROIOnFile == "Yes"){
                        $('#kt_modal_view_detail #kt_tabs_1_1 #ROIOnFile_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #kt_tabs_1_1 #ROIOnFile_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #kt_tabs_1_1 #ReleaseToVerifyWagesLabel').html(result.beneficiary.ReleaseToVerifyWages);
                    if(result.beneficiary.ReleaseToVerifyWages == "Yes"){
                        $('#kt_modal_view_detail #kt_tabs_1_1 #ReleaseToVerifyWages_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #kt_tabs_1_1 #ReleaseToVerifyWages_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #kt_tabs_1_1 #BenefitsCounselingLabel').html(result.beneficiary.BenefitsCounseling);
                    if(result.beneficiary.BenefitsCounseling == "Yes"){
                        $('#kt_modal_view_detail #kt_tabs_1_1 #BenefitsCounseling_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #kt_tabs_1_1 #BenefitsCounseling_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #kt_tabs_1_1 #SoftSkillsLabel').html(result.beneficiary.SoftSkills);
                    if(result.beneficiary.SoftSkills == "Yes"){
                        $('#kt_modal_view_detail #kt_tabs_1_1 #SoftSkills_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #kt_tabs_1_1 #SoftSkills_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #kt_tabs_1_1 #JSSLabel').html(result.beneficiary.JSS);
                    if(result.beneficiary.JSS == "Yes"){
                        $('#kt_modal_view_detail #kt_tabs_1_1 #JSS_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #kt_tabs_1_1 #JSS_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #kt_tabs_1_1 #JobSearchLabel').html(result.beneficiary.JobSearch);
                    if(result.beneficiary.JobSearch == "Yes"){
                        $('#kt_modal_view_detail #kt_tabs_1_1 #JobSearch_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #kt_tabs_1_1 #JobSearch_No').prop("checked", true);
                    }
                    $('#kt_modal_view_detail #kt_tabs_1_1 #EnReportEarningLabel').html(result.beneficiary.EnReportEarning);
                    if(result.beneficiary.EnReportEarning == "Yes"){
                        $('#kt_modal_view_detail #EnReportEarningHeading').removeClass("greenbgheading");
                        $('#kt_modal_view_detail #EnReportEarningHeading').addClass("yellowbgheading");
                        $('#kt_modal_view_detail #kt_tabs_1_1 #EnReportEarning_Yes').prop("checked", true);
                    } else {
                        $('#kt_modal_view_detail #EnReportEarningHeading').removeClass("yellowbgheading");
                        $('#kt_modal_view_detail #EnReportEarningHeading').addClass("greenbgheading");
                        $('#kt_modal_view_detail #kt_tabs_1_1 #EnReportEarning_No').prop("checked", true);
                    }
                    
                    //var created_at = new Date(result.beneficiary.created_at);
                    //$('#kt_modal_view_detail #kt_tabs_1_1 #created_at').html((month[created_at.getMonth()])+' '+created_at.getDate()+', '+created_at.getFullYear()+' '+created_at.getHours()+':'+created_at.getMinutes()+':'+created_at.getSeconds());
                    if(result.hire){
                        $('#kt_modal_view_detail #hire_id').val(result.hire.id);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #BeneficiaryLabel').html(result.beneficiary.FirstName+" "+result.beneficiary.LastName);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #beneficiary_id').val(result.hire.beneficiary_id);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #HourlyWageLabel').html(result.hire.HourlyWage);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #HourlyWage').val(result.hire.HourlyWage);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #EmployerLabel').html(result.hire.Employer);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #Employer').val(result.hire.Employer);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #BenefitsLabel').html(result.hire.Benefits);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #Benefits').val(result.hire.Benefits);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #AddressLabel').html(result.hire.Address);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #Address').val(result.hire.Address);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #PayStartLabel').html(result.hire.PayStart);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #PayStart').val(result.hire.PayStart);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #PayEndLabel').html(result.hire.PayEnd);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #PayEnd').val(result.hire.PayEnd);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #PaidDateLabel').html(result.hire.PaidDate);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #PaidDate').val(result.hire.PaidDate);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #GrossEarningsLabel').html(result.hire.GrossEarnings);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #GrossEarnings').val(result.hire.GrossEarnings);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #JobTypeLabel').html(result.hire.JobType);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #JobType').val(result.hire.JobType);
                        if(result.hire.JobType == "Full Time"){
                            $('#kt_modal_view_detail #kt_tabs_1_2 #JobType_Full').prop("checked", true);
                        } else {
                            $('#kt_modal_view_detail #kt_tabs_1_2 #JobType_Part').prop("checked", true);
                        }
                        $('#kt_modal_view_detail #kt_tabs_1_2 #EarningStructureLabel').html(result.hire.EarningStructure);
                        if(result.hire.EarningStructure == "SSDI"){
                            $('#kt_modal_view_detail #kt_tabs_1_2 #EarningStructure_SSDI').prop("checked", true);
                        } else if(result.hire.EarningStructure == "SSI"){
                            $('#kt_modal_view_detail #kt_tabs_1_2 #EarningStructure_SSI').prop("checked", true);
                        } else {
                            $('#kt_modal_view_detail #kt_tabs_1_2 #EarningStructure_Both').prop("checked", true);
                        }
                        $('#kt_modal_view_detail #kt_tabs_1_2 #TWLLabel').html(result.hire.TWL);
                        if(result.hire.TWL == "Yes"){
                            $('#kt_modal_view_detail #kt_tabs_1_2 #TWL_Yes').prop("checked", true);
                        } else {
                            $('#kt_modal_view_detail #kt_tabs_1_2 #TWL_No').prop("checked", true);
                        }
                        $('#kt_modal_view_detail #kt_tabs_1_2 #SGALabel').html(result.hire.SGA);
                        if(result.hire.SGA == "Yes"){
                            $('#kt_modal_view_detail #kt_tabs_1_2 #SGA_Yes').prop("checked", true);
                        } else {
                            $('#kt_modal_view_detail #kt_tabs_1_2 #SGA_No').prop("checked", true);
                        }
                        $('#kt_modal_view_detail #kt_tabs_1_2 #DateOfHireLabel').html(result.hire.DateOfHire);
                        $('#kt_modal_view_detail #kt_tabs_1_2 #DateOfHire').val(result.hire.DateOfHire);
                    }
                    getNotesList(beneficiary_id, '.chat-history');
                    
                    $('#kt_modal_view_detail .modal-body').show();
                    $('#kt_modal_view_detail .spinnerbox').hide();
                }
            });
        });

        $('body').on('click', '.actionTrash', function(){
            var val = $(this).attr('data-id');
            $('#kt_modal_trash #primary_id').val(val);
        });
        
        $('body').on('click', '.addNotes', function(){
            var id = $(this).attr('data-beneficiary-id');
            $('#message_form').toggle();
            $('#message_form #beneficiary_id').val(id);
        });

        $('body').on('submit', '#message_form', function(event){
            var message = $('#message_form #message').val();
            var beneficiaryid = $('#message_form #beneficiary_id').val();
            var token = $('#message_form #_token').val();
            $('#message_form #msg-submit-btn, #message_form #msg-close-btn').hide();
            $('#message_form #message-loader').show();
            $('#message_form #msg-display').removeClass('alert-danger').hide();
            $('#message_form #msg-display').removeClass('alert-success').hide();
            $.post('/message-save', {_token:token, message:message, beneficiary_id:beneficiaryid}, function(result){
                if(result.type == "error"){
                    $('#message_form #msg-display').addClass('alert-danger').text(result.message).show();
                } else {
                    $('#message_form #msg-display').addClass('alert-success').text(result.message).show();
                    $('#message_form #message').val('');
                    getNotesList(beneficiaryid, '.chat-history');
                    //$('#addNotes').modal('toggle');
                }
                setTimeout(function() {
                    $('#message_form #msg-display').fadeOut('fast');
                }, 3000);
                $('#message_form #msg-submit-btn, #message_form #msg-close-btn').show();
                $('#message_form #message-loader').hide();
            });
            event.preventDefault();
        });
        
        /******Add New Modal Tabs******/
        $('body').on('click', '#kt_modal_add_new .nextTabBtnStep, #kt_modal_add_new .prevTabBtnStep, #kt_modal_add_new .navigation-tab', function(){
            $("#kt_modal_add_new .setup-content").hide();
            var next_tab = $(this).attr('data-next-tab');
            var nav_menu = $(this).attr('data-nav');
            $("#"+next_tab).show();
            if(nav_menu){
                $("#"+nav_menu).removeClass('btn-disabled');
                $("#"+nav_menu).addClass('btn-success');
                $("#"+nav_menu).addClass('navigation-tab');
            }
            $('#kt_modal_add_new').animate({
                scrollTop: $('#nav_tab_1').offset().top - 20
            }, 'slow');
        });
        
        /******Add New Modal EnReport Earning Radio Button******/
        $('body').on('click', '#kt_modal_add_new .EnReportEarning, #kt_modal_add_new .EnReportEarning', function(){
            var value = $(this).val();
            if(value == "Yes"){
                var parent = $(this).parent().parent().parent().removeClass('greenbgheading');
                var parent = $(this).parent().parent().parent().addClass('yellowbgheading');
            } else {
                var parent = $(this).parent().parent().parent().removeClass('yellowbgheading');
                var parent = $(this).parent().parent().parent().addClass('greenbgheading');
            }
        });
        $('body').on('click', '#kt_modal_view_detail .EnReportEarning, #kt_modal_view_detail .EnReportEarning', function(){
            var value = $(this).val();
            if(value == "Yes"){
                var parent = $(this).parent().parent().parent().parent().removeClass('greenbgheading');
                var parent = $(this).parent().parent().parent().parent().addClass('yellowbgheading');
            } else {
                var parent = $(this).parent().parent().parent().parent().removeClass('yellowbgheading');
                var parent = $(this).parent().parent().parent().parent().addClass('greenbgheading');
            }
        });
        
    });
    
    function readURL(input, imageid) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(imageid)
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    var getNotesList = function (beneficiary_id=0, injector='#notesDetail'){
        $('#notesDetail #chat-loader').show();
        $.get('/message-get-all/'+beneficiary_id, {}, function(result){
            var contents = '<ul class="chat-ul">';
            $.each(result, function(index, val) {
                if(val.user_type == "admin"){
                    contents += '<li class="clearfix">';
                } else {
                    contents += '<li>';
                }
                if(val.user_type == "admin"){
                    contents += '<div class="message-data align-right">';
                } else {
                    contents += '<div class="message-data align-left">';
                }
                if(val.user_type == "admin"){
                    contents += '<span class="message-data-name" style="font-size:10px; color:#00a663;">'+val.created_at+' -> </span>';
                    contents += '<span class="message-data-name">'+val.admin_name+'</span> <i class="fa fa-circle me"></i>';
                } else {
                    contents += '<span class="message-data-name"><i class="fa fa-circle you"></i> '+val.user_name+'</span>';
                    contents += '<span class="message-data-name" style="font-size:10px; color:#00a663;"> <- '+val.created_at+'</span>';
                }
                contents += '</div>';
                if(val.user_type == "admin"){
                    contents += '<div class="message me-message align-left float-right">'+val.message+'</div>';
                } else {
                    contents += '<div class="message you-message align-left">'+val.message+'</div>';
                }
                contents += '</li>';
            });
            contents += "</ul>";
            $(injector).html(contents);
            $('#notesDetail #chat-loader').hide();
        });
    }
    
    function UpdateField(selecter, field) {
        $('#'+selecter).hide();
        $('#'+field).show();
    }
    function UpdateValue(page, idField, selecter, field) {
        var tabpanel = $('#'+page+' #data_panel').val();
        var fieldvalue = $('#'+page+' #'+idField).val();
        var fieldname = $('#'+page+' #'+idField).attr('name');
        
        if(tabpanel == "kt_tabs_1_1"){
            var primary_id = $('#'+page+' #beneficiary_id').val();
            var url = "{{ url('cpadmin/beneficiary-list/update') }}";
        } else {
            var primary_id = $('#'+page+' #hire_id').val();
            var url = "{{ url('cpadmin/hire-list/update') }}";
        }

        $.post(url, {id:primary_id, name:fieldname, value:fieldvalue, _token:"{{csrf_token()}}"}, function(result){
            if(!result.error){
                /*
                if($('#'+page+' #'+idField+" option:selected").text()){
                    $('#'+page+' #'+selecter).text($('#'+page+' #'+idField+" option:selected").text());
                } else {
                    $('#'+page+' #'+selecter).text(fieldvalue);
                }
                $('#'+page+' #'+selecter).show();
                $('#'+page+' #'+field).hide();
                */
            } else {
                alert(result.message);
            }
        });
    }
  
</script>
