<div class="modal fade" id="kt_modal_trash" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Moving To Trash?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label for="recipient-name" class="form-control-label"><p>Are you sure about this action?</p></label>
            </div>
            <div class="modal-footer">
                <form name="actiondelete" method="post" action="{{ url('/cpadmin/beneficiary-list/trash') .'/'. $listType}}">
                    @csrf
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Move to Trash</button>
                    <input type="hidden" name="id" id="primary_id" value="" />
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kt_modal_permanently_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Permanently Remove ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label for="recipient-name" class="form-control-label"><p>Are you sure about this action?</p></label>
            </div>
            <div class="modal-footer">
                <form name="actiondelete" method="post" action="{{ url('/cpadmin/users-list/empty-trash') .'/'. $listType}}">
                    @csrf
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Delete Permanent</button>
                    <input type="hidden" name="id" id="primary_id" value="" />
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kt_modal_delete_everthing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Empty Trashed List ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label for="recipient-name" class="form-control-label"><p>Are you sure about this action?</p></label>
            </div>
            <div class="modal-footer">
                <form name="actiondelete" method="post" action="{{ url('/cpadmin/users-list/empty-trash-all') .'/'. $listType}}">
                    @csrf
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Empty Trashed</button>
                </form>
            </div>
        </div>
    </div>
</div>
