<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Http\Models\Users;
use App\Http\Models\Orders;
use App\Http\Models\Invoices;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $invoice;
    public $item_name;
    public $quantity;
    public $rate;
    public $user;
    public $order;

    /**
     * Create a new message instance.
     * @return void
     */
    public function __construct(Invoices $invoice)
    {
        $this->invoice = $invoice;
        $this->item_name = explode('|', $invoice->item_name);
        $this->quantity = explode('|', $invoice->quantity);
        $this->rate = explode('|', $invoice->rate);
        $this->user = Users::find($invoice->user_id);
        $this->order = Orders::find($invoice->order_id);
    }
    
    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        return $this->subject("Your Invoice Generated For Order# ".$this->order->id)->markdown('emails.orders.invoice');
    }
}
