<?php
/**
 * Created by PhpStorm.
 * User: Tamim
 * Date: 12/7/2019
 * Time: 5:06 PM
 */
namespace App\Export;

use App\Http\Models\Paystub;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use \Maatwebsite\Excel\Writer;
use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class PaystubExport implements FromQuery, WithHeadings, WithEvents
{
    use Exportable;

    /**
     * PaystubExport constructor.
     * @param $id
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function __construct($id)
    {
        $this->id = $id;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');
        // MySQL-like timestamp '2008-12-31' or date string
        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder() );

        /** @var TYPE_NAME $sheet */
        $sheet->getActiveSheet()
            ->setCellValue('D1', '2008-12-31');

        $sheet->getActiveSheet()->getStyle('D1')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

// PHP-time (Unix time)
        $time = gmmktime(0,0,0,12,31,2008); // int(1230681600)
        $sheet->getActiveSheet()
            ->setCellValue('D1', \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($time));
        $sheet->getActiveSheet()->getStyle('D1')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

// Excel-date/time
        $sheet->getActiveSheet()->setCellValue('D1', 39813)
$sheet->getActiveSheet()->getStyle('D1')
    ->getNumberFormat()
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

        Writer::macro('setCreator', function (Writer $writer, string $creator) {
            $writer->getDelegate()->getProperties()->setCreator($creator);
        });

        Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
            $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
        });

        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => [self::class, 'afterSheet'],

            BeforeExport::class  => function(BeforeExport $event) {
                $event->writer->setCreator('DBProjectReport');
            },
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

                $event->sheet->styleCells(
                    'A1:E1',
                    [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['argb' => '000000'],
                            ],
                        ]
                    ]
                );
            },
        ];
    }

    public static function afterSheet(AfterSheet $event){

        //Single Column
        $event->sheet->styleCells(

            'A1',
            [
                'background' => [
                    'color'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKDOWN
                ]
            ]
        );

        //Range Columns
        $event->sheet->styleCells(
            'B2:E2',
            [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['argb' => '000000']
                ]
            ]
        );
    }

    public function headings(): array
    {

        return [
            'PayPeriodStart',
            'PayPeriodEnd',
            'PaidDate',
            'GrossEarnings',
            'NetEarnings',
        ];
    }

    public function query()
    {
        return Paystub::select('PayPeriodStart','PayPeriodEnd','PaidDate','GrossEarnings','NetEarnings')->where('hire_id', $this->id);
    }

}

