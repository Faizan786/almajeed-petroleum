<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Session::has('is_admin_logged_in')) {
            \Session::flash('message_title', 'Session Expired: ');
            \Session::flash('message', 'Please login Again!');
            return redirect('/cpadmin');
        }
        
        return $next($request);
    }
}
