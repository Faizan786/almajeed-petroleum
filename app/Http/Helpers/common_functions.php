<?php

function message_display() {
    if (\Session::get('message_title') != "" && \Session::get('message') != "") {
        $class_type = "alert-solid-success";
        $class_type_icon = "fa-exclamation-circle";
        if (\Session::get('message_type') != 'success') {
            $class_type = "alert-solid-danger";
            $class_type_icon = "fa-exclamation-triangle";
        }
        echo '<div class="alert ' . $class_type . ' alert-bold fade show kt-margin-t-10 kt-margin-b-10" role="alert">';
        echo '<div class="alert-icon"><i class="fa ' . $class_type_icon . '"></i></div>';
        echo '<div class="alert-text">' . \Session::get('message_title') . ' <br>' . \Session::get('message') . '</div>';
        echo '<div class="alert-close">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button>';
        echo '</div></div>';
    }
}

function message_display_admin() {
    if (\Session::get('message_title') != "" && \Session::get('message') != "") {
        $class_type = "alert-success";
        $class_type_icon = "flaticon-warning";
        if (\Session::get('message_type') != 'success') {
            $class_type = "alert-danger";
            $class_type_icon = "flaticon-questions-circular-button";
        }
        echo '<div class="alert ' . $class_type . ' alert-bold fade show" role="alert">';
        echo '<div class="alert-icon"><i class="' . $class_type_icon . '"></i></div>';
        echo '<div class="alert-text">' . \Session::get('message_title') . ' : ' . \Session::get('message') . '</div>';
        echo '<div class="alert-close">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button>';
        echo '</div></div>';
    }
}

function select_field_id($table_name, $id_value, $getfield) {
    $query = \DB::table($table_name)->find($id_value);
    if (isset($query) && !is_null($query)) {
        return $query->$getfield;
    }
}

function select_field_column($table_name, $parameter, $id_value, $getfield) {
    $query = \DB::table($table_name)->where($parameter, $id_value)->first();
    if (isset($query) && !is_null($query)) {
        return $query->$getfield;
    }
}

function select_dropdown($dropdown_parameters = "", $table_name = "", $whereArray = array(), $column1="", $column2="", $column3="") {
    $query = \DB::table($table_name)->where($whereArray)->get();
    if (isset($query) && !is_null($query)) {
        $dropdown = '<select '.$dropdown_parameters.'>';
        $dropdown .= '<option value="">Choose One</option>';
        foreach ($query as $value) {
            $custom_column1 = (trim($column1) != "") ? $value->$column1 : '';
            $custom_column2 = (trim($column2) != "") ? $value->$column2 : '';
            $custom_column3 = (trim($column3) != "") ? $value->$column3 : '';
            $dropdown .= "<option value='" . $value->id . "'>" . $custom_column1 . " " . $custom_column2 . " " . $custom_column3 . "</option>";
        }
        $dropdown .= '</select>';
        return $dropdown;
    }
}

function select_dropdown_customized($dropdown_name = "", $dropdown_id = "", $class_name = "", $style = "", $table_name = "", $whereArray = array(), $custom_column1 = "", $custom_column2 = "", $custom_column3 = "", $other_value = "") {
    $query = \DB::table($table_name)->where($whereArray)->get();
    if (isset($query) && !is_null($query)) {
        $dropdown = '<select class="' . $class_name . '" name="' . $dropdown_name . '" id="' . $dropdown_id . '" style="' . $style . '">';
        foreach ($query as $value) {
            $custom_column11 = (trim($custom_column1) != "") ? $value->$custom_column1 : '';
            $custom_column22 = (trim($custom_column2) != "") ? ' (' . $value->$custom_column2 . ') ' : '';
            $custom_column33 = (trim($custom_column3) != "") ? ' (' . $value->$custom_column3 . ') ' : '';
            $dropdown .= '<option value="' . $value->id . '">' . $custom_column11 . ' ' . $custom_column22 . ' ' . $custom_column33 . '</option>';
        }
        if($other_value != ""){
            $dropdown .= '<option value="other">Other</option>';
        }
        $dropdown .= '</select>';
        return $dropdown;
    }
}

function select_dropdown_with_brackets($dropdown_name = "", $dropdown_id = "", $class_name = "", $style = "", $table_name = "", $whereArray = array(), $custom_column1 = "", $custom_column2 = "", $custom_column3 = "") {
    $query = \DB::table($table_name)->where($whereArray)->get();
    if (isset($query) && !is_null($query)) {
        $dropdown = '<select class="' . $class_name . '" name="' . $dropdown_name . '" id="' . $dropdown_id . '" style="' . $style . '">';
        foreach ($query as $value) {
            $custom_column11 = (trim($custom_column1) != "") ? $value->$custom_column1 : '';
            $custom_column22 = (trim($custom_column2) != "") ? ' (' . $value->$custom_column2 : '';
            $custom_column33 = (trim($custom_column3) != "") ? $value->$custom_column3 . ') ' : '';
            $dropdown .= '<option value="' . $value->id . '">' . $custom_column11 . ' ' . $custom_column22 . ' ' . $custom_column33 . '</option>';
        }
        $dropdown .= '</select>';
        return $dropdown;
    }
}

//user role function
function user_roles_form_list($parent_id = 0, $level = 0, $uid = 0, $user_type = "") {
    $html = '';
    $user_role = \DB::table('roles_define')->where('parent_id', $parent_id)->get();
    //echo '<pre>'; print_r($user_role); die;
    foreach ($user_role as $row) {
        $checked = "";
        $color = '';
        $disabled = '';
        $primaryid = $row->id;
        $parentid = $row->parent_id;
        $pz_id = select_field_column('roles_define', 'id', $parentid, 'parent_id');
        $type = $row->type;

        $indent = str_repeat('&nbsp;', $level * 8);
        if ($type == 'Module') {
            $color = '#cccccc';
        } elseif ($type == 'Section') {
            $color = '#f1f1f1';
        }

        if ($user_type == "Admin") {
            $disabled = "";
        }
        if ($uid) {
            $check_role = \DB::table('roles_users')->where('user_id', $uid)->where('role_id', $primaryid)->count();
            if ($check_role > 0) {
                $checked = "checked";
            }
        }
        $html .= "<table width='100%' cellpadding='2' cellspacing='1'>";
        $html .= "<tr style='background-color:$color;'><td width='40%' class='rules-border'> $indent <label style='width:80%;'><input $disabled $checked type='checkbox' class='checkUncheck id_" . $primaryid . " pid_" . $parentid . " gpid_" . $pz_id . " type_" . $type . " user_type' data-id='" . $primaryid . "' data-parent='" . $parentid . "' data-grand-parent='" . $pz_id . "' data-page-type='" . $type . "' name='roles[]' value='" . $primaryid . "'  /> " . ucfirst(str_replace('_', ' ', $row->section_name)) . "</label> </td><td width='60%' class='rules-border2'> <input type='hidden' name='type[$primaryid]' value='" . $type . "' />";

        if ($row->has_edit == TRUE) {
            $html .= " <label style='width:24%;'><input $disabled $checked type='checkbox' name='role_edit[$primaryid]' value='1' class='actions id_" . $primaryid . " pid_" . $parentid . " gpid_" . $pz_id . " type_" . $type . " user_type' data-id='" . $primaryid . "' data-parent='" . $parentid . "' data-grand-parent='" . $pz_id . "' data-page-type='" . $type . "' /> &nbsp;Edit</label>";
        }
        if ($row->has_delete == TRUE) {
            $html .= " <label style='width:24%;'><input $disabled $checked type='checkbox' name='role_delete[$primaryid]' value='1' class='actions id_" . $primaryid . " pid_" . $parentid . " gpid_" . $pz_id . " type_" . $type . " user_type' data-id='" . $primaryid . "' data-parent='" . $parentid . "' data-grand-parent='" . $pz_id . "' data-page-type='" . $type . "' /> &nbsp;Delete</label>";
        }
        if ($row->has_detail == TRUE) {
            $html .= " <label style='width:24%;'><input $disabled $checked type='checkbox' name='role_detail[$primaryid]' value='1' class='actions id_" . $primaryid . " pid_" . $parentid . " gpid_" . $pz_id . " type_" . $type . " user_type' data-id='" . $primaryid . "' data-parent='" . $parentid . "' data-grand-parent='" . $pz_id . "' data-page-type='" . $type . "' /> &nbsp;Detail</label>";
        }
        if ($row->has_create == TRUE) {
            $html .= " <label style='width:24%;'><input $disabled $checked type='checkbox' name='role_create[$primaryid]' value='1' class='actions id_" . $primaryid . " pid_" . $parentid . " gpid_" . $pz_id . " type_" . $type . " user_type' data-id='" . $primaryid . "' data-parent='" . $parentid . "' data-grand-parent='" . $pz_id . "' data-page-type='" . $type . "' /> &nbsp;Create</label>";
        }
        $html .= "</td></tr>";

        $html .= user_roles_form_list($primaryid, $level + 1, $uid, $user_type);
    }
    $html .= "</table>";
    return $html;
}

//Check User Roles
function check_user_role($userId = '', $roleId = '', $has_role = '') {
    $check_role = \DB::table('roles_users')->where('user_id', $userId)->where('role_id', $roleId);
    if ($check_role->count() > 0) {
        $obj = $check_role->first();
        if ($has_role != "") {
            return $obj->$has_role;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

//Check User Roles
function admin_order_messages() {
    $check_role = \DB::table('messaging')->where('user_type', 'user')->where('status', 'unread')->count();
    return $check_role;
}

function get_time_ago($time) {
    $time_difference = time() - $time;
    if ($time_difference < 1) {
        return '1 Sec';
    }
    $condition = array(12 * 30 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'Month',
        7 * 24 * 60 * 60 => 'Week',
        24 * 60 * 60 => 'Day',
        60 * 60 => 'Hour',
        60 => 'Minute',
        1 => 'Second'
    );

    foreach ($condition as $secs => $str) {
        $d = $time_difference / $secs;
        if ($d >= 1) {
            $t = round($d);
            return $t . ' ' . $str . ( $t > 1 ? 's' : '' );
        }
    }
}

function display_paginations($total_pages, $page, $num_results_on_page) {
    $result = "";
    if(ceil($total_pages / $num_results_on_page) > 0){
        $result .= '<ul class="pagination" style="float:right;">';
        if ($page > 1){
            $result .= '<li class="paginate_button page-item page-link previous" data-page="'. ($page-1) .'"><i class="la la-angle-left"></i></li>';
        }
        if($page > 3){
            $result .= '<li class="paginate_button page-item page-link start" data-page="1">1</li>';
            $result .= '<li class="dots";>--</li>';
        }
        if($page-2 > 0){
            $result .= '<li class="paginate_button page-item page-link" data-page="'. ($page-2) .'">'. ($page-2) .'</li>';
        }
        if($page-1 > 0){
            $result .= '<li class="paginate_button page-item page-link" data-page="'. ($page-1) .'">'. ($page-1) .'</li>';
        }
        $result .= '<li class="paginate_button page-item page-link active" data-page="'. $page .'">'. $page .'</li>';
        if($page+1 < ceil($total_pages / $num_results_on_page)+1){
            $result .= '<li class="paginate_button page-item page-link" data-page="'. ($page+1) .'">'. ($page+1) .'</li>';
        }
        if ($page+2 < ceil($total_pages / $num_results_on_page)+1){
            $result .= '<li class="paginate_button page-item page-link" data-page="'. ($page+2) .'">'. ($page+2) .'</li>';
        }
        if($page < ceil($total_pages / $num_results_on_page)-2){
            $result .= '<li class="dots";>--</li>';
            $result .= '<li class="paginate_button page-item page-link end" data-page="'. ceil($total_pages / $num_results_on_page) .'">'. ceil($total_pages / $num_results_on_page) .'</li>';
        }
        if ($page < ceil($total_pages / $num_results_on_page)){
            $result .= '<li class="paginate_button page-item page-link next" data-page="'. ($page+1) .'"><i class="la la-angle-right"></i></li>';
        }
        $result .= '</ul>';
    }
    return $result;
}

function display_number_of_records($total_record = 0, $pageno = 0, $results_on_page = 0) {
    $html = "";
    if ($total_record > 0) {
        $html .= "Showing <b>";
        $stat_1 = ($results_on_page*($pageno-1))+1;        
        $html .= $stat_1." </b>to<b> ";
        if($total_record <= $results_on_page){
            $stat_2 = $total_record;
        } else if($pageno < ceil($total_record / $results_on_page)){
            $stat_2 = $pageno * $results_on_page;
        } else {
            $stat_2 = ($pageno * $results_on_page)-$total_record;
        }
        $html .= $stat_2."</b> of <b>" . $total_record . "</b> entries.";
    }
    return $html;
}

function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}