<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Users
 * @package    Laravel 5.8
 * @subpackage Model
 * @developer  Waqar Javed
 * @date       2 Aug, 2019
 */
class Messaging extends Model
{
    protected $table = 'messaging';
    protected $primaryKey = 'id';
    public $timestamps = true;
    
    /**
     * @param array
     * @return Array
     */
    public function populate($data) {
        
        if (array_key_exists('beneficiary_id', $data)) {
            $this->beneficiary_id = $data['beneficiary_id'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }
        if (array_key_exists('message', $data)) {
            $this->message = $data['message'];
        }
        if (array_key_exists('attachment', $data)) {
            $this->attachment = $data['attachment'];
        }
        if (array_key_exists('status', $data)) {
            $this->status = $data['status'];
        }
        if (array_key_exists('user_type', $data)) {
            $this->user_type = $data['user_type'];
        }
        if (array_key_exists('created_at', $data)) {
            $this->created_at = $data['created_at'];
        }
        if (array_key_exists('updated_at', $data)) {
            $this->updated_at = $data['updated_at'];
        }
        
    }
    
}
