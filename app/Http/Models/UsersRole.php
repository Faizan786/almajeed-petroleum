<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Users
 * @package    Laravel 5.8
 * @subpackage Model
 * @developer  Waqar Javed
 * @date       2 Aug, 2019
 */
class UsersRole extends Model
{
    protected $table = 'roles_users';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    /**
     * @param array
     * @return Array
     */
    public function populate($data) {
        
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        if (array_key_exists('role_id', $data)) {
            $this->role_id = $data['role_id'];
        }
        if (array_key_exists('type', $data)) {
            $this->type = $data['type'];
        }
        if (array_key_exists('has_create', $data)) {
            $this->has_create = $data['has_create'];
        }
        if (array_key_exists('has_edit', $data)) {
            $this->has_edit = $data['has_edit'];
        }
        if (array_key_exists('has_delete', $data)) {
            $this->has_delete = $data['has_delete'];
        }
        if (array_key_exists('has_detail', $data)) {
            $this->has_detail = $data['has_detail'];
        }
    }
    
}
