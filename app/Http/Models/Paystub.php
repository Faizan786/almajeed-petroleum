<?php
/**
 * Created by PhpStorm.
 * User: Tamim
 * Date: 12/7/2019
 * Time: 3:06 PM
 */

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;



class Paystub extends Model
{
    protected $table = 'paystub';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * @param array
     * @return Array
     */
    public function populate($data)
    {

        if (array_key_exists('beneficiary_id', $data)) {
            $this->beneficiary_id = $data['beneficiary_id'];
        }
        if (array_key_exists('hire_id', $data)) {
            $this->hire_id = $data['hire_id'];
        }
        if (array_key_exists('PayPeriodStart', $data)) {
            $this->PayPeriodStart = $data['PayPeriodStart'];
        }
        if (array_key_exists('PayPeriodEnd', $data)) {
            $this->PayPeriodEnd = $data['PayPeriodEnd'];
        }
        if (array_key_exists('PaidDate', $data)) {
            $this->PaidDate = $data['PaidDate'];
        }
        if (array_key_exists('GrossEarnings', $data)) {
            $this->GrossEarnings = $data['GrossEarnings'];
        }
        if (array_key_exists('NetEarnings', $data)) {
            $this->NetEarnings = $data['NetEarnings'];
        }
        if (array_key_exists('is_trashed', $data)) {
            $this->is_trashed = $data['is_trashed'];
        }
        if (array_key_exists('created_at', $data)) {
            $this->created_at = $data['created_at'];
        }
        if (array_key_exists('updated_at', $data)) {
            $this->updated_at = $data['updated_at'];
        }
        if (array_key_exists('deleted_at', $data)) {
            $this->deleted_at = $data['deleted_at'];
        }


    }


}
