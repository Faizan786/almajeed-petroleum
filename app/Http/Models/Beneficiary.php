<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Users
 * @package    Laravel 5.8
 * @subpackage Model
 * @developer  Waqar Javed
 * @date       2 Aug, 2019
 */
class Beneficiary extends Model
{
    protected $table = 'beneficiary';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * @param array
     * @return Array
     */
    public function populate($data)
    {

        if (array_key_exists('FirstName', $data)) {
            $this->FirstName = $data['FirstName'];
        }
        if (array_key_exists('LastName', $data)) {
            $this->LastName = $data['LastName'];
        }
        if (array_key_exists('SSN', $data)) {
            $this->SSN = $data['SSN'];
        }
        if (array_key_exists('Address', $data)) {
            $this->Address = $data['Address'];
        }
        if (array_key_exists('PhoneNumber', $data)) {
            $this->PhoneNumber = $data['PhoneNumber'];
        }
        if (array_key_exists('Email', $data)) {
            $this->Email = $data['Email'];
        }
        if (array_key_exists('Disabilities', $data)) {
            $this->Disabilities = $data['Disabilities'];
        }
        if (array_key_exists('PhysicalRestrictions', $data)) {
            $this->PhysicalRestrictions = $data['PhysicalRestrictions'];
        }
        if (array_key_exists('JobGoals', $data)) {
            $this->JobGoals = $data['JobGoals'];
        }
        if (array_key_exists('ROIOnFile', $data)) {
            $this->ROIOnFile = $data['ROIOnFile'];
        }
        if (array_key_exists('ReleaseToVerifyWages', $data)) {
            $this->ReleaseToVerifyWages = $data['ReleaseToVerifyWages'];
        }
        if (array_key_exists('BenefitsCounseling', $data)) {
            $this->BenefitsCounseling = $data['BenefitsCounseling'];
        }
        if (array_key_exists('SoftSkills', $data)) {
            $this->SoftSkills = $data['SoftSkills'];
        }
        if (array_key_exists('JSS', $data)) {
            $this->JSS = $data['JSS'];
        }
        if (array_key_exists('JobSearch', $data)) {
            $this->JobSearch = $data['JobSearch'];
        }
        if (array_key_exists('EnReportEarning', $data)) {
            $this->EnReportEarning = $data['EnReportEarning'];
        }
        if (array_key_exists('Retention', $data)) {
            $this->Retention = $data['Retention'];
        }
        if (array_key_exists('is_trashed', $data)) {
            $this->is_trashed = $data['is_trashed'];
        }
        if (array_key_exists('created_at', $data)) {
            $this->created_at = $data['created_at'];
        }
        if (array_key_exists('updated_at', $data)) {
            $this->updated_at = $data['updated_at'];
        }
        if (array_key_exists('deleted_at', $data)) {
            $this->deleted_at = $data['deleted_at'];
        }
    }

}
