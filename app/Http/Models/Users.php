<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Users
 * @package    Laravel 5.8
 * @subpackage Model
 * @developer  Waqar Javed
 * @date       2 Aug, 2019
 */
class Users extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = true;
    
    /**
     * @param array
     * @return Array
     */
    public function populate($data) {
        
        if (array_key_exists('first_name', $data)) {
            $this->first_name = $data['first_name'];
        }
        if (array_key_exists('last_name', $data)) {
            $this->last_name = $data['last_name'];
        }
        if (array_key_exists('fullname', $data)) {
            $this->fullname = $data['fullname'];
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('company', $data)) {
            $this->company = $data['company'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->generatePassword($data['password']);
        }
        if (array_key_exists('salt', $data)) {
            $this->salt = $data['salt'];
        }
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('phone_number', $data)) {
            $this->phone_number = $data['phone_number'];
        }
        if (array_key_exists('photo', $data)) {
            $this->photo = $data['photo'];
        }
        if (array_key_exists('country', $data)) {
            $this->country = $data['country'];
        }
        if (array_key_exists('email_verified', $data)) {
            $this->email_verified = $data['email_verified'];
        }
        if (array_key_exists('email_verified_at', $data)) {
            $this->email_verified_at = $data['email_verified_at'];
        }
        if (array_key_exists('user_type', $data)) {
            $this->user_type = $data['user_type'];
        }
        if(array_key_exists('last_visited', $data)) {
            $this->last_visited = $data['last_visited'];
        }
        if (array_key_exists('status', $data)) {
            $this->status = $data['status'];
        }
        if (array_key_exists('created_at', $data)) {
            $this->created_at = $data['created_at'];
        }
        if (array_key_exists('updated_at', $data)) {
            $this->updated_at = $data['updated_at'];
        }
        if (array_key_exists('deleted_at', $data)) {
            $this->deleted_at = $data['deleted_at'];
        }
    }
    
    /**
     * Function generatePassword
     * This function will generate a random string for encrypted passwords
     * @param $password
     * @return encrypted string
     */
    public function generatePassword($password) {
        $this->salt = \App\Http\Models\Users::getSalt();
        $this->password = crypt($password, $this->salt);
    }
    
    /**
     * Function getSalt
     * This function will generate a random string
     * @param empty
     * @return encrypted string
     */
    public static function getSalt() {
        $cost = 10;
        $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
        $salt = sprintf("$2a$%02d$", $cost) . $salt;
        return $salt;
    }
}
