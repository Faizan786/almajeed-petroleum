<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Users
 * @package    Laravel 5.8
 * @subpackage Model
 * @developer  Waqar Javed
 * @date       2 Aug, 2019
 */
class Hire extends Model
{
    protected $table = 'hire_information';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * @param array
     * @return Array
     */
    public function populate($data)
    {

        if (array_key_exists('beneficiary_id', $data)) {
            $this->beneficiary_id = $data['beneficiary_id'];
        }
        if (array_key_exists('JobType', $data)) {
            $this->JobType= $data['JobType'];
        }
        if (array_key_exists('HourlyWage', $data)) {
            $this->HourlyWage = $data['HourlyWage'];
        }
        if (array_key_exists('Employer', $data)) {
            $this->Employer = $data['Employer'];
        }
        if (array_key_exists('Benefits', $data)) {
            $this->Benefits = $data['Benefits'];
        }
        if (array_key_exists('Address', $data)) {
            $this->Address = $data['Address'];
        }
        if (array_key_exists('PayStart', $data)) {
            $this->PayStart = date('Y-m-d', strtotime($data['PayStart']));
        }
        if (array_key_exists('PayEnd', $data)) {
            $this->PayEnd = date('Y-m-d', strtotime($data['PayEnd']));
        }
        if (array_key_exists('PaidDate', $data)) {
            $this->PaidDate = date('Y-m-d', strtotime($data['PaidDate']));
        }
        if (array_key_exists('DateOfHire', $data)) {
            $this->DateOfHire = date('Y-m-d', strtotime($data['DateOfHire']));
        }
        if (array_key_exists('GrossEarnings', $data)) {
            $this->GrossEarnings = $data['GrossEarnings'];
        }
        if (array_key_exists('NetEarnings', $data)) {
            $this->NetEarnings = $data['NetEarnings'];
        }
        if (array_key_exists('EarningStructure', $data)) {
            $this->EarningStructure = $data['EarningStructure'];
        }
        if (array_key_exists('TWL', $data)) {
            $this->TWL = $data['TWL'];
        }
        if (array_key_exists('SGA', $data)) {
            $this->SGA = $data['SGA'];
        }
        if (array_key_exists('is_trashed', $data)) {
            $this->is_trashed = $data['is_trashed'];
        }
        if (array_key_exists('created_at', $data)) {
            $this->created_at = $data['created_at'];
        }
        if (array_key_exists('updated_at', $data)) {
            $this->updated_at = $data['updated_at'];
        }
        if (array_key_exists('deleted_at', $data)) {
            $this->deleted_at = $data['deleted_at'];
        }
    }


}
