<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Models\Users;
use App\Http\Models\Beneficiary;
use App\Http\Models\Messaging;

class MessagingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('guest');
    }

    
    public function MessageGetAll($beneficiary_id=0)
    {
        try {
            if($beneficiary_id == 0 || $beneficiary_id == ""){
                return array('type' => 'error', 'message' => "Order is not found!");
            }
            $data = array();
            $object = Messaging::where('beneficiary_id', $beneficiary_id)->orderBy('id', 'DESC')->get();
            //echo '<pre>';print_r($object); die;
            $key=0;
            foreach($object as $value){
                $data[$key]['id'] = $value['id'];
                $data[$key]['beneficiary_id'] = $value['beneficiary_id'];
                $data[$key]['user_id'] = $value['user_id'];
                $data[$key]['admin_id'] = $value['admin_id'];
                $data[$key]['user_name'] = select_field_id('users', $value['user_id'], "fullname");
                $data[$key]['admin_name'] = select_field_id('users', $value['admin_id'], "fullname");
                $data[$key]['message'] = $value['message'];
                $data[$key]['attachment'] = $value['attachment'];
                $data[$key]['status'] = $value['status'];
                $data[$key]['user_type'] = $value['user_type'];
                $data[$key]['created_at'] = get_time_ago(strtotime($value['created_at']));
            $key++;}
            return $data;
        } catch (\Exception $e) {
            return array('type' => 'error', 'message' => $e->getMessage());
        }
    }
    
    public function MessageSave()
    {
        $request = request();
        $data = $request->all();
        //print_r($data); die;
        try {
            if($data['message'] == ""){
                return array('type' => 'error', 'message' => 'Please fill the required field to proceed!');
            } else {
                $object = new Messaging();
                if(\Session::get('admin_user_type') == "employee"){
                    $data['user_id'] = \Session::get('admin_user_id');
                    $data['user_type'] = 'employee';
                } else {
                    $data['admin_id'] = \Session::get('admin_user_id');
                    $data['user_type'] = 'admin';
                }
                $object->populate($data);
                $object->save();
                
                return array('type' => 'success', 'message' => 'Message sent successfully!');
            }
        } catch (\Exception $e) {
            return array('type' => 'error', 'message' => $e->getMessage());
        }
    }
    
    
}
