<?php
/**
 * Created by PhpStorm.
 * User: Tamim
 * Date: 12/5/2019
 * Time: 12:56 PM
 */


namespace App\Http\Controllers\cpadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoulDoit\DataTable\SSP;
use App\Http\Models\Beneficiary;
use App\Http\Models\Hire;
use App\Http\Models\Messaging;


class ListBeneficiaryController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('authCheckAdmin');
    }

    public function Index($type = "") {
        $listType = "";
        if ($type == "active") {
            $listType = "Y";
        } else if ($type == "inactive") {
            $listType = "N";
        } else if ($type == "trashed") {
            $listType = "D";
        }
        return view('cpadmin.Beneficiary.list', ['title' => 'Control Panel - Active Beneficiary List', 'listType' => $listType]);
    }

    public function ListDataGet() {
        try {
            $request = request();
            $data = $request->all();
            $page = $data['page'];
            $pageno = (isset($data['pageno']) && $data['pageno'] > 0) ? $data['pageno'] : 1;
            $sort_column = (isset($data['sorting']) && trim($data['sorting']) != '') ? trim($data['sorting']) : '';
            $query_search = (isset($data['query_search']) && trim($data['query_search']) != '') ? trim($data['query_search']) : '';
            $results_on_page = (isset($data['size']) && $data['size'] > 0) ? $data['size'] : '';
            $offset = ($pageno-1)*$results_on_page;

            $object = Beneficiary::where('is_trashed', 'No');
            $object = Beneficiary::where('beneficiary.is_trashed', 'No')
                    ->leftJoin('hire_information as hire', 'hire.beneficiary_id', '=', 'beneficiary.id')
                    ->select('beneficiary.*', 'hire.id as hire_id');
            if ($query_search != "") {
                $object->where(function ($query) use ($query_search) {
                    if (validateDate($query_search, 'd') == true) {
                        $query->orwhereDay('beneficiary.created_at', "$query_search");
                    }
                    if (validateDate(ucfirst(strtolower(trim($query_search))), 'M') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'F') == true) {
                        $query->orwhereMonth('beneficiary.created_at', date('m', strtotime($query_search)));
                    }
                    if (validateDate($query_search, 'Y') == true && strlen($query_search) == 4) {
                        $query->orWhereYear('beneficiary.created_at', $query_search);
                    }
                    if (validateDate($query_search, 'M d, Y') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'M d, Y') == true || validateDate($query_search, 'F d, Y') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'F d, Y') == true) {
                        $query->orwhereDate('beneficiary.created_at', date('Y-m-d', strtotime($query_search)));
                    }
                    $query->orWhere('beneficiary.id', "$query_search");
                    $query->orWhere('beneficiary.FirstName', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.LastName', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.SSN', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.Address', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.PhoneNumber', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.Email', 'LIKE', "%$query_search%");
                    //$query->orWhere('beneficiary.DateTicketAssigned', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.Disabilities', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.PhysicalRestrictions', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.JobGoals', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.ROIOnFile', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.ReleaseToVerifyWages', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.BenefitsCounseling', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.SoftSkills', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.JSS', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.JobSearch', 'LIKE', "%$query_search%");
                    $query->orWhere('beneficiary.Retention', 'LIKE', "%$query_search%");

                });
            }
            if ($sort_column != "") {
                $exp_sort_column = explode('~', $sort_column);
                $object->orderBy($exp_sort_column[0], $exp_sort_column[1]);
            } else {
                $object->orderBy('beneficiary.id', 'DESC');
            }
            //print_r($object->toSql()); die;
            $array['counter'] = display_number_of_records($object->count(), $pageno, $results_on_page);
            $array['pagination'] = display_paginations($object->count(), $pageno, $results_on_page);
            $array['object'] = $object->offset($offset)->limit($results_on_page)->get();
            return $array;
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function ViewDetail() {
        $request = request();
        $data = $request->all();
        $id = $data['id'];
        try {
            $object = Beneficiary::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $obj['beneficiary'] = $object;
                $obj['hire'] = Hire::where('beneficiary_id', $object->id)->first();
             return $obj;
            } else {
                $data = array('error' => 1, 'message' => 'Object not found!');
                return $data;
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function CreateNew() {
        $request = request();
        $data = $request->all();
        //echo '<pre>'; print_r($data); die;
        try {
            if ($data['FirstName'] == "" && $data['LastName'] == ""){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Saved Failled!');
                \Session::flash('message', "Please fill all the required fields!");
                return redirect('/cpadmin/beneficiary-list');
            } else {
                $Beneficiary = new Beneficiary();
                $Beneficiary->populate($data);
                $Beneficiary->save();
                $data['beneficiary_id'] = $Beneficiary->id;
                $Hire = new Hire();
                $Hire->populate($data);
                $Hire->save();

                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Saved Successfull');
                \Session::flash('message', 'Beneficiary Created Successfull.');
                return redirect('/cpadmin/beneficiary-list/' );
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_type', 'error');
            \Session::flash('message_title', 'Saved Failled!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/beneficiary-list/');
        }
    }

    public function UpdateData() {
        try {
            $request = request();
            $data = $request->all();
            $beneficiary_id = $data['beneficiary_id'];
            $hire_id = $data['hire_id'];
            //echo '<pre>'; print_r($update); die;

            $object = Beneficiary::find($beneficiary_id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $object->populate($data);
                $object->save();

                $object_hire = Hire::find($hire_id);
                $object_hire->populate($data);
                $object_hire->save();

                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Update Successfull');
                \Session::flash('message', 'Beneficiary Updated Successfull.');
                return redirect('/cpadmin/beneficiary-list/' );
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Save Failled!');
                \Session::flash('message', "Object is not found!");
                return redirect('/cpadmin/beneficiary-list');
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_type', 'error');
            \Session::flash('message_title', 'Save Failled!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/beneficiary-list/');
        }
    }

    public function UpdateDetail() {
        try {
            $request = request();
            $data = $request->all();
            $id = $data['id'];
            $name = $data['name'];
            $value = $data['value'];
            $update[$name] = $value;
            //echo '<pre>'; print_r($update); die;

            $object = Beneficiary::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $object->populate($update);
                $object->save();

                $data = array('success' => 1, 'message' => 'Record saved successful!');
                return $data;
            } else {
                $data = array('error' => 1, 'message' => 'Object not found!');
                return $data;
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function TrashData() {
        try {
            $request = request();
            $data = $request->all();
            $id = $data['id'];

            $object = Beneficiary::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $data['is_trashed'] = "Yes";
                $object->populate($data);
                $object->save();

                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Delete Successfull');
                \Session::flash('message', 'Data deleted successfull.');
                return redirect('/cpadmin/beneficiary-list/');
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Delete Failed');
                \Session::flash('message', 'Invalid Data Provided May Be Session Expired, Please relogin.');
                return redirect('/cpadmin/beneficiary-list/');
            }
        } catch (\Exception $e) {
            \Session::flash('message_type', 'error');
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Delete Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/beneficiary-list/');
        }
    }

}
