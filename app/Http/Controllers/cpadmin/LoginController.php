<?php
namespace App\Http\Controllers\cpadmin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordMail;
use App\Http\Models\Users;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    /**
     * Show the application dashboard.
     * @return \Illuminate\Contracts\Support\Renderable
    */
    public function Login()
    {
        return view('cpadmin.login', ['title' => 'Control Panel Login']);
    }
    
    public function LoginData()
    {
        $request = request();
        $data = $request->all();
        try {
            $email_username = $data['username'];
            $password = $data['password'];
            
            if($data['username'] == "" && $data['password'] == ""){
                \Session::flash('message_title', 'Login Failled!');
                \Session::flash('message', "Please fill all the required fields!");
                return redirect('/cpadmin/');
            }
            
            $object_credentials = Users::where('username', $email_username)->where('user_type', '!=', 'user')->first();
            //echo $object_credentials->toSql();
            if(isset($object_credentials) && !is_null($object_credentials) && $object_credentials->count() > 0){
                $user_password = $object_credentials->password;
                $user_salt = $object_credentials->salt;
                $entered_password = \crypt($password, $user_salt);
                if ($user_password != $entered_password) {
                    \Session::flash('message_title', 'Login Failled!');
                    \Session::flash('message', 'Incorect Credentials, Please re-type (username/email) and password.');
                    return redirect('/cpadmin/');
                } else {
                    \Session::put('is_admin_logged_in', true);
                    \Session::put('admin_user_id', $object_credentials->id);
                    \Session::put('admin_first_name', $object_credentials->first_name);
                    \Session::put('admin_last_name', $object_credentials->last_name);
                    \Session::put('admin_fullname', $object_credentials->fullname);
                    \Session::put('admin_email', $object_credentials->email);
                    \Session::put('admin_username', $object_credentials->username);
                    \Session::put('admin_company', $object_credentials->company);
                    \Session::put('admin_phone_number', $object_credentials->phone_number);
                    \Session::put('admin_photo', $object_credentials->photo);
                    \Session::put('admin_user_type', $object_credentials->user_type);
                    \Session::put('email_verified_at', $object_credentials->email_verified_at);
                    
                    //$redirectSeconds = '<script>window.setTimeout(function() {window.location.href = "http://www.google.com"; }, 2000);</script>';
                    \Session::flash('message_title', 'Login Successfull');
                    \Session::flash('message', 'You are being redirect to your account please wait...');
                    return redirect('/cpadmin/dashboard');
                }
            } else {
                \Session::flash('message_title', 'Login Failed');
                \Session::flash('message', 'Incorect Credentials, Please re-type (username/email) and password.');
                return redirect('/cpadmin/');
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Login Failled!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/');
        }
    }
    
    public function ForgotPassword()
    {
        return view('cpadmin.forgotpass', ['title' => 'Control Panel Forgot Password']);
    }
    
    public function ForgotPasswordData()
    {
        $request = request();
        $data = $request->all();
        \DB::beginTransaction();
        try {
            if($data['email'] == ""){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Oops!');
                \Session::flash('message', 'Please enter email address!');
                return redirect('cpadmin/forgot-password');
            }
            $object = Users::where('email', $data['email']);
            if($object->count() > 0){
                $obdata = $object->first();
                $temp_pass = substr(md5(uniqid()), 0, 7);
                $obdata->populate(array('password' => $temp_pass));
                $obdata->save();
                $obdata->temp_pass = $temp_pass;
                
                Mail::to($data['email'])->send(new ForgotPasswordMail($obdata));
                \DB::commit();
                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Thank You!');
                \Session::flash('message', 'Your new temporary password has been sent to your email address. Please check your inbox or span.');
                return redirect('cpadmin/forgot-password');
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Oops!');
                \Session::flash('message', 'This email address is not exist into our database!');
                return redirect('cpadmin/forgot-password');
            }
        } catch (\Exception $e){
            \DB::rollback();
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_type', 'error');
            \Session::flash('message_title', 'Oops!');
            \Session::flash('message', $e->getMessage());
            return redirect('cpadmin/forgot-password');
        }
    }
    
    public function LogOut()
    {
        \Session::flush();
        \Session::flash('message_title', 'Good Bye!');
        \Session::flash('message', 'You are logged out successfully. See you again!');
        return redirect('/cpadmin/');
    }
    
}
