<?php
namespace App\Http\Controllers\cpadmin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Users;
use App\Http\Models\Orders;
use App\Http\Models\Messaging;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authCheckAdmin');
    }
    
    /**
     * Show the application dashboard.
     * @return \Illuminate\Contracts\Support\Renderable
    */
    public function Dashboard()
    {
        $data['users'] = Users::where('user_type', 'user')->where('status', 'N')->offset(0)->limit(10)->get();
        $data['messages'] = Messaging::where('user_type', 'user')->where('status', 'unread')->offset(0)->limit(6)->get();
        return view('cpadmin.dashboard', ['title' => 'Control Panel Dashboard', 'data' => $data]);
    }
    
    public function Profile()
    {
        return view('cpadmin.profile', ['title' => 'Control Panel Profile']);
    }
    
    public function ProfileData()
    {
        $request = request();
        $data = $request->all();
        $admin_user_id = \Session::get('admin_user_id');
        //echo '<pre>'; print_r($data); die;
        try {
            $countEmail = Users::Where('email', $data['email'])->where('id', '!=', $admin_user_id)->get()->count();
            if($countEmail > 0){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failed!');
                \Session::flash('message', "Email already existed! Please choose another email.");
                return redirect('/cpadmin/profile');
            }
            if($data['email'] == "" && $data['first_name'] == "" && $data['last_name'] == "" && $data['company'] == ""){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failed!');
                \Session::flash('message', "Please fill all the required fields!");
                return redirect('/cpadmin/profile');
            }
            
            $object = Users::find($admin_user_id);
            if(isset($object) && !is_null($object) && $object->count() > 0){
                if ($request->hasFile('profile_avatar')) {
                    $realFileName = $request->file('profile_avatar')->getClientOriginalName();
                    $realFileExt = $request->file('profile_avatar')->getClientOriginalExtension();
                    $PrefixfileName = \Session::get('admin_username').'_'.substr(md5(uniqid()), 0,6);
                    $FilePath = public_path().'/uploaded_files/profile_avatars/';
                    $fileName = $PrefixfileName.'.'.$realFileExt;
                    $request->file('profile_avatar')->move( $FilePath, $fileName );
                    if(file_exists($FilePath.$object->photo)){
                        unlink($FilePath.$object->photo);
                    }
                    $data['photo'] = $fileName;
                }
                
                $object->populate($data);
                $object->save();
                
                \Session::put('admin_user_id', $object->id);
                \Session::put('admin_first_name', $object->first_name);
                \Session::put('admin_last_name', $object->last_name);
                \Session::put('admin_email', $object->email);
                \Session::put('admin_username', $object->username);
                \Session::put('admin_company', $object->company);
                \Session::put('admin_phone_number', $object->phone_number);
                \Session::put('admin_photo', $object->photo);
                \Session::put('admin_user_type', $object->user_type);
                \Session::put('email_verified_at', $object->email_verified_at);

                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Update Successfull');
                \Session::flash('message', 'Profile updated successfull.');
                return redirect('/cpadmin/profile');
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failed');
                \Session::flash('message', 'Session Expired, Please relogin.');
                return redirect('/cpadmin/profile');
            }
        } catch (\Exception $e) {
            \Session::flash('message_type', 'error');
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Update Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/profile');
        }
    }
    
    public function ChangePassword()
    {
        $request = request();
        $data = $request->all();
        
        try {
            $user_id = \Session::get('admin_user_id');
            $old_password = $data['old_password'];
            $add_new_pass = $data['choose_password'];
            $add_confirm_pass = $data['again_password'];
            
            if($old_password == "" && $add_new_pass == "" && $add_confirm_pass == ""){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failled!');
                \Session::flash('message', "Please fill all passwords fields!");
                return redirect('/cpadmin/profile');
            } else if($add_new_pass != "" && $add_new_pass != $add_confirm_pass){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failled!');
                \Session::flash('message', "New and Verify Passwords Mismatched!");
                return redirect('/cpadmin/profile');
            } else {
                $object_credentials = Users::findOrFail($user_id);
                if(isset($object_credentials) && !is_null($object_credentials) && $object_credentials->count() > 0){
                    $exist_password = $object_credentials->password;
                    $entered_password = \crypt($old_password, $object_credentials->salt);
                    if ($exist_password != $entered_password) {
                        \Session::flash('message_type', 'error');
                        \Session::flash('message_title', 'Update Failled!');
                        \Session::flash('message', 'Old password is incorrect!');
                        return redirect('/cpadmin/profile');
                    } else {
                        $updatedata['password'] = $add_new_pass;
                        $object_credentials->populate($updatedata);
                        $object_credentials->save();
                        
                        \Session::flash('message_type', 'success');
                        \Session::flash('message_title', 'Profile Update Successfull');
                        \Session::flash('message', 'Password changed successfully!');
                        return redirect('/cpadmin/profile');
                    }
                }
            }
            
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_type', 'error');
            \Session::flash('message_title', 'Update Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/profile');
        }
    }
    
}
