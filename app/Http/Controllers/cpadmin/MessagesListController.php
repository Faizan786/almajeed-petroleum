<?php

namespace App\Http\Controllers\cpadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Users;
use App\Http\Models\Messaging;
use App\Http\Models\Beneficiary;

class MessagesListController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('authCheckAdmin');
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Index() {
        return view('cpadmin.ListMessages.list', ['title' => 'Clients Messages Control Panel', 'dt_info' => ['labels' => '', 'beneficiary' => '']]);
    }

    public function GetMessagesChat() {
        try {
            $request = request();
            $dataGet = $request->all();
            $beneficiary_id = $dataGet['beneficiary'];
            $user_id = $dataGet['user'];
            $object['beneficiary'] = Beneficiary::find($beneficiary_id);
            $object['user'] = Users::find($user_id);
            $messages = Messaging::where('beneficiary_id', $beneficiary_id)->OrderBy('status', 'ASC')->OrderBy('created_at', 'ASC')->get();
            foreach ($messages as $key => $value) {
                $id = $value['id'];
                $object['message'][$key]['beneficiary_id'] = $value['beneficiary_id'];
                $object['message'][$key]['user_id'] = $value['user_id'];
                $object['message'][$key]['admin_id'] = $value['admin_id'];
                $object['message'][$key]['client_username'] = select_field_id('users', $value['user_id'], 'username');
                $object['message'][$key]['admin_username'] = select_field_id('users', $value['admin_id'], 'username');
                $object['message'][$key]['client_photo'] = select_field_id('users', $value['user_id'], 'photo');
                $object['message'][$key]['admin_photo'] = select_field_id('users', $value['admin_id'], 'photo');
                $object['message'][$key]['message'] = $value['message'];
                $object['message'][$key]['status'] = $value['status'];
                $object['message'][$key]['user_type'] = $value['user_type'];
                $object['message'][$key]['created_at'] = get_time_ago(strtotime($value['created_at']));

                $obj = Messaging::find($id);
                $obj->populate(array('status' => 'read'));
                $obj->save();
            }
            //return print_r($object);
            return $object;
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function GetMessagesList() {
        try {
            $request = request();
            $dataGet = $request->all();
            $search = (isset($dataGet['search'])) ? $dataGet['search'] : '';
            $data = array();
            if ($search != "") {
                $object = Messaging::where('beneficiary_id', $search)->groupBy('beneficiary_id')->OrderBy('status', 'DESC')->OrderBy('created_at', 'DESC')->get();
            } else {
                $object = Messaging::groupBy('beneficiary_id')->OrderBy('status', 'DESC')->OrderBy('created_at', 'DESC')->get();
            }
            foreach ($object as $key => $value) {
                $user_ob = Users::find(($value['user_id'] != "")?$value['user_id']:$value['admin_id']);
                $bene_ob = Beneficiary::find($value['beneficiary_id']);
                $unread_msg_count = Messaging::where('beneficiary_id', $value['beneficiary_id'])->where('status', 'unread')->count();
                $update['unread_msg_count'] = $unread_msg_count;
                $update['beneficiary_id'] = $value['beneficiary_id'];
                $update['user_id'] = $value['user_id'];
                $update['admin_id'] = $value['admin_id'];
                $update['message'] = $value['message'];
                $update['status'] = $value['status'];
                $update['user_type'] = $value['user_type'];
                $update['created_at'] = get_time_ago(strtotime($value['created_at']));
                $update['first_name'] = $user_ob['first_name'];
                $update['last_name'] = $user_ob['last_name'];
                $update['email'] = $user_ob['email'];
                $update['username'] = $user_ob['username'];
                $update['company'] = $user_ob['company'];
                $update['phone_number'] = $user_ob['phone_number'];
                $update['photo'] = $user_ob['photo'];
                $update['country'] = $user_ob['country'];
                $update['request_type'] = $bene_ob['request_type'];
                array_push($data, $update);
            }
            return $data;
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function SubmitMessagesChat() {
        try {
            $request = request();
            $dataGet = $request->all();
            ///$update['beneficiary_id'] = $dataGet['beneficiary'];
            $update['user_id'] = $dataGet['user'];
            $update['message'] = $dataGet['msg'];
            $update['admin_id'] = \Session::get('admin_user_id');
            $update['status'] = 'unread';
            $update['user_type'] = 'admin';

            $object = new Messaging();
            $object->populate($update);
            $object->save();

            $return['admin_message'] = $object->message;
            $return['admin_created_at'] = get_time_ago(strtotime($object->created_at));
            $return['admin_username'] = \Session::get('admin_username');
            $return['admin_photo'] = \Session::get('admin_photo');
            return $return;
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function ClearMessagesChat() {
        \DB::beginTransaction();
        try {
            $request = request();
            $dataGet = $request->all();
            $beneficiary_id = $dataGet['beneficiary'];
            $object = Messaging::where('beneficiary_id', $beneficiary_id);
            $object->delete();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

}
