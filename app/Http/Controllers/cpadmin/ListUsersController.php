<?php

namespace App\Http\Controllers\cpadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoulDoit\DataTable\SSP;
use App\Http\Models\Users;
use App\Http\Models\UsersRole;
use App\Http\Models\Messaging;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccountInactiveMail;
use App\Mail\AccountActiveMail;

class ListUsersController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('authCheckAdmin');
    }

    public function Index($type = "") {
        $listType = "";
        if ($type == "active") {
            $listType = "Y";
        } else if ($type == "inactive") {
            $listType = "N";
        } else if ($type == "trashed") {
            $listType = "D";
        }
        return view('cpadmin.ListUsers.list', ['title' => 'Control Panel - Active Users List', 'listType' => $listType]);
    }

    public function ListDataGet() {
        try {
            $request = request();
            $data = $request->all();
            $page = $data['page'];
            $pageno = (isset($data['pageno']) && $data['pageno'] > 0) ? $data['pageno'] : 1;
            $sort_column = (isset($data['sorting']) && trim($data['sorting']) != '') ? trim($data['sorting']) : '';
            $query_search = (isset($data['query_search']) && trim($data['query_search']) != '') ? trim($data['query_search']) : '';
            $results_on_page = (isset($data['size']) && $data['size'] > 0) ? $data['size'] : '';
            $offset = ($pageno-1)*$results_on_page;

            $object = Users::where('status', $page);
            if ($query_search != "") {
                $object->where(function ($query) use ($query_search) {
                    if (validateDate($query_search, 'd') == true) {
                        $query->orwhereDay('created_at', "$query_search");
                    }
                    if (validateDate(ucfirst(strtolower(trim($query_search))), 'M') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'F') == true) {
                        $query->orwhereMonth('created_at', date('m', strtotime($query_search)));
                    }
                    if (validateDate($query_search, 'Y') == true && strlen($query_search) == 4) {
                        $query->orWhereYear('created_at', $query_search);
                    }
                    if (validateDate($query_search, 'M d, Y') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'M d, Y') == true || validateDate($query_search, 'F d, Y') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'F d, Y') == true) {
                        $query->orwhereDate('created_at', date('Y-m-d', strtotime($query_search)));
                    }
                    $query->orWhere('id', "$query_search");
                    $query->orWhere('first_name', 'LIKE', "%$query_search%");
                    $query->orWhere('last_name', 'LIKE', "%$query_search%");
                    $query->orWhere('fullname', 'LIKE', "%$query_search%");
                    $query->orWhere('gender', 'LIKE', "%$query_search%");
                    $query->orWhere('email', 'LIKE', "%$query_search%");
                    $query->orWhere('username', 'LIKE', "%$query_search%");
                    $query->orWhere('company', 'LIKE', "%$query_search%");
                    $query->orWhere('phone_number', 'LIKE', "%$query_search%");
                    $query->orWhere('country', 'LIKE', "%$query_search%");
                    $query->orWhere('user_type', 'LIKE', "%$query_search%");
                });
            }
            if ($sort_column != "") {
                $exp_sort_column = explode('~', $sort_column);
                $object->orderBy($exp_sort_column[0], $exp_sort_column[1]);
            } else {
                $object->orderBy('id', 'DESC');
            }
            //print_r($object->toSql()); die;
            $array['counter'] = display_number_of_records($object->count(), $pageno, $results_on_page);
            $array['pagination'] = display_paginations($object->count(), $pageno, $results_on_page);
            $array['object'] = $object->offset($offset)->limit($results_on_page)->get();
            return $array;
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function ViewDetail() {
        $request = request();
        $data = $request->all();
        $id = $data['id'];
        try {
            $object = Users::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                unset($object->password);
                unset($object->salt);
                $object['user_rights'] = user_roles_form_list(0, 0, $id, '');
                return $object;
            } else {
                $data = array('error' => 1, 'message' => 'Object not found!');
                return $data;
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function ChangeStatus() {
        $request = request();
        $data = $request->all();
        $id = $data['id'];
        $value = $data['value'];
        try {
            $object = Users::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                if ($value == "Active") {
                    $data['status'] = "N";
                    Mail::to($object->email)->send(new AccountInactiveMail($object));
                } elseif ($value == "Inactive") {
                    $data['status'] = "Y";
                    Mail::to($object->email)->send(new AccountActiveMail($object));
                } elseif ($value == "Deleted") {
                    $data['status'] = "Y";
                }
                $object->populate($data);
                $object->save();

                return $object->status;
            } else {
                return "error";
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            return "error";
        }
    }

    public function CreateNew($type = "") {
        $request = request();
        $data = $request->all();
        if ($type == "Y") {
            $listType = "active";
        } else if ($type == "N") {
            $listType = "inactive";
        } else if ($type == "D") {
            $listType = "trashed";
        }
        //echo '<pre>'; print_r($data); die;
        try {
            $object_username = \App\Http\Models\Users::where('username', $data['username'])->get();
            $object_email = \App\Http\Models\Users::where('email', $data['email'])->get();
            if ($data['first_name'] == "" && $data['last_name'] == "" && $data['username'] == "" && $data['email'] == "" && $data['password'] == "") {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Registration Failled!');
                \Session::flash('message', "Please fill all the required fields!");
                return redirect('/cpadmin/users-list');
            } else if ($data['password'] != $data['confirm_password']) {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Registration Failled!');
                \Session::flash('message', "Passwords Mismatched!");
                return redirect('/cpadmin/users-list');
            } else if ($object_username->count() > 0) {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Registration Failled!');
                \Session::flash('message', "Username already exist!");
                return redirect('/cpadmin/users-list/' . $listType);
            } else if ($object_email->count() > 0) {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Registration Failled!');
                \Session::flash('message', "Email already exist!");
                return redirect('/cpadmin/users-list/' . $listType);
            } else {
                if ($request->hasFile('profile_avatar')) {
                    $realFileName = $request->file('profile_avatar')->getClientOriginalName();
                    $realFileExt = $request->file('profile_avatar')->getClientOriginalExtension();
                    $PrefixfileName = \Session::get('admin_username') . '_' . substr(md5(uniqid()), 0, 6);
                    $FilePath = public_path() . '/uploaded_files/profile_avatars/';
                    $fileName = $PrefixfileName . '.' . $realFileExt;
                    $request->file('profile_avatar')->move($FilePath, $fileName);
                    $data['photo'] = $fileName;
                }

                $data['fullname'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['status'] = "Y";
                $users = new Users();
                $users->populate($data);
                $users->save();

                if (isset($data['roles']) && count($data['roles']) > 0) {
                    foreach ($data['roles'] as $key => $value) {
                        $roles['role_id'] = $value;
                        $roles['user_id'] = $users->id;
                        $roles['type'] = (isset($data['type'][$value])) ? $data['type'][$value] : '';
                        $roles['has_edit'] = (isset($data['role_edit'][$value])) ? $data['role_edit'][$value] : 0;
                        $roles['has_delete'] = (isset($data['role_delete'][$value])) ? $data['role_delete'][$value] : 0;
                        $roles['has_detail'] = (isset($data['role_detail'][$value])) ? $data['role_detail'][$value] : 0;
                        $roles['has_create'] = (isset($data['role_create'][$value])) ? $data['role_create'][$value] : 0;

                        //echo '<pre>'; print_r($roles);
                        $UsersRole = new UsersRole();
                        $UsersRole->populate($roles);
                        $UsersRole->save();
                    }
                }
                //die;
                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Registration Successfull');
                \Session::flash('message', 'Account Created Successfull And Held For Review! We get back to you soon.');
                return redirect('/cpadmin/users-list/' . $listType);
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_type', 'error');
            \Session::flash('message_title', 'Registration Failled!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/users-list/' . $listType);
        }
    }

    public function EditDetail($type = "") {
        $request = request();
        $data = $request->all();
        if ($type == "Y") {
            $listType = "active";
        } else if ($type == "N") {
            $listType = "inactive";
        } else if ($type == "D") {
            $listType = "trashed";
        }
        //echo '<pre>'; print_r($data); die;

        $user_id = $data['id'];
        try {
            $countEmail = Users::Where('email', $data['email'])->where('id', '!=', $user_id)->get()->count();
            if ($countEmail > 0) {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failed!');
                \Session::flash('message', "Email already existed! Please choose another email.");
                return redirect('/cpadmin/users-list/' . $listType);
            }
            if ($data['email'] == "" && $data['first_name'] == "" && $data['last_name'] == "" && $data['company'] == "") {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failed!');
                \Session::flash('message', "Please fill all the required fields!");
                return redirect('/cpadmin/users-list/' . $listType);
            }
            if ($data['password'] != "" && $data['password'] != $data['confirm_password']) {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failled!');
                \Session::flash('message', "New and Confirm Passwords Mismatched!");
                return redirect('/cpadmin/users-list/' . $listType);
            }
            if ($data['password'] == "") {
                unset($data['password']);
            }
            $object = Users::find($user_id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                if ($request->hasFile('profile_avatar')) {
                    $realFileName = $request->file('profile_avatar')->getClientOriginalName();
                    $realFileExt = $request->file('profile_avatar')->getClientOriginalExtension();
                    $PrefixfileName = \Session::get('admin_username') . '_' . substr(md5(uniqid()), 0, 6);
                    $FilePath = public_path() . '/uploaded_files/profile_avatars/';
                    $fileName = $PrefixfileName . '.' . $realFileExt;
                    $request->file('profile_avatar')->move($FilePath, $fileName);
                    if (file_exists($FilePath . $object->photo) && $object->photo != "") {
                        unlink($FilePath . $object->photo);
                    }
                    $data['photo'] = $fileName;
                }
                $data['fullname'] = $data['first_name'].' '.$data['last_name'];
                $object->populate($data);
                $object->save();

                if (isset($data['roles']) && count($data['roles']) > 0) {
                    UsersRole::where('user_id', $user_id)->delete();
                    foreach ($data['roles'] as $key => $value) {
                        $roles['role_id'] = $value;
                        $roles['user_id'] = $user_id;
                        $roles['type'] = (isset($data['type'][$value])) ? $data['type'][$value] : '';
                        $roles['has_edit'] = (isset($data['role_edit'][$value])) ? $data['role_edit'][$value] : 0;
                        $roles['has_delete'] = (isset($data['role_delete'][$value])) ? $data['role_delete'][$value] : 0;
                        $roles['has_detail'] = (isset($data['role_detail'][$value])) ? $data['role_detail'][$value] : 0;
                        $roles['has_create'] = (isset($data['role_create'][$value])) ? $data['role_create'][$value] : 0;

                        $UsersRole = new UsersRole();
                        $UsersRole->populate($roles);
                        $UsersRole->save();
                    }
                }

                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Update Successfull');
                \Session::flash('message', 'User updated successfull.');
                return redirect('/cpadmin/users-list/' . $listType);
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Update Failed');
                \Session::flash('message', 'Session Expired, Please relogin.');
                return redirect('/cpadmin/users-list/' . $listType);
            }
        } catch (\Exception $e) {
            \Session::flash('message_type', 'error');
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Update Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/users-list/' . $listType);
        }
    }

    public function DeleteData($type = "") {
        if ($type == "Y") {
            $listType = "active";
        } else if ($type == "N") {
            $listType = "inactive";
        } else if ($type == "D") {
            $listType = "trashed";
        }
        $request = request();
        $data = $request->all();
        $id = $data['id'];
        try {
            $object = Users::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $data['status'] = "D";
                $object->populate($data);
                $object->save();
                
                Messaging::where('user_id', $object->id)->delete();
                
                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Delete Successfull');
                \Session::flash('message', 'User deleted successfull.');
                return redirect('/cpadmin/users-list/' . $listType);
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Delete Failed');
                \Session::flash('message', 'Invalid Data Provided May Be Session Expired, Please relogin.');
                return redirect('/cpadmin/users-list/' . $listType);
            }
        } catch (\Exception $e) {
            \Session::flash('message_type', 'error');
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Delete Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/users-list/' . $listType);
        }
    }

    public function EmptyTrashed($type = "") {
        if ($type == "Y") {
            $listType = "active";
        } else if ($type == "N") {
            $listType = "inactive";
        } else if ($type == "D") {
            $listType = "trashed";
        }
        $request = request();
        $data = $request->all();
        $id = $data['id'];
        try {
            $object = Users::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $object->delete();
                Messaging::where('user_id', $object->id)->delete();
                
                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Delete Successfull');
                \Session::flash('message', 'User deleted permanent successfull.');
                return redirect('/cpadmin/users-list/' . $listType);
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Delete Failed');
                \Session::flash('message', 'Invalid Data Provided May Be Session Expired, Please relogin.');
                return redirect('/cpadmin/users-list/' . $listType);
            }
        } catch (\Exception $e) {
            \Session::flash('message_type', 'error');
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Delete Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/users-list/' . $listType);
        }
    }

    public function AllEmptyTrashed($type = "") {
        if ($type == "Y") {
            $listType = "active";
        } else if ($type == "N") {
            $listType = "inactive";
        } else if ($type == "D") {
            $listType = "trashed";
        }
        try {
            $object = Users::Where('status', 'D')->get();
            if($object->count() > 0){
                $object_del = Users::Where('status', 'D')->delete();
                
                foreach($object as $key => $value){
                    Messaging::where('user_id', $value->id)->delete();
                }
                
                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Trashed Successfull');
                \Session::flash('message', 'Trashed emptied successfull.');
                return redirect('/cpadmin/users-list/' . $listType);
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Trashed Failed');
                \Session::flash('message', 'No trashed data found.');
                return redirect('/cpadmin/users-list/' . $listType);
            }
        } catch (\Exception $e) {
            \Session::flash('message_type', 'error');
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Trashed Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/users-list/' . $listType);
        }
    }

}
