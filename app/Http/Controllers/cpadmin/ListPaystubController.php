<?php
/**
 * Created by PhpStorm.
 * User: Tamim
 * Date: 12/5/2019
 * Time: 12:56 PM
 */


namespace App\Http\Controllers\cpadmin;
use App\Http\Controllers\Controller;
use App\Http\Models\Paystub;
use App\Http\Models\Beneficiary;
use App\Http\Models\Hire;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;



class   ListPaystubController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('authCheckAdmin');
    }

    public function PayStub() {
        $request = request();
        $data = $request->all();
        //echo '<pre>'; print_r($data); die;
        try {
            if (
                $data['PayPeriodStart'] == "" && $data['PayPeriodEnd'] == "" && $data['PaidDate'] == ""
                && $data['GrossEarnings'] == "" && $data['NetEarnings'] == "" && $data['beneficiary_id'] == "" && $data['hire_id'] == ""){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Registration Failled!');
                \Session::flash('message', "Please fill all the required fields!");
                return redirect('/cpadmin/beneficiary-list');
            } else {
                $Paystub = new Paystub();
                $Paystub->populate($data);
                $Paystub->save();
                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Registration Successfull');
                \Session::flash('message', 'Pay Stub Created Successfull .');
                return redirect('/cpadmin/beneficiary-list/' );
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_type', 'error');
            \Session::flash('message_title', 'Registration Failled!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/beneficiary-list/');
        }
    }

   public function ExportExcel($id)
   {
        $bene_qry = Beneficiary::find($id);
        $hire_qry = Hire::where('beneficiary_id', $bene_qry->id)->first();
        $qry = Paystub::Where('beneficiary_id', $bene_qry->id)->Where('hire_id', $hire_qry->id);
        $query = $qry->get();
        $queryCount = $qry->count();
       // CREATE A NEW SPREADSHEET + SET METADATA
       $spreadsheet = new Spreadsheet();
       $spreadsheet->getProperties()
           ->setCreator('Endb Portal')
           ->setLastModifiedBy('Endb Portal')
           ->setTitle('Report Document')
           ->setSubject('Report Document')
           ->setDescription('Report Document')
           ->setKeywords('Report ')
           ->setCategory('Report file');
          // CREATE A NEW SPREADSHEET + POPULATE DATA
       $spreadsheet = new Spreadsheet();
       $sheet = $spreadsheet->getActiveSheet();
       //heading of table
       $sheet ->getCell('A6')->setValue('Pay Period Start');
       $sheet ->getCell('B6')->setValue('Pay Period End');
       $sheet ->getCell('C6')->setValue('Paid Date');
       $sheet ->getCell('A5')->setValue('Pay Period');
       $sheet ->getCell('D6')->setValue('Gross Earnings');
       $sheet ->getCell('E6')->setValue('Net Earnings');
       $sheet ->getCell('C1')->setValue('Monthly Earning Estimator');
       $sheet ->getCell('A2')->setValue('Beneficiary');
       $sheet ->getCell('A4')->setValue('Beneficiary Earning Details');
       $sheet ->getCell('C2')->setValue('Beneficiary SSN');
       $sheet ->getCell('C3')->setValue('Beneficiary Name');
       $sheet ->getCell('D2')->setValue($bene_qry->SSN);
       $sheet ->getCell('D3')->setValue($bene_qry->FirstName.' '.$bene_qry->LastName);
       $spreadsheet->getActiveSheet()->getCell('G6')->setValue('Possible Claims');
       $sheet ->getCell('I6')->setValue('SSDI');
       $sheet ->getCell('K6')->setValue('SSI');
       $sheet ->getCell('G15')->setValue('');
       $sheet ->getCell('H15')->setValue('Below');
       $sheet ->getCell('I15')->setValue('TWL');
       $sheet ->getCell('J15')->setValue('SGA');
       $sheet ->getCell('K15')->setValue('SGA for Blind');
       $sheet ->getCell('G16')->setValue('Phase-i');
       $sheet ->getCell('G17')->setValue('Phase-ii');
       $sheet ->getCell('G18')->setValue('Phase-iii');
       $sheet ->getCell('H16')->setValue('No');
       $sheet ->getCell('H17')->setValue('No');
       $sheet ->getCell('H18')->setValue('No');
       $sheet ->getCell('I16')->setValue('Yes');
       $sheet ->getCell('I17')->setValue('No');
       $sheet ->getCell('I18')->setValue('No');
       $sheet ->getCell('J16')->setValue('Yes');
       $sheet ->getCell('J17')->setValue('Yes');
       $sheet ->getCell('J18')->setValue('Yes');
       $sheet ->getCell('K16')->setValue('Yes');
       $sheet ->getCell('K17')->setValue('Yes');
       $sheet ->getCell('K18')->setValue('Yes');
    
     
       //$sheet ->getCell('F1')->setValue('Total Net Earnings');
       //size auto
       $sheet->getColumnDimension('A')->setAutoSize(true);
       $sheet->getColumnDimension('B')->setAutoSize(true);
       $sheet->getColumnDimension('C')->setAutoSize(true);
       $sheet->getColumnDimension('D')->setAutoSize(true);
       $sheet->getColumnDimension('E')->setAutoSize(true);
       $sheet->getColumnDimension('G')->setAutoSize(true);
       $sheet->getColumnDimension('H')->setAutoSize(true);
       $sheet->getColumnDimension('J')->setAutoSize(true);
       $sheet->getColumnDimension('K')->setAutoSize(true);
       $sheet->getColumnDimension('L')->setAutoSize(true);
       // OUTPUT
       //styles array for A1
       $styleArray = [
           'font' => [
               'bold' => true,
           ],
           'alignment' => [
               'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
           ],
           'borders' => [
               'top' => [
                   'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
               ],
           ],
           'fill' => [
               'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
               'rotation' => 90,
               'startColor' => [
                   'argb' => '808080',
               ],
               'endColor' => [
                   'argb' => 'FFFFFFFF',
               ],
           ],
//         
       ];
       //style for headings
        //styles array for A1
       $styleheading = [
           'font' => [
               'bold' => true,
           ],
           'alignment' => [
               'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
           ],
           'borders' => [
               'outline' => [
                   'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
               ],
           ],
           'fill' => [
               'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
               'rotation' => 90,
               'startColor' => [
                   'argb' => '808080',
               ],
               'endColor' => [
                   'argb' => 'FFFFFFFF',
               ],
           ],
//         
       ];
          $stylecolumns = [
           'font' => [
               'bold' => true,
           ],
           'alignment' => [
               'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
           ],
           'borders' => [
               'outline' => [
                   'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
               ],
           ],
           
//         
       ];
        $styletotal = [
           'font' => [
               'bold' => true,
           ],
           'alignment' => [
               'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
           ],
           'borders' => [
               'outline' => [
                   'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
               ],
           ],
           'fill' => [
               'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
               'rotation' => 90,
               'startColor' => [
                   'argb' => 'FFB6C1',
               ],
               'endColor' => [
                   'argb' => 'FFFFFFFF',
               ],
           ],
//         
       ];
           $databasevalues = [
           'font' => [
               'bold' => true,
           ],
           'alignment' => [
               'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
           ],
           'borders' => [
               'outline' => [
                   'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
               ],
           ],
           'fill' => [
               'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
               'rotation' => 90,
               'startColor' => [
                   'argb' => 'F5F5F5',
               ],
               'endColor' => [
                   'argb' => 'FFFFFFFF',
               ],
           ],
//         
       ];
       
       $sheet->setTitle('Report For Beneficiary');
       //find id of specific hire person
       
       $NetEarnings = array();
       $sumGrossEarnings = array();
       $i=7;
       $j=8+$queryCount;
       $k=8+$queryCount;
       foreach ($query as $items) {
            array_push($NetEarnings, $items['NetEarnings']);
            array_push($sumGrossEarnings, $items['GrossEarnings']);
            $sheet->setCellValue('A'.$i, $items['PayPeriodStart']);
            $sheet->setCellValue('B'.$i, $items['PayPeriodEnd']);
            $sheet->setCellValue('C'.$i, $items['PaidDate']);
            $sheet->setCellValue('D'.$i,'$'. $items['GrossEarnings']);
            $sheet->setCellValue('E'.$i,'$'. $items['NetEarnings']);

            $sheet->setCellValue('G'.$i, date('M - Y', strtotime($items['PayPeriodStart'])));
            $sheet->setCellValue('I'.$i,'$'. $items['NetEarnings']);
            $sheet->setCellValue('K'.$i,'$'. $items['NetEarnings']);
            //style
            $spreadsheet->getActiveSheet()->getStyle('A'.$i)->applyFromArray($databasevalues);
            $spreadsheet->getActiveSheet()->getStyle('B'.$i)->applyFromArray($databasevalues);
            $spreadsheet->getActiveSheet()->getStyle('C'.$i)->applyFromArray($databasevalues);
            $spreadsheet->getActiveSheet()->getStyle('D'.$i)->applyFromArray($databasevalues);
            $spreadsheet->getActiveSheet()->getStyle('E'.$i)->applyFromArray($databasevalues);
           $i++;
       }
       $sheet->setCellValue('D'.$j,'$'. array_sum($sumGrossEarnings));
       $sheet->setCellValue('E'.$k,'$'. array_sum($NetEarnings));

       $sheet->setCellValue('I'.$k,'$'. array_sum($NetEarnings));
       $sheet->setCellValue('K'.$k,'$'. array_sum($NetEarnings));
       $spreadsheet->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('E'.$k)->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->mergeCells("A".$k.":C".$k);
       $sheet->setCellValue("A".$k,'Total Earning: ');
       $spreadsheet->getActiveSheet()->getStyle('A'.$k)->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('G'.$k.":K".$k)->applyFromArray($styleheading);

      //colors in each A1
       $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
       $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
       $spreadsheet->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
       $spreadsheet->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
       $spreadsheet->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
       //colors for heading 
       $spreadsheet->getActiveSheet()->getStyle('A6')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('B6')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('D2')->applyFromArray($stylecolumns);
       $spreadsheet->getActiveSheet()->getStyle('D3')->applyFromArray($stylecolumns);
       $spreadsheet->getActiveSheet()->getStyle('C6')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('D6')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('E6')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('A5:B5')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('A6:B6')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('G6:K6')->applyFromArray($styleheading);
       $spreadsheet->getActiveSheet()->getStyle('G15:K15')->applyFromArray($styleheading);
       
       //text colors
       $spreadsheet->getActiveSheet()->getStyle('B2')
       ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
       $spreadsheet->getActiveSheet()->getStyle('A2')
       ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
       $spreadsheet->getActiveSheet()->getStyle('D2')
       ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
       $spreadsheet->getActiveSheet()->getStyle('D3')
       ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
       $spreadsheet->getActiveSheet()->getStyle('A4')
       ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
       $spreadsheet->getActiveSheet()->getStyle('A3')
       ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
       //specific height of a row
       $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
       $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(150);
       //merge cells
       $spreadsheet->getActiveSheet()->mergeCells("A1:B1:C1:D1:E1");
       $spreadsheet->getActiveSheet()->mergeCells("A5:B5");
       
       
       //number formats
      
       $writer = new Xlsx($spreadsheet);
       // OR FORCE DOWNLOAD
       header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
       header('Content-Disposition: attachment;filename="Report.xlsx"');
       header('Cache-Control: max-age=0');
       header('Expires: Fri, 11 Nov 2011 11:11:11 GMT');
       header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
       header('Cache-Control: cache, must-revalidate');
       header('Pragma: public');
       $writer->save('php://output');

   }

}

