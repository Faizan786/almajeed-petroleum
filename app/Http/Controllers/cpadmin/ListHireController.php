<?php
/**
 * Created by PhpStorm.
 * User: Tamim
 * Date: 12/5/2019
 * Time: 12:56 PM
 */


namespace App\Http\Controllers\cpadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoulDoit\DataTable\SSP;
use App\Http\Models\Hire;
use App\Http\Models\Paystub;



class ListHireController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('authCheckAdmin');
    }

    public function Index($type = "") {
        $listType = "";
        if ($type == "active") {
            $listType = "Y";
        } else if ($type == "inactive") {
            $listType = "N";
        } else if ($type == "trashed") {
            $listType = "D";
        }
        return view('cpadmin.Hire.list', ['title' => 'Control Panel - Active Hire List', 'listType' => $listType]);
    }

    public function ListDataGet() {
        try {
            $request = request();
            $data = $request->all();
            $page = $data['page'];
            $pageno = (isset($data['pageno']) && $data['pageno'] > 0) ? $data['pageno'] : 1;
            $sort_column = (isset($data['sorting']) && trim($data['sorting']) != '') ? trim($data['sorting']) : '';
            $query_search = (isset($data['query_search']) && trim($data['query_search']) != '') ? trim($data['query_search']) : '';
            $results_on_page = (isset($data['size']) && $data['size'] > 0) ? $data['size'] : '';
            $offset = ($pageno-1)*$results_on_page;

            $object = Hire::where('hire_information.is_trashed', 'No')
                    ->leftJoin('beneficiary as bene', 'bene.id', '=', 'hire_information.beneficiary_id')
                    ->select('hire_information.*', 'bene.FirstName', 'bene.LastName', 'bene.SSN');
            if ($query_search != "") {
                $object->where(function ($query) use ($query_search) {
                    if (validateDate($query_search, 'd') == true) {
                        $query->orwhereDay('hire_information.DateOfHire', "$query_search");
                    }
                    if (validateDate(ucfirst(strtolower(trim($query_search))), 'M') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'F') == true) {
                        $query->orwhereMonth('hire_information.DateOfHire', date('m', strtotime($query_search)));
                    }
                    if (validateDate($query_search, 'Y') == true && strlen($query_search) == 4) {
                        $query->orWhereYear('hire_information.DateOfHire', $query_search);
                    }
                    if (validateDate($query_search, 'M d, Y') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'M d, Y') == true || validateDate($query_search, 'F d, Y') == true || validateDate(ucfirst(strtolower(trim($query_search))), 'F d, Y') == true) {
                        $query->orwhereDate('hire_information.DateOfHire', date('Y-m-d', strtotime($query_search)));
                    }
                    $query->orWhere('hire_information.id', "$query_search");
                    $query->orWhere('bene.FirstName', 'LIKE', "%$query_search%");
                    $query->orWhere('bene.LastName', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.DateOfHire', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.JobType', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.HourlyWage', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.Employer', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.Benefits', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.Address', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.PayStart', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.PayEnd', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.PaidDate', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.GrossEarnings', 'LIKE', "%$query_search%");
                    $query->orWhere('hire_information.EarningStructure', 'LIKE', "%$query_search%");
                });
            }
            if ($sort_column != "") {
                $exp_sort_column = explode('~', $sort_column);
                $object->orderBy($exp_sort_column[0], $exp_sort_column[1]);
            } else {
                $object->orderBy('hire_information.id', 'DESC');
            }
            //print_r($object->toSql()); die;
            $array['counter'] = display_number_of_records($object->count(), $pageno, $results_on_page);
            $array['pagination'] = display_paginations($object->count(), $pageno, $results_on_page);
            $array['object'] = $object->offset($offset)->limit($results_on_page)->get();
            return $array;
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function ViewDetail() {
        $request = request();
        $data = $request->all();
        $id = $data['id'];
        try {
            $object = Hire::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $object->beneficiary = select_field_column('beneficiary', 'id', $object->beneficiary_id, 'FirstName').' '.select_field_column('beneficiary', 'id', $object->beneficiary_id, 'LastName');
                return $object;
            } else {
                $data = array('error' => 1, 'message' => 'Object not found!');
                return $data;
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }



    public function CreateNew() {
        $request = request();
        $data = $request->all();
        //echo '<pre>'; print_r($data); die;
        try {
            if (
                $data['DateOfHire'] == "" && $data['JobType'] == ""
                && $data['HourlyWage'] == "" && $data['Employer'] == ""
                && $data['Benefits'] == "" && $data['Address'] == ""
                && $data['PayStart'] == "" && $data['PayEnd'] == ""
                && $data['PaidDate'] == "" && $data['MonthPaid'] == ""
                && $data['GrossEarnings'] == "" && $data['EarningStructure'] == ""
                && $data['TWL'] == "" && $data['SGA'] == ""

            ){
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Registration Failled!');
                \Session::flash('message', "Please fill all the required fields!");
                return redirect('/cpadmin/hire-list');
            } else {
                $Hire = new Hire();
                $Hire->populate($data);
                $Hire->save();
                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Registration Successfull');
                \Session::flash('message', 'Hire Created Successfull .');
                return redirect('/cpadmin/hire-list/' );
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_type', 'error');
            \Session::flash('message_title', 'Registration Failled!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/hire-list/');
        }
    }


    public function UpdateDetail() {
        try {
            $request = request();
            $data = $request->all();
            $id = $data['id'];
            $name = $data['name'];
            $value = $data['value'];
            $update[$name] = $value;
            //echo '<pre>'; print_r($update); die;

            $object = Hire::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $object->populate($update);
                $object->save();

                $data = array('success' => 1, 'message' => 'Record saved successful!');
                return $data;
            } else {
                $data = array('error' => 1, 'message' => 'Object not found!');
                return $data;
            }
        } catch (\Exception $e) {
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            $data = array('error' => 1, 'message' => $e->getMessage());
            return $data;
        }
    }

    public function TrashData() {
        try {
            $request = request();
            $data = $request->all();
            $id = $data['id'];
            //echo '<pre>'; print_r($data); die;
            $object = Hire::find($id);
            if (isset($object) && !is_null($object) && $object->count() > 0) {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $data['is_trashed'] = "Yes";
                $object->populate($data);
                $object->save();

                \Session::flash('message_type', 'success');
                \Session::flash('message_title', 'Delete Successfull');
                \Session::flash('message', 'Data deleted successfull.');
                return redirect('/cpadmin/hire-list/');
            } else {
                \Session::flash('message_type', 'error');
                \Session::flash('message_title', 'Delete Failed');
                \Session::flash('message', 'Invalid Data Provided May Be Session Expired, Please relogin.');
                return redirect('/cpadmin/hire-list/');
            }
        } catch (\Exception $e) {
            \Session::flash('message_type', 'error');
            \Log::info("List Exception", array("Exception" => $e->getMessage()));
            \Session::flash('message_title', 'Delete Failed!');
            \Session::flash('message', $e->getMessage());
            return redirect('/cpadmin/hire-list/');
        }
    }



}
