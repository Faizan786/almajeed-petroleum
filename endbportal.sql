-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2019 at 06:15 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `endbportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `beneficiary`
--

CREATE TABLE `beneficiary` (
  `id` int(11) NOT NULL,
  `FirstName` varchar(150) DEFAULT NULL,
  `LastName` varchar(150) DEFAULT NULL,
  `SSN` varchar(30) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(20) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Disabilities` varchar(100) DEFAULT 'No',
  `PhysicalRestrictions` enum('Yes','No') DEFAULT 'No',
  `JobGoals` enum('Yes','No') DEFAULT 'No',
  `ROIOnFile` enum('Yes','No') DEFAULT 'No',
  `ReleaseToVerifyWages` enum('Yes','No') DEFAULT 'No',
  `BenefitsCounseling` enum('Yes','No') DEFAULT 'No',
  `SoftSkills` enum('Yes','No') DEFAULT NULL,
  `JSS` enum('Yes','No') DEFAULT NULL,
  `JobSearch` enum('Yes','No') DEFAULT NULL,
  `Retention` varchar(255) DEFAULT NULL,
  `is_trashed` enum('No','Yes') DEFAULT 'No',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beneficiary`
--

INSERT INTO `beneficiary` (`id`, `FirstName`, `LastName`, `SSN`, `Address`, `PhoneNumber`, `Email`, `Disabilities`, `PhysicalRestrictions`, `JobGoals`, `ROIOnFile`, `ReleaseToVerifyWages`, `BenefitsCounseling`, `SoftSkills`, `JSS`, `JobSearch`, `Retention`, `is_trashed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dwsf', 'sdfs', 'sdfs', 'sdfds', 'sdf', 'sdfsdf', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'Yes', 'No any retention', 'No', '2019-08-05 16:36:42', '2019-12-07 01:23:48', '2019-12-07 01:23:48'),
(2, 'dd', 'asds', 'sadas', 'asd', 'dssd', 'tamimahmadiub@gmail.com', 'sadd', '', '', 'No', 'No', 'No', NULL, NULL, NULL, NULL, 'No', '2019-12-05 04:39:32', '2019-12-05 04:39:32', NULL),
(3, 'Waqar', 'Javed', '123456-123', 'Gang Road, Street 10th South', '03225892509', 'skpsoftech@gmail.com', 'No any disabilities', 'No', 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, 'Yes', NULL, '2019-12-08 03:34:11', '2019-12-08 03:34:11');

-- --------------------------------------------------------

--
-- Table structure for table `hire_information`
--

CREATE TABLE `hire_information` (
  `id` bigint(50) NOT NULL,
  `beneficiary_id` int(11) DEFAULT NULL,
  `JobType` enum('Full Time','Part Time') DEFAULT NULL,
  `HourlyWage` decimal(10,0) DEFAULT NULL,
  `Employer` varchar(150) DEFAULT NULL,
  `Benefits` varchar(150) DEFAULT NULL,
  `Address` varchar(150) DEFAULT NULL,
  `PayStart` date DEFAULT NULL,
  `PayEnd` date DEFAULT NULL,
  `PaidDate` date DEFAULT NULL,
  `GrossEarnings` varchar(150) DEFAULT NULL,
  `EarningStructure` enum('SSDI','SSI','Both') DEFAULT 'Both',
  `TWL` enum('Yes','No') DEFAULT 'No',
  `SGA` enum('Yes','No') DEFAULT 'Yes',
  `DateOfHire` date DEFAULT NULL,
  `is_trashed` enum('No','Yes') DEFAULT 'No',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hire_information`
--

INSERT INTO `hire_information` (`id`, `beneficiary_id`, `JobType`, `HourlyWage`, `Employer`, `Benefits`, `Address`, `PayStart`, `PayEnd`, `PaidDate`, `GrossEarnings`, `EarningStructure`, `TWL`, `SGA`, `DateOfHire`, `is_trashed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Full Time', '342', 'wasdf', 'sdfs', 'sdf', '2019-12-05', '2019-12-30', '2020-01-01', '30000', 'SSDI', 'No', 'Yes', '2019-12-01', 'No', '2019-08-05 16:36:42', '2019-08-05 16:36:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messaging`
--

CREATE TABLE `messaging` (
  `id` int(11) NOT NULL,
  `beneficiary_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  `status` enum('read','unread') DEFAULT 'unread',
  `user_type` enum('user','admin','employee') DEFAULT 'user',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paystub`
--

CREATE TABLE `paystub` (
  `id` int(100) NOT NULL,
  `hire_id` int(100) NOT NULL,
  `beneficiary_id` int(100) NOT NULL,
  `PayPeriodStart` date NOT NULL,
  `PayPeriodEnd` date NOT NULL,
  `PaidDate` date NOT NULL,
  `GrossEarnings` decimal(10,2) NOT NULL,
  `NetEarnings` decimal(10,2) NOT NULL,
  `is_trashed` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paystub`
--

INSERT INTO `paystub` (`id`, `hire_id`, `beneficiary_id`, `PayPeriodStart`, `PayPeriodEnd`, `PaidDate`, `GrossEarnings`, `NetEarnings`, `is_trashed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 0, '2019-12-07', '2019-12-07', '2019-12-07', '31231.00', '12313.00', 'No', '2019-12-07 02:25:40', '2019-12-07 02:25:40', '0000-00-00 00:00:00'),
(2, 1, 1, '2019-12-07', '2019-12-11', '2019-12-11', '1000.50', '1234.50', 'No', '2019-12-07 02:33:02', '2019-12-07 02:33:02', '0000-00-00 00:00:00'),
(3, 1, 1, '2019-12-07', '2019-12-12', '2019-12-13', '3121.12', '3434.34', 'No', '2019-12-07 02:54:19', '2019-12-07 02:54:19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles_define`
--

CREATE TABLE `roles_define` (
  `id` int(11) NOT NULL,
  `section_name` varchar(250) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` enum('Module','Section','Page') DEFAULT 'Module',
  `has_create` tinyint(2) DEFAULT 0,
  `has_edit` tinyint(2) DEFAULT 0,
  `has_delete` tinyint(2) DEFAULT 0,
  `has_detail` tinyint(2) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles_define`
--

INSERT INTO `roles_define` (`id`, `section_name`, `parent_id`, `type`, `has_create`, `has_edit`, `has_delete`, `has_detail`) VALUES
(1, 'Users List', 0, 'Module', 0, 0, 0, 0),
(2, 'Active Users List', 1, 'Section', 1, 1, 1, 1),
(3, 'Inactive Users List', 1, 'Section', 1, 1, 1, 1),
(4, 'Trashed Users List', 1, 'Section', 1, 1, 1, 1),
(5, 'Messages List', 0, 'Module', 0, 1, 1, 1),
(6, 'Beneficiary', 0, 'Module', 1, 1, 1, 1),
(7, 'Hire Information', 0, 'Module', 1, 1, 1, 1),
(8, 'Reports', 0, 'Module', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles_users`
--

CREATE TABLE `roles_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `type` enum('Module','Section','Page') DEFAULT 'Module',
  `has_create` tinyint(2) DEFAULT 0,
  `has_edit` tinyint(2) DEFAULT 0,
  `has_delete` tinyint(2) DEFAULT 0,
  `has_detail` tinyint(2) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles_users`
--

INSERT INTO `roles_users` (`id`, `user_id`, `role_id`, `type`, `has_create`, `has_edit`, `has_delete`, `has_detail`) VALUES
(1, 1, 1, 'Module', 0, 0, 0, 0),
(2, 1, 2, 'Section', 1, 1, 1, 1),
(3, 1, 3, 'Section', 1, 1, 1, 1),
(4, 1, 4, 'Section', 1, 1, 1, 1),
(5, 1, 5, 'Module', 0, 1, 1, 1),
(6, 1, 6, 'Module', 1, 1, 1, 1),
(7, 1, 7, 'Module', 1, 1, 1, 1),
(8, 1, 8, 'Module', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `fullname` varchar(250) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(100) DEFAULT NULL,
  `company` varchar(250) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `last_visited` datetime DEFAULT NULL,
  `user_type` enum('superadmin','admin','employee','user') DEFAULT 'user',
  `status` enum('Y','N','D') DEFAULT 'N',
  `email_verified` enum('Yes','No') DEFAULT 'No',
  `email_verified_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `fullname`, `gender`, `email`, `username`, `password`, `salt`, `company`, `phone_number`, `photo`, `country`, `last_visited`, `user_type`, `status`, `email_verified`, `email_verified_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Waqar', 'Javed', 'Waqar Javed', 'Male', 'skpsoftech@gmail.com', 'admin', '$2a$10$cASBGmhnx2dyBYIv7ws6P.Yj0vRf5yrH3pYOhoFqgvYXXWEmxNG.C', '$2a$10$cASBGmhnx2dyBYIv7ws6P.', 'SKP SOFT', '03225892509', '', NULL, NULL, 'superadmin', 'Y', 'Yes', NULL, '2019-08-05 16:36:42', '2019-11-02 21:27:29', '2019-11-02 21:27:29'),
(5, 'Asif', 'Raza', 'Asif Raza', NULL, 'testing4@gmail.com', 'testing', '$2a$10$D3/SjVe/APuu2xE2UVm2T.Q8VbnWhyI/aPIJ/5cJJwcvoQ3enE/XC', '$2a$10$D3/SjVe/APuu2xE2UVm2TB', 'Test', '03225892505', 'admin_15cd20.jpg', NULL, NULL, 'user', 'Y', 'No', NULL, '2019-10-10 21:47:37', '2019-11-02 21:17:08', NULL),
(11, 'Muhammd', 'Haseeb', 'Muhammd Haseeb', NULL, 'haseeb@gmail.com', 'testing2', '$2a$10$Ma3ySP9qqXTMN21mxjTgg.xHeyeudZvbjmfvEhUumgGPB6T/TVXS.', '$2a$10$Ma3ySP9qqXTMN21mxjTggB', 'Haseeb Comp', '03225892504', NULL, NULL, NULL, 'user', 'Y', 'No', NULL, '2019-10-10 22:46:55', '2019-11-02 21:16:57', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beneficiary`
--
ALTER TABLE `beneficiary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hire_information`
--
ALTER TABLE `hire_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messaging`
--
ALTER TABLE `messaging`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `paystub`
--
ALTER TABLE `paystub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_define`
--
ALTER TABLE `roles_define`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_users`
--
ALTER TABLE `roles_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beneficiary`
--
ALTER TABLE `beneficiary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hire_information`
--
ALTER TABLE `hire_information`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messaging`
--
ALTER TABLE `messaging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paystub`
--
ALTER TABLE `paystub`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles_define`
--
ALTER TABLE `roles_define`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles_users`
--
ALTER TABLE `roles_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
