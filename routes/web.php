<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'HomeController@index');
//
//Route::get('/message-get-all/tasksheet/{order_id}', 'MessagingController@MessageGetAllTaskSheet');
Route::get('/message-get-all/{beneficiary_id}', 'MessagingController@MessageGetAll');
Route::post('/message-save', 'MessagingController@MessageSave');

Route::prefix('cpadmin')->group(function(){
    Route::get('/', 'cpadmin\LoginController@Login');
    Route::post('/login-data', 'cpadmin\LoginController@LoginData');
    Route::get('/forgot-password', 'cpadmin\LoginController@ForgotPassword');
    Route::post('/forgot-password', 'cpadmin\LoginController@ForgotPasswordData');
    Route::get('/logout', 'cpadmin\LoginController@LogOut');
    Route::get('/dashboard', 'cpadmin\DashboardController@Dashboard');
    Route::get('/profile', 'cpadmin\DashboardController@Profile');
    Route::post('/profile', 'cpadmin\DashboardController@ProfileData');
    Route::post('/change-pass', 'cpadmin\DashboardController@ChangePassword');

    //Users List
    Route::get('/users-list/{type}', 'cpadmin\ListUsersController@Index');
    Route::post('/users-list-data', 'cpadmin\ListUsersController@ListDataGet');
    Route::post('/users-list/create/{type}', 'cpadmin\ListUsersController@CreateNew');
    Route::post('/users-list/edit/{type}', 'cpadmin\ListUsersController@EditDetail');
    Route::post('/users-list/delete/{type}', 'cpadmin\ListUsersController@DeleteData');
    Route::post('/users-list/empty-trash/{type}', 'cpadmin\ListUsersController@EmptyTrashed');
    Route::post('/users-list/empty-trash-all/{type}', 'cpadmin\ListUsersController@AllEmptyTrashed');
    Route::post('/users-list/view', 'cpadmin\ListUsersController@ViewDetail');
    Route::post('/users-list/changestatus', 'cpadmin\ListUsersController@ChangeStatus');

    //Messages List
    Route::get('/messages-list', 'cpadmin\MessagesListController@Index');
    Route::post('/messages-list/onload', 'cpadmin\MessagesListController@GetMessagesList');
    Route::post('/messages-list/chatload', 'cpadmin\MessagesListController@GetMessagesChat');
    Route::post('/messages-list/chatsubmit', 'cpadmin\MessagesListController@SubmitMessagesChat');
    Route::post('/messages-list/clearchat', 'cpadmin\MessagesListController@ClearMessagesChat');

    //Beneficiary List
    Route::get('/beneficiary-list/', 'cpadmin\ListBeneficiaryController@Index');
    Route::post('/beneficiary-list-data', 'cpadmin\ListBeneficiaryController@ListDataGet');
    Route::post('/beneficiary-list/trash', 'cpadmin\ListBeneficiaryController@TrashData');
    Route::post('/beneficiary-list/create', 'cpadmin\ListBeneficiaryController@CreateNew');
    Route::post('/beneficiary-list/view', 'cpadmin\ListBeneficiaryController@ViewDetail');
    Route::post('/beneficiary-list/update', 'cpadmin\ListBeneficiaryController@UpdateData');
    //Hire Information List

    Route::get('/hire-list', 'cpadmin\ListHireController@Index');
    Route::post('/hire-list-data', 'cpadmin\ListHireController@ListDataGet');
    Route::post('/hire-list/trash', 'cpadmin\ListHireController@TrashData');
    Route::post('/hire-list/create', 'cpadmin\ListHireController@CreateNew');
    Route::post('/hire-list/view', 'cpadmin\ListHireController@ViewDetail');
    Route::post('/hire-list/update', 'cpadmin\ListHireController@UpdateDetail');
    Route::get('/hire-list/report', 'cpadmin\ListHireController@ExcelReport');

    //pay stub
    Route::post('/paystub-list/paystub', 'cpadmin\ListPaystubController@PayStub');
    Route::get('/paystub-list/export/{id}', 'cpadmin\ListPaystubController@ExportExcel');

    //Reports List
    Route::get('/reports-list', 'cpadmin\ListReportsController@Index');
    Route::post('/reports-list-data', 'cpadmin\ListReportsController@ListDataGet');


});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
